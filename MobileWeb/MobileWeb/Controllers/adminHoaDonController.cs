﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using MobileWeb.ViewModels;
using MobileWebBLL.BLL;
using MobileWebBLL.Models;

namespace MobileWeb.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class adminHoaDonController : Controller
    {
        //
        // GET: /adminHoaDon/
        public HoaDonBLL adHoaDon = new HoaDonBLL();
        public ChiTietHoaDonBLL adcthd = new ChiTietHoaDonBLL();
        public KhachHangBLL adkhachhang = new KhachHangBLL();

        private void SendMail(long MaHD)
        {
            HoaDon hd = adHoaDon.GetOneBillToEdit(MaHD);
            int MaKH = int.Parse(hd.MaKH.ToString());
            KhachHang kh = adkhachhang.GetOneCustomer(MaKH);

            List<ChiTietHoaDon> listcthd = adcthd.ShowDetailsBill(MaHD);
            //Send mail
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com", 587);
            mail.From = new MailAddress("tinivippromobile@gmail.com");
            mail.To.Add("violet.glass9x86@gmail.com");
            //mail.To.Add(kh.Email);
            mail.Subject = "Thông tin hóa đơn - TiniVippro";
            mail.IsBodyHtml = true;
            mail.Body += String.Format("Họ tên: <b>{0}</b><br/>Email: <b>{1}</b><br/>Ngày đặt hàng: <b>{2}</b><br/><br/>", kh.TenKH, kh.Email, String.Format("{0:dd/MM/yyyy}", hd.ThoiGianDatHang));
            for (int i = 0; i < listcthd.Count; i++)
            {
                mail.Body += String.Format("<span style=\"margin-left:20px\">+ Sản phẩm: </span><b>{0}</b> Số lượng: <b>{1}</b>        Thành tiền: <b>{2}</b><br/><br/>", listcthd[i].SanPham.TenSP, listcthd[i].SoLuong, listcthd[i].ThanhTien);
            }
            mail.Body += String.Format("Tổng tiền: <b>{0}</b>", hd.TongTien);
            SmtpServer.EnableSsl = true;
            SmtpServer.UseDefaultCredentials = false;
            var loginInfo = new System.Net.NetworkCredential("tinivippromobile@gmail.com", "tinivippro123");
            SmtpServer.Credentials = loginInfo;
            SmtpServer.Send(mail);
        }

        private void SendMailCancel(long MaHD)
        {
            HoaDon hd = adHoaDon.GetOneBillToEdit(MaHD);
            int MaKH = int.Parse(hd.MaKH.ToString());
            KhachHang kh = adkhachhang.GetOneCustomer(MaKH);

            List<ChiTietHoaDon> listcthd = adcthd.ShowDetailsBill(MaHD);
            //Send mail
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com", 587);
            mail.From = new MailAddress("tinivippromobile@gmail.com");
            mail.To.Add("violet.glass9x86@gmail.com");
            //mail.To.Add(kh.Email);
            mail.Subject = "Thông báo hủy hóa đơn vì không thanh toán đúng kỳ hạn - TiniVippro";
            mail.IsBodyHtml = true;
            mail.Body += String.Format("Họ tên: <b>{0}</b><br/>Email: <b>{1}</b><br/>Ngày đặt hàng: <b>{2}</b><br/><br/>", kh.TenKH, kh.Email, String.Format("{0:dd/MM/yyyy}", hd.ThoiGianDatHang));
            for (int i = 0; i < listcthd.Count; i++)
            {
                mail.Body += String.Format("<span style=\"margin-left:20px\">+ Sản phẩm: </span><b>{0}</b> Số lượng: <b>{1}</b>        Thành tiền: <b>{2}</b><br/><br/>", listcthd[i].SanPham.TenSP, listcthd[i].SoLuong, listcthd[i].ThanhTien);
            }
            mail.Body += String.Format("Tổng tiền: <b>{0}</b>", hd.TongTien);
            SmtpServer.EnableSsl = true;
            SmtpServer.UseDefaultCredentials = false;
            var loginInfo = new System.Net.NetworkCredential("tinivippromobile@gmail.com", "tinivippro123");
            SmtpServer.Credentials = loginInfo;
            SmtpServer.Send(mail);
        }

        public ActionResult Index(int? ma)
        {
            int message = ma ?? 0;
            switch (message)
            {
                case 1: ViewBag.Message = "Cập nhật thành công"; break;
                case 2: ViewBag.Message = "Xóa thành công"; break;
                case 3: ViewBag.Message = "Xóa thất bại. Xin vui lòng thử lại sau"; break;
                case 4: ViewBag.Message = "Cập nhật thất bại. Xin vui lòng thử lại sau"; break;
                case 5: ViewBag.Message = "Hủy thành công"; break;
                case 6: ViewBag.Message = "Hủy thất bại. Xin vui lòng thử lại sau"; break;
            }
            var bills = adHoaDon.ShowListBills();
            return View(bills);
        }
        public ActionResult UpdateTinhTrang12(long MaHD)
        {
            bool kq = false;

            ViewBag.Message = null;
            kq = adHoaDon.UpdateTinhTrangDAO12(MaHD);
            if (kq)
            {
                return RedirectToAction("Index", new { ma = 1 });
                SendMail(MaHD);
            }
            else
            {
                return RedirectToAction("Index", new { ma = 4 });
            }

        }

        public ActionResult UpdateTinhTrang3(long MaHD)
        {
            bool kq = false;

            ViewBag.Message = null;
            kq = adHoaDon.UpdateTinhTrangDAO3(MaHD);
            if (kq)
            {
                return RedirectToAction("Index", new { ma = 1 });
            }
            else
            {
                return RedirectToAction("Index", new { ma = 4 });
            }
        }

        public ActionResult UpdateTinhTrang4(long MaHD)
        {
            bool kq = false;

            ViewBag.Message = null;
            kq = adHoaDon.UpdateTinhTrangDAO4(MaHD);
            if (kq)
            {
                return RedirectToAction("Index", new { ma = 1 });
            }
            else
            {
                return RedirectToAction("Index", new { ma = 4 });
            }
        }

        public ActionResult ShowEditBill(long MaHD, int? ma)
        {
            int message = ma ?? 0;
            if (message == 10)
            {
                ViewBag.Message = "Cập nhật thất bại. Xin vui lòng thử lại sau";
            }
            var hd = adHoaDon.GetOneBillToEdit(MaHD);
            return View(hd);
        }

        public ActionResult EditBill(HoaDon HoaDon)
        {
            bool kq = false;

            ViewBag.Message = null;
            kq = adHoaDon.UpdateBillDAO(HoaDon); if (kq)
            {
                return RedirectToAction("Index", new { ma = 1 });
            }
            else
            {
                return RedirectToAction("ShowEditBill", new { MaHD = HoaDon.MaHD, ma = 10 });
            }

        }

        public ActionResult CancelBill(long MaHD)
        {
            bool kq = false;

            ViewBag.Message = null;
            kq = adHoaDon.CancelBillDAO(MaHD);
            if (kq)
            {
                return RedirectToAction("Index", new { ma = 5 });
            }
            else
            {
                return RedirectToAction("Index", new { ma = 6 });
            }
        }

        public ActionResult CancelBillOverdue()
        {
            bool kq = false;

            ViewBag.Message = null;
            List<HoaDon> listhd = adHoaDon.GetBillOverdue();
            kq = adHoaDon.CancelBillOverdue();
            for (int i = 0; i < listhd.Count; i++)
            {
                long MaHD = listhd[i].MaHD;
                SendMailCancel(MaHD);
            }
            if (kq)
            {
                return RedirectToAction("Index", new { ma = 5 });
            }
            else
            {
                return RedirectToAction("Index", new { ma = 6 });
            }
        }

        public ActionResult adminShowListBillCancel(int? ma)
        {
            int message = ma ?? 0;
            if (message == 3)
            {
                ViewBag.Message = "Xóa thất bại. Xin vui lòng thử lại sau";
            }
            var bills = adHoaDon.ShowListCancelBills();
            return View(bills);
        }

        public ActionResult Deletebill(long MaHD)
        {
            bool kq = false;

            ViewBag.Message = null;
            kq = adHoaDon.DeleteBillDAO(MaHD);
            if (kq)
            {
                return RedirectToAction("Index", new { ma = 2 });
            }
            else
            {
                return RedirectToAction("adminShowListBillCancel", new { ma = 3 });
            }
        }

        //Chi tiết hóa đơn

        public ActionResult adminShowDetailBill(long MaHD)
        {
            var cthd = adcthd.ShowDetailsBill(MaHD);
            return View(cthd);
        }

        public ActionResult DeleteDetailbill(long MaCTHD)
        {
            bool kq = false;

            ViewBag.Message = null;
            kq = adcthd.DeleteDetailBillDAO(MaCTHD);
            if (kq)
            {
                return RedirectToAction("Index", new { ma = 2 });
            }
            else
            {
                return RedirectToAction("Index", new { ma = 3 });
            }
        }

    }
}
