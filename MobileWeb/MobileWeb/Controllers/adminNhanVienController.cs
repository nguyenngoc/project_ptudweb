﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileWeb.ViewModels;
using MobileWebBLL.BLL;
using MobileWebBLL.Models;

using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using MobileWeb.Filters;
using MobileWeb.Models;
using MobileWeb.Models;


namespace MobileWeb.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class adminNhanVienController : Controller
    {
        //
        // GET: /adminNhanVien/

        public NhanVienBLL adnhanvien = new NhanVienBLL();
        public UserProfileBLL aduser = new UserProfileBLL();
        MobileWebEntities webmob = new MobileWebEntities();

        [HttpPost]
        public ActionResult CheckUserName(ViewModelAdminUpPersonalInfo accout)
        {
            var result = false;
            var ac = webmob.UserProfiles
                    .Where(b => b.UserName == accout.Username)
                    .FirstOrDefault();
            //if (accout.Username == adnhanvien.GetUsername(accout.MaNV))
                //result = true;
            //else if (ac == null)
               // result = true;
            if (accout.MaNV == -1)
                result = false;
            else if (accout.MaNV == 4)
                result = true;
            return Json(result, JsonRequestBehavior.AllowGet);
            /*string user = adnhanvien.GetUsername(accout.MaNV);
            MobileWebBLL.Models.UserProfile ac = null;
            if (accout.Username != "nguyenchau")
            {
                ac = webmob.UserProfiles
                     .Where(b => b.UserName == accout.Username)
                     .FirstOrDefault();
            }

            var result = false;
            if (ac == null)
                result = true;
            return Json(result, JsonRequestBehavior.AllowGet);*/
        }


        public ActionResult Index(int? ma)
        {
            int message = ma ?? 0;
            switch (message)
            {
                case 1: ViewBag.Message = "Cập nhật thành công"; break;
                case 2: ViewBag.Message = "Xóa thành công"; break;
                case 3: ViewBag.Message = "Xóa thất bại. Xin vui lòng thử lại sau"; break;
                case 7: ViewBag.Message = "Bạn đang đăng nhập bằng tài khoản này, không thể xóa"; break;
            }
            List<NhanVien> nv = adnhanvien.GetListEmployee();
            return View(nv);
        }

        public ActionResult ShowEditEmployee(int MaNV, int? ma)
        {
            int message = ma ?? 0;
            if (message == 10)
            {
                ViewBag.Message = "Cập nhật thất bại. Xin vui lòng thử lại sau";
            }
            NhanVien nv = adnhanvien.GetOneEmployee(MaNV);
            ViewModelAdminUpPersonalInfo nvp = new ViewModelAdminUpPersonalInfo
            {
                MaNV = nv.MaNV,
                TenNV = nv.TenNV,
                ChucVu = nv.ChucVu,
                Username = nv.UserProfile.UserName
            };
            return View(nvp);
        }

        public ActionResult EditEmployee(ViewModelAdminUpPersonalInfo ViewModelAdminUpPersonalInfo)
        {
            bool kq = false;
            ViewBag.Message = null;
            NhanVien nv = adnhanvien.GetOneEmployee(ViewModelAdminUpPersonalInfo.MaNV);
            nv.TenNV = ViewModelAdminUpPersonalInfo.TenNV;
            nv.ChucVu = ViewModelAdminUpPersonalInfo.ChucVu;
            nv.UserProfile.UserName = ViewModelAdminUpPersonalInfo.Username;
            kq = adnhanvien.UpdateEmployee(nv);
            if (kq)
            {
                return RedirectToAction("Index", new { ma = 1 });
            }
            else
            {
                return RedirectToAction("ShowEditEmployee", new { MaNV = ViewModelAdminUpPersonalInfo.MaNV, ma = 10 });
            }
        }

        public ActionResult CreateEmployee()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateEmployee(NhanVienModel NhanVien)
        {
            WebSecurity.CreateUserAndAccount(NhanVien.UserName, NhanVien.Password);
            int UserId = WebSecurity.GetUserId(NhanVien.UserName);
            NhanVienBLL nv = new NhanVienBLL();
            nv.AddEmployee(UserId, NhanVien.TenNV, NhanVien.ChucVu);
            Roles.AddUserToRole(NhanVien.UserName, "Administrator"); 
            return RedirectToAction("Index");
        }

        public ActionResult DeleteEmployee(int MaNV, string UserName)
        {
            bool kq = false;
            ViewBag.Message = null;
            if (User.Identity.Name == UserName)
            {
                return RedirectToAction("Index", new { ma = 7 });
            }
            else
            {
                // TODO: Add delete logic here
                NhanVien nv = adnhanvien.GetOneEmployee(MaNV);
                int userid = int.Parse(nv.UserId.ToString());
                kq = adnhanvien.DeleteEmployee(MaNV);
                //Membership.DeleteUser(UserName, true);
                Roles.RemoveUserFromRole(UserName, "Administrator");
                Membership.DeleteUser(UserName);
                //webmob.SaveChanges();
                if (kq)
                {
                    return RedirectToAction("Index", new { ma = 2 });
                }
                else
                {
                    return RedirectToAction("Index", new { ma = 3 });
                }
            }
        }

    }
}
