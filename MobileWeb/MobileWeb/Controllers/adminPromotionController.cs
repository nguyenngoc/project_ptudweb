﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileWeb.ViewModels;
using MobileWebBLL.BLL;
using MobileWebBLL.Models;

namespace MobileWeb.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class adminPromotionController : Controller
    {
        //
        // GET: /adminPromotion/


        public KhuyenMaiBLL adPromotion = new KhuyenMaiBLL();
        public ProductBLL adProduct = new ProductBLL();

        public ActionResult Index(int? ma)
        {
            int message = ma ?? 0;
            switch (message)
            {
                case 1: ViewBag.Message = "Cập nhật thành công"; break;
                case 2: ViewBag.Message = "Xóa thành công"; break;
                case 3: ViewBag.Message = "Xóa thất bại. Xin vui lòng thử lại sau"; break;
                case 8: ViewBag.Message = "Thêm thành công"; break;
            }
            var pro = adPromotion.ShowListPromotions();
            return View(pro);
        }

        public ActionResult ShowEditPromotion(int MaKhuyenMai, int? ma)
        {
            int message = ma ?? 0;
            if (message == 1)
            {
                ViewBag.Message = "Cập nhật thất bại. Xin vui lòng thử lại sau";
            }
            ViewModelPromotion editPromotion = new ViewModelPromotion
            {
                KhuyenMai = adPromotion.GetOnePromotion(MaKhuyenMai),
                SanPham = adProduct.ShowListProduct(),
                QuaTang = adPromotion.GetListGifts()
            };
            return View(editPromotion);
        }
        public ActionResult EditPromotion(KhuyenMai KhuyenMai)
        {
            bool kq = false;
            ViewBag.Message = null;
            kq = adPromotion.EditPromotionDAO(KhuyenMai);
            if (kq)
            {
                return RedirectToAction("Index", new { ma = 1 });
            }
            else
            {
                return RedirectToAction("ShowEditPromotion", new { MaKhuyenMai = KhuyenMai.MaKhuyenMai, ma = 1 });
            }
        }

        public ActionResult CreatePromotion(int? ma)
        {
            int message = ma ?? 0;
            if (message == 11)
            {
                ViewBag.Message = "Cập nhật thất bại. Xin vui lòng thử lại sau";
            }
            ViewModelPromotion CreatePromotion = new ViewModelPromotion
            {

                SanPham = adProduct.ShowListProduct(),
                QuaTang = adPromotion.GetListGifts()
                //hedieuhanh = adProduct.GetListOperationSystem()
            };
            return View(CreatePromotion);
        }

        public ActionResult CreatePromotionSubmit(KhuyenMai KhuyenMai)
        {
            bool kq = false;
            ViewBag.Message = null;
            KhuyenMai.TrangThai = 1;
            kq = adPromotion.InsertPromotionDAO(KhuyenMai);
            if (kq)
            {
                return RedirectToAction("Index", new { ma = 8 });
            }
            else
            {
                return RedirectToAction("CreatePromotion", new { ma = 11 });
            }
        }

        public ActionResult DeletePromotion(int MaKhuyenMai)
        {
            bool kq = false;
            ViewBag.Message = null;
            kq = adPromotion.DeletePromotionDAO(MaKhuyenMai);
            if (kq)
            {
                return RedirectToAction("Index", new { ma = 2 });
            }
            else
            {
                return RedirectToAction("Index", new { ma = 3 });
            }
        }

    }
}
