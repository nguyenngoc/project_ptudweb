﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using MobileWeb.Filters;
using MobileWeb.Models;
using System.Net.Mail;
using MobileWebBLL.BLL;
using MobileWeb.App_Code;


namespace MobileWeb.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class ManageAccountController : Controller
    {
        //
        // GET: /ManageAccount/

        public ActionResult ProfileUser()
        {
            KhachHangBLL kh = new KhachHangBLL();
            var cus = kh.GetCustomerFromUsername(User.Identity.Name);
            ProfileUserParts model = new ProfileUserParts {
            TenKH = cus.TenKH,
            DiaChi = cus.DiaChi,
            DienThoai = cus.DienThoai,
            Email = cus.Email};
            return View(model);
        }
        
        [HttpPost]
        public ActionResult ProfileUser(ProfileUserParts model)
        {
            KhachHangBLL kh = new KhachHangBLL();
            if (kh.UpdateKhachHang(User.Identity.Name, model.TenKH, model.DiaChi, model.Email, model.DienThoai) != 1)
            {
                ViewData["message"] = "Rất tiếc có một sự cố đã xảy ra. Vui lòng thử lại!!";
            }
            return View(model);
        }

        public ActionResult Transaction(int? page)
        {
            ViewBag.Page = page ?? 1;
            return View();
        }

        public ActionResult ChangePageTransaction(int page)
        {
            HoaDonBLL hd = new HoaDonBLL();
            KhachHangBLL kh = new KhachHangBLL();
            List<MobileWebBLL.Models.HoaDon> list = hd.GetHDFromMaKH(kh.GetMaKHFromUserName(User.Identity.Name), page, 5);
            ViewData["pagetrans"] = page;
            List<MobileWebBLL.Models.HoaDon> listall = hd.GetHDFromMaKH(kh.GetMaKHFromUserName(User.Identity.Name), 1, 0);
            ViewData["pagestrans"] = hd.CountPagesHoaDon(listall, 5);
            ViewData["trans"] = listall.Count;
            return PartialView("_PartialTransaction",list);
        }

        public ActionResult DetailTransaction(int MaHD, int? page)
        {
            ViewBag.Page = page ?? 1;
            HoaDonBLL hd = new HoaDonBLL();
            return View(hd.GetHDFromMaHD(MaHD));
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }
        //
        // GET: /Account/Manage
        public ActionResult ChangePassword(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Mật khẩu của bạn đã được thay đổi."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : "";
            ViewBag.HasLocalPassword = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.ReturnUrl = Url.Action("ChangePassword");
            return View();
        }

        //
        // POST: /Account/Manage

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(LocalPasswordModel model)
        {
            bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.HasLocalPassword = hasLocalAccount;
            ViewBag.ReturnUrl = Url.Action("ChangePassword");
            if (hasLocalAccount)
            {
                if (ModelState.IsValid)
                {
                    // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                    bool changePasswordSucceeded;
                    try
                    {
                        changePasswordSucceeded = WebSecurity.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword);
                    }
                    catch (Exception)
                    {
                        changePasswordSucceeded = false;
                    }

                    if (changePasswordSucceeded)
                    {
                        return RedirectToAction("ChangePassword", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        ModelState.AddModelError("", "Mật khẩu hiện tại không chính xác hay Mật khẩu mới không phù hợp.");
                    }
                }
            }
            else
            {
                // User does not have a local password so remove any validation errors caused by a missing
                // OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        WebSecurity.CreateAccount(User.Identity.Name, model.NewPassword);
                        return RedirectToAction("ChangePassword", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    catch (Exception e)
                    {
                        ModelState.AddModelError("", e);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }
    }
}
