﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileWeb.ViewModels;
using MobileWebBLL.BLL;
using MobileWebBLL.Models;

using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using MobileWeb.Filters;
using MobileWeb.Models;

namespace MobileWeb.Controllers
{
    [Authorize(Roles = "Administrator")]
    [InitializeSimpleMembership]
    public class adminKhachHangController : Controller
    {
        //
        // GET: /adminKhachHang/
        public KhachHangBLL adkhachhang = new KhachHangBLL();
        public UserProfileBLL aduser = new UserProfileBLL();
        MobileWebEntities webmob = new MobileWebEntities();

        public ActionResult Index(int? ma)
        {
            int message = ma ?? 0;
            switch (message)
            {
                case 1: ViewBag.Message = "Cập nhật thành công"; break;
                case 2: ViewBag.Message = "Xóa thành công"; break;
                case 3: ViewBag.Message = "Xóa thất bại. Xin vui lòng thử lại sau"; break;
            }
            List<KhachHang> kh = adkhachhang.GetListCustomer();
            return View(kh);
        }

        public ActionResult ShowEditCustomer(int MaKH, int? ma)
        {
            int message = ma ?? 0;
            if (message == 10)
            {
                ViewBag.Message = "Cập nhật thất bại. Xin vui lòng thử lại sau";
            }
            KhachHang kh = adkhachhang.GetOneCustomer(MaKH);
            ViewModelCustomerInfo khp = new ViewModelCustomerInfo
            {
                MaKH = kh.MaKH,
                TenKH = kh.TenKH,
                Username = kh.UserProfile.UserName,
                DiaChi = kh.DiaChi,
                Email = kh.Email,
                DienThoai = kh.DienThoai
            };
            return View(khp);
        }

        public ActionResult EditCustomer(ViewModelCustomerInfo ViewModelCustomerInfo)
        {
            bool kq = false;
            ViewBag.Message = null;
            KhachHang kh = adkhachhang.GetOneCustomer(ViewModelCustomerInfo.MaKH);
            kh.TenKH = ViewModelCustomerInfo.TenKH;
            kh.DiaChi = ViewModelCustomerInfo.DiaChi;
            kh.UserProfile.UserName = ViewModelCustomerInfo.Username;
            kh.DienThoai = ViewModelCustomerInfo.DienThoai;
            kh.Email = ViewModelCustomerInfo.Email;
            kq = adkhachhang.UpdateCustomer(kh);

            if (kq)
            {
                return RedirectToAction("Index", new { ma = 1 });
            }
            else
            {
                return RedirectToAction("ShowEditCustomer", new { MaKH = ViewModelCustomerInfo.MaKH, ma = 10 });
            }
        }

        
        public ActionResult DeleteCustomer(int MaKH, string UserName)
        {
            bool kq = false;
            ViewBag.Message = null;
                // TODO: Add delete logic here
                KhachHang kh = adkhachhang.GetOneCustomer(MaKH);
                int userid = int.Parse(kh.UserId.ToString());
                kq = adkhachhang.DeleteCustomer(MaKH);
                Roles.RemoveUserFromRole(UserName, "Administrator");
                Membership.DeleteUser(UserName);

                if (kq)
                {
                    return RedirectToAction("Index", new { ma = 2 });
                }
                else
                {
                    return RedirectToAction("Index", new { ma = 3 });
                }
        }
    }
}
