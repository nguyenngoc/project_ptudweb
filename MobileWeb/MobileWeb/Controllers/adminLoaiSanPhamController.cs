﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileWeb.ViewModels;
using MobileWebBLL.BLL;
using MobileWebBLL.Models;

using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using System.Transactions;
using System.Web.Security;
using WebMatrix.WebData;

using MobileWeb.Filters;
using MobileWeb.Models;

namespace MobileWeb.Controllers
{
    [Authorize(Roles = "Administrator")]
    [InitializeSimpleMembership]
    public class adminLoaiSanPhamController : Controller
    {
        //
        // GET: /adminLoaiSanPham/
        MobileWebEntities webmob = new MobileWebEntities();
        public LoaiSanPhamBLL adLoaiSanPham = new LoaiSanPhamBLL();
        public ActionResult Index()
        {
            var loaisp = adLoaiSanPham.ShowListLoaiSanPham();
            return View(loaisp);
        }
        public ActionResult CreateLoaiSP()
        {
            return View();
        }
        public ActionResult CreateLoaiSPSubmit(LoaiSanPham LoaiSanPham)
        {
            adLoaiSanPham.CreateLoaiSanPhamDAO(LoaiSanPham);
            return RedirectToAction("Index");
        }

        public ActionResult ShowEditLoaiSP(Int16 MaLoaiSP)
        {
            var lsp = adLoaiSanPham.ShowEditLoaiSanPhamDAO(MaLoaiSP);
            return View(lsp);
        }

        public ActionResult EditLoaiSP(LoaiSanPham LoaiSanPham)
        {
            adLoaiSanPham.EditLoaiSanPhamDAO(LoaiSanPham);

            return RedirectToAction("Index");
        }

        public ActionResult DeleteLoaiSP(Int16 MaLoaiSP)
        {
            adLoaiSanPham.DeleteLoaiSanPhamDAO(MaLoaiSP);
            return RedirectToAction("Index");
        }
    }
}
