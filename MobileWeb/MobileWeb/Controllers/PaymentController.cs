﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Security;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Mvc;
using WebMatrix.WebData;
using MobileWeb.Models;
using MobileWeb.Filters;
using MobileWeb.App_Code;
using MobileWebBLL.BLL;

namespace MobileWeb.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class PaymentController : Controller
    {
        //
        // GET: /Payment/
        public ActionResult Index()
        {
            decimal VND = 0;
            decimal USD = 0;
            foreach (var item in ShoppingCartController.shoppingcart)
            {
                VND += (item.SoLuong * item.DonGia);
            }
            USD = ConvertGOOG(VND * 1000, "VND", "USD");
            KhachHangBLL kh = new KhachHangBLL();
            var cus = kh.GetCustomerFromUsername(User.Identity.Name);
            Bill bill = new Bill
            {
                TongTienUSD = USD,
                TongTienVND = VND * 1000,
                TenKHDat = cus.TenKH,
                DienThoai = cus.DienThoai,
                DiaChiGiaoHang = cus.DiaChi,
                HinhThucGiaoHang = 1,
                HinhThucThanhToan = 1,
                MaKH = cus.MaKH
            };
            return View(bill);
        }

        public ActionResult _LoadChiTietHD()
        {
            return PartialView("_PartialChiTietHD", ShoppingCartController.shoppingcart);
        }

        public ActionResult PostToPayPal(Bill billparts)
        {
            HoaDonBLL hd = new HoaDonBLL();
            long MaHD = hd.AddHoaDon(ShoppingCartController.shoppingcart, billparts.TenKHDat,
                billparts.DienThoai, billparts.DiaChiGiaoHang, billparts.HinhThucGiaoHang,
                billparts.HinhThucThanhToan, int.Parse((billparts.TongTienVND/1000).ToString()), billparts.MaKH);
            if (billparts.HinhThucThanhToan == 1)
            {
                Paypal paypal = new Paypal();
                paypal.cmd = "_xclick";
                paypal.business = ConfigurationManager.AppSettings["BusinessAccountKey"];

                bool useSandbox = Convert.ToBoolean(ConfigurationManager.AppSettings["UseSandbox"]);
                if (useSandbox)
                    ViewBag.actionURl = "https://www.sandbox.paypal.com/cgi-bin/webscr";
                else
                    ViewBag.actionURl = "https://www.paypal.com/cgi-bin/webscr";

                paypal.cancel_return = MembershipHelper.GetBaseUrl() + "ShoppingCart";
                paypal.@return = MembershipHelper.GetBaseUrl() + "Payment/PayPalSuccess?digest=" + EncryptCode(MaHD, billparts.MaKH, 60 * 24); //+"&PaymentId=1"; you can append your order Id here
                paypal.notify_url = MembershipHelper.GetBaseUrl();// +"?PaymentId=1"; to maintain database logic 

                paypal.currency_code = ConfigurationManager.AppSettings["CurrencyCode"];

                paypal.item_name = String.Format("TiniVippro - MaKH:{0} - MaHD:{1}", billparts.MaKH, MaHD);
                paypal.amount = billparts.TongTienUSD.ToString(); ;
                return View(paypal);
            }
            ShoppingCartController.shoppingcart.RemoveAll(p => 1 == 1);
            return RedirectToAction("Index", "Message", new { ma = 5 });
        }

        public ActionResult PayPalUser(long MaHD, int MaKH, int TongTien)
        {
            Paypal paypal = new Paypal();
            paypal.cmd = "_xclick";
            paypal.business = ConfigurationManager.AppSettings["BusinessAccountKey"];

            bool useSandbox = Convert.ToBoolean(ConfigurationManager.AppSettings["UseSandbox"]);
            if (useSandbox)
                ViewBag.actionURl = "https://www.sandbox.paypal.com/cgi-bin/webscr";
            else
                ViewBag.actionURl = "https://www.paypal.com/cgi-bin/webscr";

            paypal.cancel_return = MembershipHelper.GetBaseUrl() + "ShoppingCart";
            paypal.@return = MembershipHelper.GetBaseUrl() + "Payment/PayPalSuccess?digest=" + EncryptCode(MaHD, MaKH, 60 * 24); //+"&PaymentId=1"; you can append your order Id here
            paypal.notify_url = MembershipHelper.GetBaseUrl();// +"?PaymentId=1"; to maintain database logic 

            paypal.currency_code = ConfigurationManager.AppSettings["CurrencyCode"];

            paypal.item_name = String.Format("TiniVippro - MaKH:{0} - MaHD:{1}", MaKH, MaHD);
            paypal.amount = ConvertGOOG(decimal.Parse((TongTien*1000).ToString()),"VND","USD").ToString();
            return View(paypal);
        }

        public ActionResult PayPalSuccess(string digest)
        {
            var parts = ValidateResetCode(digest);
            if (!parts.IsValid)
            {
                return RedirectToAction("NotFound", "Error");
            }
            HoaDonBLL hd = new HoaDonBLL();
            hd.UpdateStatusHoaDon(parts.MaHD, 2, 1);
            return RedirectToAction("Index", "Message", new { ma = 4 }); ;
        }

        public decimal ConvertGOOG(decimal amount, string fromCurrency, string toCurrency)
        {
            WebClient web = new WebClient();
            string url = string.Format("http://www.google.com/ig/calculator?hl=en&q={2}{0}%3D%3F{1}", fromCurrency.ToUpper(), toCurrency.ToUpper(), amount);
            string response = web.DownloadString(url);
            Regex regex1 = new Regex(@"\s+");
            string outputString = regex1.Replace(response, "");
            response = outputString;
            Regex regex = new Regex("rhs:\\\"(\\d*.\\d*)");
            string st = regex.Match(response).Groups[1].Value;
            decimal rate = decimal.Parse(st);
            return rate;
        }

        public PaymentParts ValidateResetCode(string encryptedParam)
        {
            string decrypted = "";
            var results = new PaymentParts { IsValid = false};
            try
            {
                decrypted = Encryption.Decrypt(encryptedParam, null);
            }
            catch (Exception ex)
            {
                return results;
            }
            var parts = decrypted.Split('&');

            if (parts.Length != 3)
                return results;

            var expires = DateTime.Now.AddHours(-1);

            results.MaHD = long.Parse(parts[0]);
            results.MaKH = int.Parse(parts[1]);
            HoaDonBLL hd = new HoaDonBLL();
            if (!hd.CheckHoaDon(results.MaHD, results.MaKH))
                return results;

            long ticks = 0;
            if (!long.TryParse(parts[2], out ticks)) 
                return results;
            expires = new DateTime(ticks);
            results.Expires = expires;

            if (expires < DateTime.Now) 
                return results;
            results.IsValid = true;
            return results;
        }

        public string EncryptCode(long MaHD, int MaKH, int min)
        {
            string encrypted = MobileWeb.App_Code.Encryption.Encrypt(String.Format("{0}&{1}&{2}",
                MaHD,MaKH,
                DateTime.Now.AddMinutes(min).Ticks),
                null);
            return encrypted;
        }
    }
}
