﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileWeb.ViewModels;
using MobileWebBLL.Models;
using MobileWebBLL.BLL;

namespace MobileWeb.Controllers
{
    public class ProductController : Controller
    {
        ProductBLL sanpham = new ProductBLL();
        LoaiSanPhamBLL loaisanpham = new LoaiSanPhamBLL();
        NhaSanXuatBLL nhasanxuat = new NhaSanXuatBLL();
        ChiTietSanPhamBLL ctsanpham = new ChiTietSanPhamBLL();
        //
        // GET: /Product/

        public ActionResult Mobile(string category, string carrier, int? page, int? sort)
        {
            Session["Search"] = null;
                              
            ViewData["category"] = category;
            ViewData["carrier"] = carrier;

            var loaisanphams = loaisanpham.ListLoaiSanPham();
            var nhasanxuats = nhasanxuat.ListSearchNhaSanXuat();

            var product = new ViewModelProduct
            {
                Search = new ViewModelSearch
                {
                    SanPham = new SanPham(),
                    LoaiSanPhams = AddFirstItem(new SelectList(loaisanphams, "MaLoaiSP", "TenLoaiSP")),
                    NhaSanXuats = AddFirstItem(new SelectList(nhasanxuats, "MaNSX", "TenNSX"))
                }

            };
            return View(product);
        }

        public ActionResult Search(int? sort, string appendedInputButton)
        {
            int ssort = (sort ?? -1);

            if (Session["Sort"] != null && ssort == -1)
                ssort = int.Parse(Session["Sort"].ToString());
            else if (ssort != -1)
                Session["Sort"] = ssort;

            SanPham Sanpham = new SanPham();
            if (appendedInputButton == null)
            {
                if (Sanpham.TenSP != null || Sanpham.DonGia != 0 || Sanpham.MaLoaiSP != 0 || Sanpham.MaNSX != 0)
                    Session["Search"] = Sanpham;
                else if (Session["Search"] == null)
                    Session["Search"] = new SanPham { MaLoaiSP = -1, MaNSX = -1, DonGia = 0, TenSP = "" };
            }
            else
            {
                Session["Search"] = new SanPham { MaLoaiSP = -1, MaNSX = -1, DonGia = 0, TenSP = appendedInputButton.ToString() };
            }
            SanPham sp = Session["Search"] as SanPham;
            Session["Search"] = sp;

            var loaisanphams = loaisanpham.ListLoaiSanPham();
            var nhasanxuats = nhasanxuat.ListSearchNhaSanXuat();

            // lấy danh sách sản phẩm
            var sanphams = sanpham.ShowDSSanPhamTheoTieuChi(sp);
            //Lấy số trang sản phẩm
            ViewData["pages"] = sanpham.CountPageProduct(sanphams);
            // sắp xếp lại sản phẩm theo chỉ số sort
            sanphams = sanpham.SortProduct(sanphams, 1, ssort);

            ViewData["page"] = 1;
            ViewData["sort"] = ssort;

            var product = new ViewModelProduct
            {
                Sanphams = sanphams,
                Search = new ViewModelSearch
                {
                    SanPham = sp,
                    LoaiSanPhams = AddFirstItem(new SelectList(loaisanphams, "MaLoaiSP", "TenLoaiSP")),
                    NhaSanXuats = AddFirstItem(new SelectList(nhasanxuats, "MaNSX", "TenNSX"))
                }
            };
            return View(product);
        }

        public ActionResult SearchAjax(int? sort, SanPham Sanpham)
        {
            int ssort = (sort ?? -1);

            if (Session["Sort"] != null && ssort == -1)
                ssort = int.Parse(Session["Sort"].ToString());
            else if (ssort != -1)
                Session["Sort"] = ssort;
            if (Sanpham.TenSP != null || Sanpham.DonGia != 0 || Sanpham.MaLoaiSP != 0 || Sanpham.MaNSX != 0)
                Session["Search"] = Sanpham;
            else if (Session["Search"] == null)
                Session["Search"] = new SanPham { MaLoaiSP = -1, MaNSX = -1, DonGia = 0, TenSP = "" };

            SanPham sp = Session["Search"] as SanPham;
            Session["Search"] = sp;

            var loaisanphams = loaisanpham.ListLoaiSanPham();
            var nhasanxuats = nhasanxuat.ListSearchNhaSanXuat();

            // lấy danh sách sản phẩm
            var sanphams = sanpham.ShowDSSanPhamTheoTieuChi(sp);
            //Lấy số trang sản phẩm
            ViewData["pages"] = sanpham.CountPageProduct(sanphams);
            // sắp xếp lại sản phẩm theo chỉ số sort
            sanphams = sanpham.SortProduct(sanphams, 1, ssort);

            ViewData["page"] = 1;
            ViewData["sort"] = ssort;

            return PartialView("_PartialMobileSearch", sanphams);
        }

        public ActionResult ChangePage(string category, string carrier, int? page, int? sort)
        {
            Session["Search"] = null;
            int pageNumber = (page ?? 1);
            int ssort = (sort ?? -1);
            if (Session["Sort"] != null && ssort == -1)
                ssort = int.Parse(Session["Sort"].ToString());
            else if (ssort != -1)
                Session["Sort"] = ssort;
            // lấy danh sách sản phẩm
            List<SanPham> sanphams = sanpham.ShowDSSanPhamTheoLoaiSanPham(category, carrier);

            //Lấy số trang sản phẩm
            ViewData["pages"] = sanpham.CountPageProduct(sanphams);

            // sắp xếp lại sản phẩm theo chỉ số sort
            sanphams = sanpham.SortProduct(sanphams, pageNumber, ssort);

            // nếu ko yêu cầu sắp xếp thì sắp xếp theo mới nhất
            if (ssort == -1)
                ssort = 0;

            ViewData["page"] = pageNumber;
            ViewData["category"] = category;
            ViewData["carrier"] = carrier;
            ViewData["sort"] = ssort;
            return PartialView("_PartialMobile", sanphams);
        }

        public ActionResult ChangePageSearch(int? page, int? sort)
        {
            int ssort = (sort ?? -1);
            int pageNumber = (page ?? 1);
            if (Session["Sort"] != null && ssort == -1)
                ssort = int.Parse(Session["Sort"].ToString());
            else if (ssort != -1)
                Session["Sort"] = ssort;

            if (Session["Search"] == null)
                Session["Search"] = new SanPham { MaLoaiSP = -1, MaNSX = -1, DonGia = 0, TenSP = "" };
            SanPham sp = Session["Search"] as SanPham;
            Session["Search"] = sp;

            var loaisanphams = loaisanpham.ListLoaiSanPham();
            var nhasanxuats = nhasanxuat.ListSearchNhaSanXuat();
            // lấy danh sách sản phẩm
            var sanphams = sanpham.ShowDSSanPhamTheoTieuChi(sp);
            //Lấy số trang sản phẩm
            ViewData["pages"] = sanpham.CountPageProduct(sanphams);
            // sắp xếp lại sản phẩm theo chỉ số sort
            sanphams = sanpham.SortProduct(sanphams, pageNumber, ssort);

            ViewData["page"] = pageNumber;
            ViewData["sort"] = ssort;
            return PartialView("_PartialMobileSearch", sanphams);
        }

        private SelectList AddFirstItem(SelectList list)
        {
            List<SelectListItem> _list = list.ToList();
            _list.Insert(0, new SelectListItem() { Value = "-1", Text = "Tất cả" });
            return new SelectList((IEnumerable<SelectListItem>)_list, "Value", "Text");
        }

        CommentBLL comment = new CommentBLL();
        public ActionResult Detail(string category, string carrier, int id)
        {
            var vsanpham = sanpham.GetSanPham(id);
            sanpham.UpdateView(id);
            ViewModelDetailProduct detail = new ViewModelDetailProduct
            {
                Sanpham = vsanpham,
                ctSanpham = ctsanpham.GetChiTietSanPham(id),
                SanphamsDongGia = sanpham.ShowDSSanPhamTheoMucGia(id),
                SanphamsLienQuan = sanpham.ShowDSSanPhamLienQuan(id)
            };
            ViewData["GiaThat"] = new ProductBLL().GetGiaThatFromMaSP(id);
            ViewData["pagecomment"] = 1;
            ViewData["pagescomment"] = comment.CountPages(comment.GetCommentsFromProduct(vsanpham.MaSP, 1, 0), 5);
            ViewData["comments"] = comment.CountComments(comment.GetCommentsFromProduct(vsanpham.MaSP, 1, 0));
            return View(detail);
        }
    }
}
