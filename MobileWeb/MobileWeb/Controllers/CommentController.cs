﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileWebBLL.Models;
using MobileWebBLL.BLL;
using MobileWeb.ViewModels;

namespace MobileWeb.Controllers
{
    public class CommentController : Controller
    {
        //
        // GET: /Comment/
        CommentBLL comment = new CommentBLL();
        public ActionResult Comment(BinhLuan Binhluan, int page)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // Save Comment
                    comment.InsertCommentDAO(Binhluan);
                    Binhluan = new BinhLuan { MaSP = Binhluan.MaSP };
                    page = 1;
                    ViewData["message"] = "";
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(String.Empty, ex);
            }
            ViewModelComment Comment =
                new ViewModelComment
                {
                    BinhLuan = Binhluan,
                    BinhLuans = comment.GetCommentsFromProduct(Binhluan.MaSP, page, 5)
                };
            ViewData["pagecomment"] = page;
            ViewData["pagescomment"] = comment.CountPages(comment.GetCommentsFromProduct(Binhluan.MaSP, 1, 0),5);
            ViewData["comments"] = comment.CountComments(comment.GetCommentsFromProduct(Binhluan.MaSP, 1, 0));            
            return PartialView("_PartialComment", Comment);
        }

        public ActionResult ChangePageComments(long MaSP, int page)
        {
            ViewModelComment Comment =
                new ViewModelComment
                {
                    BinhLuan = new BinhLuan { MaSP = MaSP },
                    BinhLuans = comment.GetCommentsFromProduct(MaSP, page, 5)
                };
            ViewData["pagecomment"] = page;
            ViewData["pagescomment"] = comment.CountPages(comment.GetCommentsFromProduct(MaSP, 1, 0), 5);
            ViewData["comments"] = comment.CountComments(comment.GetCommentsFromProduct(MaSP, 1, 0));
            return PartialView("_PartialComment", Comment);
        }
    }
}
