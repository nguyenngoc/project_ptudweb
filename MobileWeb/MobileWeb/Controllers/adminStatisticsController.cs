﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileWebBLL.Models;
using MobileWebBLL.BLL;

using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using System.Transactions;
using System.Web.Security;
using WebMatrix.WebData;

using MobileWeb.App_Code;
using MobileWeb.Filters;
using MobileWeb.Models;

namespace MobileWeb.Controllers
{

    [Authorize(Roles = "Administrator")]
    [InitializeSimpleMembership]
    public class adminStatisticsController : Controller
    {
        //
        // GET: /adminStatistics/

        public ActionResult Index()
        {
            return View(new List<StatisticParts>());
        }

        [HttpPost]
        public ActionResult Index(string fromdate, string todate, int format)
        {
            ViewBag.FromDate = fromdate;
            ViewBag.ToDate = todate;
            ViewBag.Format = format;
            HoaDonBLL hd = new HoaDonBLL();
            return View(hd.Statistic(fromdate, todate, format));
        }

        public JsonResult GetAreaChartData(string fromdate, string todate, int format)
        {
            HoaDonBLL hd = new HoaDonBLL();
            var sta = hd.Statistic(fromdate,todate, format);
            List<string[]> data = new List<string[]>();
            if (format == 1)
                data.Add(new[] { "Ngày", "Doanh thu" });
            if (format == 2)
                data.Add(new[] { "Tháng", "Doanh thu" });
            if (format == 3)
                data.Add(new[] { "Năm", "Doanh thu" });
            foreach (var item in sta)
            {
                string str = "";
                if (item.Day != 0)
                {
                    str += item.Day + "/";
                }
                if (item.Month != 0)
                {
                    str += item.Month + "/";
                }
                if (item.Year != 0)
                {
                    str += item.Year;
                }
                data.Add(new[] { str, (item.Revenue*1000).ToString() });
            }
            return Json(data);
        }

    }
}
