﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileWebBLL.BLL;
using MobileWebBLL.Models;

using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using System.Transactions;
using System.Web.Security;
using WebMatrix.WebData;

using MobileWeb.App_Code;
using MobileWeb.Filters;
using MobileWeb.Models;

namespace MobileWeb.Controllers
{
    [Authorize(Roles = "Administrator")]
    [InitializeSimpleMembership]
    public class adminSliderController : Controller
    {
        //
        // GET: /adminSlider/
        SliderBLL adslider = new SliderBLL();

        public ActionResult Index(int? ma)
        {
            int message = ma ?? 0;
            switch (message)
            {
                case 2: ViewBag.Message = "Xóa thành công"; break;
                case 3: ViewBag.Message = "Xóa thất bại. Xin vui lòng thử lại sau"; break;
                case 8: ViewBag.Message = "Thêm thành công"; break;
                case 12: ViewBag.Message = "Thêm thất bại. Xin vui lòng thử lại sau"; break;
            }
            var sli = adslider.ShowSlider();
            return View(sli);
        }

        /*
        public string GetBaseUrl()
        {
            var request = HttpContext.Current.Request;
            var appUrl = HttpRuntime.AppDomainAppVirtualPath;
            var baseUrl = string.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Authority, appUrl);

            return baseUrl;
        }
        */

        [HttpPost]
        public ActionResult CreateSliderSubmit(Slider Slider, HttpPostedFileBase Link)
        {
            bool kq = false;
            ViewBag.Message = null;
            if (Link != null)
            {
                string filePath = Path.Combine(Server.MapPath("~/Images/home/mobiles/"), Path.GetFileName(Link.FileName));

                Link.SaveAs(filePath);
                Slider.Link = Link.FileName;
                kq = adslider.InsertSlider(Slider);
            }
            if (kq)
            {
                return RedirectToAction("Index", new { ma = 8 });
            }
            else
            {
                return RedirectToAction("Index", new { ma = 12 });
            }
            //Link.SaveAs(absFileAndPath);
        }

        public ActionResult DeleteSlider(int MaSlider)
        {
            bool kq = false;
            ViewBag.Message = null;
            kq = adslider.DeleteSilder(MaSlider);
            if (kq)
            {
                return RedirectToAction("Index", new { ma = 2 });
            }
            else
            {
                return RedirectToAction("Index", new { ma = 3 });
            }
        }
    }
}
