﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileWeb.Models;
using MobileWeb.ViewModels;
using MobileWebBLL.BLL;
using MobileWebBLL.Models;
using System.Net.Mail;

namespace MobileWeb.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        ProductBLL sanpham = new ProductBLL();
        NhaSanXuatBLL nsx = new NhaSanXuatBLL();
        LoaiSanPhamBLL lsp = new LoaiSanPhamBLL();
        SliderBLL slider = new SliderBLL();

        public ActionResult Index()
        {
            ProductBLL sp = new ProductBLL();
            var sliders = slider.ShowSlider();
            var sanphammoinhats = sp.ShowSanPhamMoiNhat();
            var sanphambanchays = sp.ShowDSSanPhamBanChay();
            var sanphamkhuyenmais = sp.ShowKhuyenMaiMoiNhat();
            ViewModelIndex pro = new ViewModelIndex
            {
                SanphamKhuyenmais = sanphamkhuyenmais,
                Sliders = sliders,
                SanphamMoinhaps = sanphammoinhats,
                SanphamBanchaynhats = sanphambanchays
            };
            return View(pro);
        }

        public ActionResult _Header()
        {
            var menu = new ViewModelSidebar
            {
                Carriers = nsx.ListNhaSanXuat(),
                Categories = lsp.ListLoaiSanPham()
            };
            return PartialView("_Header", menu);
        }

        public ActionResult _Footer()
        {
            List<LoaiSanPham> Categories = lsp.ListLoaiSanPham();
            return PartialView("_Footer", Categories);
        }

        public ActionResult _Sidebar()
        {
            var menu = new ViewModelSidebar
            {
                Carriers = nsx.ListNhaSanXuat(),
                Categories = lsp.ListLoaiSanPham()
            };
            return PartialView("_Sidebar2", menu);
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact(int? Message)
        {
            Message = Message ?? 0;
            if (Message == 1)
                ViewData["message"] = "Tin nhắn đã được gửi đi. Chúng tôi sẽ nhanh chóng hồi âm cho quý khách sớm nhất có thể!!";
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Contact(ContactParts parts)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    MailMessage mail = new MailMessage();
                    SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com", 587);
                    mail.From = new MailAddress("tinivippromobile@gmail.com");
                    mail.To.Add("tinivippromobile@gmail.com");
                    mail.Subject = "Liên hệ - TiniVippro";
                    mail.IsBodyHtml = true;
                    mail.Body += String.Format("Họ tên: <b>{0}</b><br/>Email: <b>{1}</b><br/>Nội dung: <pre style=\"font-size: 12px\">{2}</pre>", parts.Name, parts.Email, parts.Content);
                    SmtpServer.EnableSsl = true;
                    SmtpServer.UseDefaultCredentials = false;
                    var loginInfo = new System.Net.NetworkCredential("tinivippromobile@gmail.com", "tinivippro123");
                    SmtpServer.Credentials = loginInfo;
                    SmtpServer.Send(mail);
                    return RedirectToAction("Contact", new { Message = 1});
                }
                catch
                {
                    return View(parts);
                }
            }
            return View(parts);
        }

    }
}
