﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileWeb.ViewModels;
using MobileWebBLL.BLL;
using MobileWebBLL.Models;

using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using System.Transactions;
using System.Web.Security;
using WebMatrix.WebData;

using MobileWeb.App_Code;
using MobileWeb.Filters;
using MobileWeb.Models;

namespace MobileWeb.Controllers
{
    [Authorize(Roles = "Administrator")]
    [InitializeSimpleMembership]
    public class BestSellingProductsController : Controller
    {
        //
        // GET: /BestSellingProducts/
        public ProductBLL adProduct = new ProductBLL();
        public HoaDonBLL adBill = new HoaDonBLL();

        public ActionResult Index()
        {
            List<ViewModelBestSellingProduct> best = new List<ViewModelBestSellingProduct>();
            var products = adProduct.ShowListProduct();
            for (int i = 0; i < products.Count; i++)
            {
                ViewModelBestSellingProduct temp = new ViewModelBestSellingProduct
                {
                    MaSP = products[i].MaSP,
                    TenSP = products[i].TenSP,
                    LoaiSP = products[i].LoaiSanPham.TenLoaiSP,
                    NhaSX = products[i].NhaSanXuat.TenNSX,
                    DonGia = products[i].DonGia,
                    SoLuongBan = adBill.Soluong(products[i].MaSP)
                };
                best.Add(temp);
            }
            var result = best.OrderByDescending(item => item.SoLuongBan).Take(10).ToList();
            return View(result);
        }

    }
}
