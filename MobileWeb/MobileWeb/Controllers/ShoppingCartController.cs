﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileWebBLL.Models;
using MobileWebBLL.BLL;

namespace MobileWeb.Controllers
{
    public class ShoppingCartController : Controller
    {
        //
        // GET: /ShoppingCart/
        ProductBLL sanpham = new ProductBLL();
        public static List<SanPham> shoppingcart = new List<SanPham>();
        public static HashSet<int> already = new HashSet<int>();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult loadShoppingCart()
        {
            return PartialView("_ShoppingCart", shoppingcart);
        }

        public ActionResult AddShoppingCart(int id)
        {
            if (already.Add(id))
            {
                SanPham sp = sanpham.GetSanPham(id);
                sp.SoLuong = 1;
                shoppingcart.Add(sp);
            }
            else
            {
                SanPham sp = shoppingcart.SingleOrDefault(p => p.MaSP == id);
                sp.SoLuong = sp.SoLuong + 1;
            }
            return RedirectToAction("_ShoppingCart", "ShoppingCart");
        }

        public ActionResult _ShoppingCart()
        {
            return PartialView("_ShoppingCartImage", shoppingcart);
        }

        public ActionResult _upShoppingCart(int maSP, int amount)
        {
            List<SanPham> sc = shoppingcart;
            SanPham sp = sc.SingleOrDefault(p => p.MaSP == maSP);
            sp.SoLuong = sp.SoLuong + amount;
            if (sp.SoLuong == 0)
            {
                sc.RemoveAll(p=>p.MaSP == maSP);
                already.Remove(maSP);
            }
            return PartialView("_ShoppingCartImage", sc);
        }

        public ActionResult _deShoppingCart(int maSP)
        {
            List<SanPham> sc = shoppingcart;
            sc.RemoveAll(p => p.MaSP == maSP);
            already.Remove(maSP);
            return PartialView("_ShoppingCartImage", sc);
        }

        public ActionResult updateShoppingCart(int maSP, int amount)
        {
            /*SanPham sp = shoppingcart.FirstOrDefault(p => p.MaSP == maSP);
            int index = shoppingcart.IndexOf(sp);
            shoppingcart.RemoveAll(p => p.MaSP == maSP);            
            sp.SoLuong = sp.SoLuong + amount;
            if (sp.SoLuong == 0)
            {
                already.Remove(maSP);
            }
            else
            {
                shoppingcart.Insert(index, sp);
            }   */         
            return RedirectToAction("loadShoppingCart", "ShoppingCart");
        }

        public ActionResult deleteShoppingCart(int maSP)
        {
            /*already.Remove(maSP);
            shoppingcart.RemoveAll(p => p.MaSP == maSP);*/
            return RedirectToAction("loadShoppingCart", "ShoppingCart");
        }

    }
}
