﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Net.Mail;
using WebMatrix.WebData;
using MobileWeb.App_Code;
using MobileWeb.Models;
using MobileWebBLL.BLL;

namespace MobileWeb.Controllers
{
    public class ResetPasswordController : Controller
    {
        //
        // GET: /ResetPassword/

        [HttpGet]
        public ActionResult RequestResetPasswordLink()
        {
            return View();
        }
 
        [HttpPost]
        public ActionResult RequestResetPasswordLink(string userName)
        {
            if(string.IsNullOrEmpty(userName)) 
                return View();
 
            var existingUser = Membership.GetUser(userName);
            
            KhachHangBLL kh = new KhachHangBLL();
            if (existingUser == null && !kh.CheckMail(userName))
            {
                ViewData["message"] = "Tên đăng nhập hay Email không tồn tại!!";
                return View();
            }
            if (existingUser == null)
            {
                userName = kh.GetUserNameFromEmail(userName);
            }
            string email = kh.GetMailFromUserId(WebSecurity.GetUserId(userName));
 
            MembershipHelper.SendResetEmail(userName, email);
            return RedirectToAction("Index", "Message", new { ma = 2 });
        }

        [HttpGet]
        public ActionResult ResetPassword(string digest)
        {
            var parts = ValidateResetCode(HttpUtility.UrlDecode(digest));
 
            if (!parts.IsValid)
            {
                return RedirectToAction("NotFound", "Error");
            }
            MembershipUser user = Membership.GetUser(parts.UserName);
            string newPass = MembershipHelper.ResetPassword(user);
            KhachHangBLL kh = new KhachHangBLL();
            MembershipHelper.SendNewPasswordEmail(parts.UserName, newPass, kh.GetMailFromUserId(WebSecurity.GetUserId(parts.UserName)));
 
            return RedirectToAction("Index", "Message", new { ma = 3 });
        }

        public ResetPasswordParts ValidateResetCode(string encryptedParam)
        {
            string decrypted = "";
            var results = new ResetPasswordParts();

            try
            {
                decrypted = Encryption.Decrypt(encryptedParam, null);
            }
            catch (Exception ex)
            {
                return results;
            }

            var parts = decrypted.Split('&');

            if (parts.Length != 2) 
                return results;

            var expires = DateTime.Now.AddHours(-1);


            results.UserName = parts[0];
            if (Membership.GetUser(parts[0]) == null) 
                return results;

            long ticks = 0;
            if (!long.TryParse(parts[1], out ticks)) return results;
                expires = new DateTime(ticks);
            results.Expires = expires;

            if (expires < DateTime.Now) return results;
                results.IsValid = true;

            return results;
        }
    }
}
