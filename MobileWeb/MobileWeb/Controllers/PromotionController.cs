﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileWebBLL.Models;
using MobileWebBLL.BLL;

namespace MobileWeb.Controllers
{
    public class PromotionController : Controller
    {
        //
        // GET: /Promotion/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ChangePage(int? page, int? sort)
        {
            ProductBLL sanpham = new ProductBLL();
            Session["Search"] = null;
            int pageNumber = (page ?? 1);
            int ssort = (sort ?? -1);
            if (Session["Sort"] != null && ssort == -1)
                ssort = int.Parse(Session["Sort"].ToString());
            else if (ssort != -1)
                Session["Sort"] = ssort;
            // lấy danh sách sản phẩm
            List<ProductM> sanphams = sanpham.ShowDanhMucKhuyenMai();

            //Lấy số trang sản phẩm
            ViewData["pages"] = sanpham.CountPageProductM(sanphams, 9);

            // sắp xếp lại sản phẩm theo chỉ số sort
            sanphams = sanpham.SortProductM(sanphams, pageNumber, ssort, 9);

            // nếu ko yêu cầu sắp xếp thì sắp xếp theo mới nhất
            if (ssort == -1)
                ssort = 0;

            ViewData["page"] = pageNumber;
            ViewData["sort"] = ssort;
            return PartialView("_PartialPromotion", sanphams);
        }

    }
}
