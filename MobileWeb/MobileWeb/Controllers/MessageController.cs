﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MobileWeb.Controllers
{
    public class MessageController : Controller
    {
        //
        // GET: /Message/

        public ActionResult Index(int ma)
        {
            if (ma == 1)
                ViewData["message"] = "Chúng tôi đã gửi link kích hoạt tài khoản đến địa chỉ mail mà bạn đã đăng kí. Vui lòng nhấp vào link kích hoạt trong vòng 60 phút kể từ lúc đăng kí để kích hoạt tài khoản!!";
            if (ma == 2)
                ViewData["message"] = "Chúng tôi đã gửi link đổi mật khẩu tài khoản đến địa chỉ mail mà bạn đã đăng kí. Vui lòng nhấp vào link đổi mật khẩu trong vòng 60 phút kể từ lúc này để đổi mật tài khoản!!";
            if (ma == 3)
                ViewData["message"] = "Thông tin tài khoản và mật khẩu mới đã được gửi đến Email của bạn. Vui lòng kiểm tra!!";
            if (ma == 4)
                ViewData["message"] = "Cám ơn bạn đã đặt hàng tại cửa hàng của chúng tôi!! Mọi chi tiết đặt hàng vui lòng kiểm tra tại Thông tin giao dịch của Quý khách. Sau khi kiểm tra thông tin chúng tôi sẽ nhanh chóng gửi mail để xác nhận cho quý khách trong vòng 24 tiếng";
            if (ma == 5)
                ViewData["message"] = "Cám ơn bạn đã đặt hàng tại cửa hàng của chúng tôi!! Quý khách đã chọn hình thức thanh toán là Thanh toán khi giao hàng. Mọi chi tiết đặt hàng vui lòng kiểm tra tại Thông tin giao dịch của Quý khách. Chúng tôi sẽ liên hệ với quý khách trong vòng 24 tiếng để xác nhận yêu cầu!!";
            return View();
        }

    }
}
