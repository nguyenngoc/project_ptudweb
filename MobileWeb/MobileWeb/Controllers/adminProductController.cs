﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileWeb.ViewModels;
using MobileWebBLL.BLL;
using MobileWebBLL.Models;
using System.IO;

using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using System.Transactions;
using System.Web.Security;
using WebMatrix.WebData;

using MobileWeb.Filters;
using MobileWeb.Models;

namespace MobileWeb.Controllers
{
    [Authorize(Roles = "Administrator")]
    [InitializeSimpleMembership]
    public class adminProductController : Controller
    {
        //
        // GET: /adminProduct/

        MobileWebEntities webmob = new MobileWebEntities();
        public ProductBLL adProduct = new ProductBLL();
        public NhaSanXuatBLL nsxBLL = new NhaSanXuatBLL();
        public LoaiSanPhamBLL lspBLL = new LoaiSanPhamBLL();
        public HinhAnhBLL imgBLL = new HinhAnhBLL();

        public ActionResult Index()
        {
            //var products = webmob.SanPhams.Where(p => p.TrangThai ==1);
            //products.OrderBy(p => p.MaNSX);
            List<SanPham> products = adProduct.ShowListProduct();
            return View(products);
        }
        public ActionResult adminShowDetailProduct(int MaSP)
        {

            List<ChiTietSanPham> ctsp = adProduct.ShowDetailsProduct(MaSP);
            List<HinhAnh> img = imgBLL.GetHinhAnh(MaSP);
            ViewBag.img = img;
            return View(ctsp);
        }
        public ActionResult CreateProduct()
        {
            ViewModelAdminProducts CreateProduct = new ViewModelAdminProducts
            {

                loaisp = adProduct.GetListCategories(),
                nhasanxuat = adProduct.GetListCompany(),
                //hedieuhanh = adProduct.GetListOperationSystem()
            };
            ViewBag.lsp = lspBLL.ShowListLoaiSanPham();
            ViewBag.nsx = nsxBLL.ListNhaSanXuat();
            return View(CreateProduct);
        }
        public ActionResult CreateProductSubmit(SanPham SanPham, ChiTietSanPham ChiTietSanPham)
        {
            adProduct.CreateroductDAO(SanPham, ChiTietSanPham);
            var MaSP = adProduct.GetLastProduct().MaSP;
            return RedirectToAction("CreateProductImages", new { MaSP = MaSP });
        }

        /*public ActionResult CreateProduct()
        {
            ViewModelAdminProducts CreateProduct = new ViewModelAdminProducts
            {

                loaisp = adProduct.GetListCategories(),
                nhasanxuat = adProduct.GetListCompany(),
                //hedieuhanh = adProduct.GetListOperationSystem()
            };
            ViewBag.lsp = lspBLL.ShowListLoaiSanPham();
            ViewBag.nsx = nsxBLL.ListNhaSanXuat();
            return View(CreateProduct);
        }

        [HttpPost]
        public ActionResult CreateProduct(SanPham SanPham, ChiTietSanPham ChiTietSanPham)
        {
            adProduct.CreateroductDAO(SanPham, ChiTietSanPham);
            var MaSP = adProduct.GetLastProduct().MaSP;
            ViewModelAdminProducts CreateProduct = new ViewModelAdminProducts
            {

                loaisp = adProduct.GetListCategories(),
                nhasanxuat = adProduct.GetListCompany(),
                SanPham = SanPham,
                ChiTietSanPham = ChiTietSanPham
                //hedieuhanh = adProduct.GetListOperationSystem()
            };
            ViewBag.lsp = lspBLL.ShowListLoaiSanPham();
            ViewBag.nsx = nsxBLL.ListNhaSanXuat();
            return View(CreateProduct);
        }*/

        public ActionResult CreateProductImages(int MaSP)
        {
            List<HinhAnh> img = imgBLL.GetHinhAnh(MaSP);
            ViewBag.img = img;
            ViewBag.masp = MaSP;
            return View();
        }

        public ActionResult ShowEidtPruduct(int MaSP, int? Message)
        {
            int mess = Message ?? 0;
            if (mess != 0)
                ViewBag.Mess = "Cập nhật thành công!!";
            List<HinhAnh> img = imgBLL.GetHinhAnh(MaSP);
            ViewBag.img = img;
            ViewBag.lsp = lspBLL.ShowListLoaiSanPham();
            ViewBag.nsx = nsxBLL.ListNhaSanXuat();
            ViewModelAdminProducts editProduct = new ViewModelAdminProducts
            {
                SanPham = adProduct.GetOneProduct(MaSP),
                ChiTietSanPham = adProduct.ShowDetailProduct(MaSP),
                loaisp = adProduct.GetListCategories(),
                nhasanxuat = adProduct.GetListCompany(),
                HinhAnh = new HinhAnh { MaSP = MaSP }
                //hedieuhanh = adProduct.GetListOperationSystem()
            };
            return View(editProduct);
        }

        public ActionResult EditProduct(SanPham SanPham, ChiTietSanPham ChiTietSanPham)
        {
            adProduct.EditProductDAO(SanPham, ChiTietSanPham);

            return RedirectToAction("ShowEidtPruduct", new { MaSP = SanPham.MaSP, Message = 1 });
        }

        [HttpPost]
        public ActionResult InsertHinh(HinhAnh HinhAnh, HttpPostedFileBase linkhinh)
        {
            if (linkhinh != null)
            {
                string filePath = Path.Combine(Server.MapPath("~/Images/home/mobiles/"), Path.GetFileName(linkhinh.FileName));

                linkhinh.SaveAs(filePath);
                HinhAnh.Link = linkhinh.FileName;
                imgBLL.InsertHinh(HinhAnh);
                return RedirectToAction("ShowEidtPruduct", new { MaSP = HinhAnh.MaSP });
            }
            else
            {
                return RedirectToAction("Index");
            }

            //Link.SaveAs(absFileAndPath);
            //return RedirectToAction("CreateProduct");
        }

        public ActionResult DeleteHinh(int MaHinhAnh, int MaSP)
        {
            imgBLL.DeleteHinh(MaHinhAnh);
            return RedirectToAction("ShowEidtPruduct", new { MaSP = MaSP });
        }

        public ActionResult DeleteProduct(int MaSP)
        {
            adProduct.DeleteProductDAO(MaSP);
            return RedirectToAction("Index");
        }

        public ActionResult LoadUploadHinh(int MaSP)
        {
            List<HinhAnh> img = imgBLL.GetHinhAnh(MaSP);
            ViewBag.img = img;
            ViewBag.lsp = lspBLL.ShowListLoaiSanPham();
            ViewBag.nsx = nsxBLL.ListNhaSanXuat();
            ViewModelAdminProducts editProduct = new ViewModelAdminProducts
            {
                SanPham = adProduct.GetOneProduct(MaSP),
                ChiTietSanPham = adProduct.ShowDetailProduct(MaSP),
                loaisp = adProduct.GetListCategories(),
                nhasanxuat = adProduct.GetListCompany(),
                HinhAnh = new HinhAnh { MaSP = MaSP }
                //hedieuhanh = adProduct.GetListOperationSystem()
            };
            return PartialView("_PartialUploadAnh", editProduct);
        }
        
    }
}
