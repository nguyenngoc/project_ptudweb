﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileWeb.ViewModels;
using MobileWebBLL.BLL;
using MobileWebBLL.Models;

using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using System.Transactions;
using System.Web.Security;
using WebMatrix.WebData;

using MobileWeb.Filters;
using MobileWeb.Models;

namespace MobileWeb.Controllers
{
    [Authorize(Roles = "Administrator")]
    [InitializeSimpleMembership]
    public class adminNhaSanXuatController : Controller
    {
        //
        // GET: /adminNhaSanXuat/
        public NhaSanXuatBLL nsx = new NhaSanXuatBLL();
        public LoaiSanPhamBLL lsp = new LoaiSanPhamBLL();

        public ActionResult Index()
        {
            var n = nsx.ListNhaSanXuat();
            return View(n);
        }

        public ActionResult CreateNSX()
        {
            ViewBag.listlsp = lsp.ShowListLoaiSanPham();
            return View();
        }

        public ActionResult CreateNhaSanXuatSubmit(NhaSanXuat n)
        {
            nsx.CreateNhaSanXuatDAO(n);
            return RedirectToAction("Index");
        }

        public ActionResult ShowEditNSX(short MaNSX)
        {
            ViewBag.listlsp = lsp.ShowListLoaiSanPham();
            var n = nsx.ShowEditNhaSanXuatDAO(MaNSX);
            return View(n);
        }

        public ActionResult EditNSX(NhaSanXuat n)
        {
            
            nsx.EditNhaSanXuatDAO(n);

            return RedirectToAction("Index");
        }

        public ActionResult DeleteNSX(short MaNSX)
        {
            nsx.DeleteNhaSanXuatDAO(MaNSX);
            return RedirectToAction("Index");
        }
    }
}
