﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileWeb.ViewModels;
using MobileWebBLL.BLL;
using MobileWebBLL.Models;

using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using System.Transactions;
using System.Web.Security;
using WebMatrix.WebData;

using MobileWeb.App_Code;
using MobileWeb.Filters;
using MobileWeb.Models;

namespace MobileWeb.Controllers
{
    [Authorize(Roles = "Administrator")]
    [InitializeSimpleMembership]
    public class adminCommentController : Controller
    {
        //
        // GET: /adminComment/

        MobileWebEntities webmob = new MobileWebEntities();
        public CommentBLL adComment = new CommentBLL();
        public ProductBLL adProduct = new ProductBLL();

        
        public ActionResult Index(int? ma)
        {
            int message = ma ?? 0;
            switch (message)
            {
                case 1: ViewBag.Message = "Cập nhật thành công"; break;
                case 2: ViewBag.Message = "Xóa thành công"; break;
                case 3: ViewBag.Message = "Xóa thất bại. Xin vui lòng thử lại sau"; break;
            }
            var comments = adComment.ShowListComments();
            return View(comments);
        }
        public ActionResult ShowEditComment(int MaBL, int? ma)
        {
            int message = ma ?? 0;
            if(message == 10)
            {
                ViewBag.Message = "Cập nhật thất bại. Xin vui lòng thử lại sau";
            }
            ViewModelAdminComment editComment = new ViewModelAdminComment
            {
                BinhLuan = adComment.GetOneComment(MaBL),
                sanpham = adProduct.ShowListProduct()
            };
            return View(editComment);
        }

        public ActionResult EditComment(BinhLuan BinhLuan)
        {
            bool kq = false;

            kq = adComment.EditCommentDAO(BinhLuan);
            if (kq)
            {
                return RedirectToAction("Index", new {ma = 1 });
            }
            else
            {
                return RedirectToAction("ShowEditComment", new { MaBL = BinhLuan.MaBinhLuan, ma = 10 });
            }
        }

        public ActionResult DeleteComment(int MaBL, int? ma)
        {
            bool kq = false;
            kq = adComment.DeleteCommentDAO(MaBL);
            if (kq)
            {
                return RedirectToAction("Index", new { ma = 2 });
            }
            else
            {
                return RedirectToAction("Index", new { ma = 3 });
            }
            
        }

    }
}
