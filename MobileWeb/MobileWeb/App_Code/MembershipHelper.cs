﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Configuration;
using System.Text.RegularExpressions;
using MobileWeb.Models;
using System.Net.Mail;
using WebMatrix.WebData;

namespace MobileWeb.App_Code
{
    public class MembershipHelper
    {
        public static double _minExpires = 120;
        public static string FriendlyPassword()
        {
            string newPassword = Membership.GeneratePassword(8, 0);
            newPassword = Regex.Replace(newPassword, @"[^a-zA-Z0-9]", m => "9");
            return newPassword;
        }

        public static string ResetPassword(MembershipUser user)
        {
            string newPass = FriendlyPassword();
            var token = WebSecurity.GeneratePasswordResetToken(user.UserName);
            bool response = WebSecurity.ResetPassword(token, newPass);
            return newPass;
        }

        public static void SendResetEmail(string username, string email)
        {
            string encrypted = MobileWeb.App_Code.Encryption.Encrypt(String.Format("{0}&{1}",
                username,
                DateTime.Now.AddMinutes(_minExpires).Ticks),
                null);

            var passwordLink = MembershipHelper.GetBaseUrl() +
                  "ResetPassword/ResetPassword?digest=" +
                  HttpUtility.UrlEncode(encrypted);
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com", 587);

            mail.From = new MailAddress("tinivippromobile@gmail.com");
            mail.To.Add(email);
            mail.Subject = "Thay đổi mật khẩu - TiniVippro";
            mail.IsBodyHtml = true;
            mail.Body += "<p>Nếu bạn không phải là người muốn thay đổi mật khẩu tại TiniVippro, vui lòng bỏ qua Email.</p>";
            mail.Body += "<p>Vui lòng nhấp vào link sau đây để thay đổi mật khẩu tại TiniVippro:<a href='" + passwordLink + "'>" + passwordLink + "</a></p>";
            SmtpServer.EnableSsl = true;
            SmtpServer.UseDefaultCredentials = false;
            var loginInfo = new System.Net.NetworkCredential("tinivippromobile@gmail.com", "tinivippro123");
            SmtpServer.Credentials = loginInfo;
            SmtpServer.Send(mail);
        }

        public static void SendEnableUser(string username, string password, string email)
        {
            string encrypted = MobileWeb.App_Code.Encryption.Encrypt(String.Format("{0}&{1}&{2}",
                username, password,
                DateTime.Now.AddMinutes(_minExpires).Ticks),
                null);

            var passwordLink = MembershipHelper.GetBaseUrl() +
                  "Account/EnableUser?digest=" +
                  HttpUtility.UrlEncode(encrypted);
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com", 587);

            mail.From = new MailAddress("tinivippromobile@gmail.com");
            mail.To.Add(email);
            mail.Subject = "Kích hoạt tài khoản - TiniVippro";
            mail.IsBodyHtml = true;
            mail.Body += "<p>Nếu bạn không phải là người đăng kí tài khoản tại TiniVippro, vui lòng bỏ qua Email.</p>";
            mail.Body += "<p>Vui lòng nhấp vào link sau đây để kích hoạt tài khoản tại TiniVippro:<a href='" + passwordLink + "'>" + passwordLink + "</a></p>";
            SmtpServer.EnableSsl = true;
            SmtpServer.UseDefaultCredentials = false;
            var loginInfo = new System.Net.NetworkCredential("tinivippromobile@gmail.com", "tinivippro123");
            SmtpServer.Credentials = loginInfo;
            SmtpServer.Send(mail);
        }

        public static void SendNewPasswordEmail(string Username, string newPassword, string Email)
        {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com", 587);

            mail.From = new MailAddress("tinivippromobile@gmail.com");
            mail.To.Add(Email);
            mail.Subject = "Thông tin tài khoản - TiniVippro";
            mail.IsBodyHtml = true;
            var passwordLink = MembershipHelper.GetBaseUrl() +
                  "Account/Login";
            var body = @"<p>Tên đăng nhập: {0} Mật khẩu: {1} Để đăng nhập,
                        hãy đi đến đường dẫn sau: <a href='" + passwordLink + "'>" + passwordLink + "</a></p>";
            mail.Body = string.Format(body, Username, newPassword);
            SmtpServer.EnableSsl = true;
            SmtpServer.UseDefaultCredentials = false;
            var loginInfo = new System.Net.NetworkCredential("tinivippromobile@gmail.com", "tinivippro123");
            SmtpServer.Credentials = loginInfo;
            SmtpServer.Send(mail);
        }

        public static string GetBaseUrl()
        {
            var request = HttpContext.Current.Request;
            var appUrl = HttpRuntime.AppDomainAppVirtualPath;
            var baseUrl = string.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Authority, appUrl);

            return baseUrl;
        }
    }
}