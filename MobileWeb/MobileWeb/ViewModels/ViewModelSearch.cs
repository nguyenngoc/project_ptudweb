﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileWebBLL.Models;

namespace MobileWeb.ViewModels
{
    public class ViewModelSearch
    {
        public SanPham SanPham { get; set; }
        public SelectList NhaSanXuats { get; set; }
        public SelectList LoaiSanPhams { get; set; }
    }
}