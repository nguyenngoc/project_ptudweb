﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MobileWebBLL;
using MobileWebBLL.Models;
using MobileWeb.Models;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MobileWeb.ViewModels
{
    public class ViewModelAdminUpPersonalInfo
    {
        [Required]
        public int MaNV {get;set;}
        [Required]
        [DataType(DataType.Text)]
        public string TenNV { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string ChucVu { get; set; }
        //[Remote("CheckUserName", "adminNhanVien", HttpMethod = "POST", ErrorMessage = "Tên đăng nhập đã tồn tại")]
        [Required]
        [DataType(DataType.Text)]
        public string Username { get; set; }
    }
}