﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MobileWebBLL.Models;

namespace MobileWeb.ViewModels
{
    public class ViewModelComment
    {
        public BinhLuan BinhLuan { get; set; }
        public List<BinhLuan> BinhLuans { get; set; }
    }
}