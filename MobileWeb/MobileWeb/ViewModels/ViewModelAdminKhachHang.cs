﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MobileWebBLL.Models;
using MobileWebBLL.BLL;

namespace MobileWeb.ViewModels
{
    public class ViewModelAdminKhachHang
    {
        public KhachHang KhachHang { get; set; }
        public UserProfile UserProfile { get; set; }
    }
}