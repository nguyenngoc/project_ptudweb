﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MobileWebBLL.Models;

namespace MobileWeb.ViewModels
{
    public class ViewModelAdminProducts
    {
        public SanPham SanPham { get; set; }
        public ChiTietSanPham ChiTietSanPham { get; set; }
        public HinhAnh HinhAnh { get; set; }
        public List<LoaiSanPham> loaisp { get; set; }
        public List<NhaSanXuat> nhasanxuat { get; set; }
    }
}