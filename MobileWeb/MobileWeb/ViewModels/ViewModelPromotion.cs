﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MobileWebBLL.Models;

namespace MobileWeb.ViewModels
{
    public class ViewModelPromotion
    {
        public KhuyenMai KhuyenMai { get; set; }
        public List<SanPham> SanPham { get; set; }
        public List<QuaTang> QuaTang { get; set; }
    }
}