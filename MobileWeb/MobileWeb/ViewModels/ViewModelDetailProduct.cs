﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileWebBLL.Models;
using MobileWeb.ViewModels;

namespace MobileWeb.ViewModels
{
    public class ViewModelDetailProduct
    {
        public SanPham Sanpham { get; set; }
        public ChiTietSanPham ctSanpham { get; set; }
        public List<SanPham> SanphamsDongGia { get; set; }
        public List<SanPham> SanphamsLienQuan { get; set; }
    }
}