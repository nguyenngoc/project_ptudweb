﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileWebBLL.Models;

namespace MobileWeb.ViewModels
{
    public class ViewModelSidebar
    {
        public List<LoaiSanPham> Categories { get; set; }
        public List<NhaSanXuat> Carriers { get; set; }
    }
}