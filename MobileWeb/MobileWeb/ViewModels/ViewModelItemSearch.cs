﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileWeb.ViewModels
{
    public class ViewModelSanpham
    {
        public string TenSanPham;
        public string LoaiSanPham;
        public int KhoangGia;
        public string HangSanXuat;
    }
}