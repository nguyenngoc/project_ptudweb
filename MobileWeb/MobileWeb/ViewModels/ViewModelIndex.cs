﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MobileWebBLL.Models;

namespace MobileWeb.ViewModels
{
    public class ViewModelIndex
    {
        public List<SanPham> SanphamBanchaynhats { get; set; }
        public List<SanPham> SanphamMoinhaps { get; set; }
        public List<KhuyenMai> SanphamKhuyenmais { get; set; }
        public List<Slider> Sliders { get; set; }
    }
}