﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileWeb.ViewModels
{
    public class ViewModelBestSellingProduct
    {
        public long MaSP { get; set; }
        public string TenSP { get; set; }
        public string LoaiSP { get; set; }
        public string NhaSX { get; set; }
        public int DonGia { get; set; }
        public int SoLuongBan { get; set; }
    }
}