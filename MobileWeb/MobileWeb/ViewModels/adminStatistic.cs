﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileWeb.ViewModels
{
    public class adminStatistic
    {
        public int countProduct { get; set; }
        public int countMember { get; set; }
        public int sumRevenue { get; set; }
        public int countGift { get; set; }
    }
}