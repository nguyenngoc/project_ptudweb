﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MobileWebBLL;
using MobileWebBLL.Models;
using MobileWeb.Models;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MobileWeb.ViewModels
{
    public class ViewModelCustomerInfo
    {
        [Required]
        public int MaKH { get; set; }
        [Required]
        [DataType (DataType.Text)]
        public string TenKH { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string Username { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string DiaChi { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        [DataType(DataType.PhoneNumber)]
        [MinLength(6, ErrorMessage = "Chiều dài tối đa là 6")]
        [MaxLength(13,ErrorMessage="Chiều dài tối đa là 13")]
        public string DienThoai { get; set; }
    }
}