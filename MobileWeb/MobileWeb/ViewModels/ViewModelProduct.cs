﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileWebBLL.Models;
using MobileWeb.ViewModels;

namespace MobileWeb.ViewModels
{
    public class ViewModelProduct
    {
        public List<SanPham> Sanphams { get; set; }
        public ViewModelSearch Search { get; set; }
    }
}