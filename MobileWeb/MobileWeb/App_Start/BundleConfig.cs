﻿using System.Web;
using System.Web.Optimization;

namespace MobileWeb
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Scripts/jquery").Include(
                        "~/Scripts/home/jquery-ui-1.8.16.custom.min.js",
                        "~/Scripts/home/all-in-one-min.js",
                        "~/Scripts/home/setup.js",
                        "~/Scripts/home/main.js",
                        "~/Scripts/home/jquery.easing.1.3.js",
                        "~/Scripts/home/jquery.validate.min.js",
                        "~/Scripts/home/jquery.validate.unobtrusive.min.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/admin").Include(
                         "~/Scripts/admin/jquery-1.7.2.js",
                         "~/Scripts/admin/jquery-ui-1.8.21.custom.js",
                         "~/Scripts/admin/bootstrap-transition.js",
                         "~/Scripts/admin/bootstrap-alert.js",
                         "~/Scripts/admin/bootstrap-modal.js",
                         "~/Scripts/admin/bootstrap-dropdown.js",
                         "~/Scripts/admin/bootstrap-scrollspy.js",
                         "~/Scripts/admin/bootstrap-tab.js",
                         "~/Scripts/admin/bootstrap-tooltip.js",
                         "~/Scripts/admin/bootstrap-popover.js",
                         "~/Scripts/admin/bootstrap-button.js",
                         "~/Scripts/admin/bootstrap-collapse.js",
                         "~/Scripts/admin/bootstrap-carousel.js",
                         "~/Scripts/admin/bootstrap-typeahead.js",
                         "~/Scripts/admin/bootstrap-tour.js",
                         "~/Scripts/admin/jquery.cookie.js",
                         "~/Scripts/admin/fullcalendar.js",
                         "~/Scripts/admin/jquery.dataTables.js",
                         "~/Scripts/admin/excanvas.js",
                         "~/Scripts/admin/jquery.flot.js",
                         "~/Scripts/admin/jquery.flot.pie.js",
                         "~/Scripts/admin/jquery.flot.stack.js",
                         "~/Scripts/admin/jquery.flot.resize.js",
                         "~/Scripts/admin/jquery.chosen.js",
                         "~/Scripts/admin/jquery.uniform.js",
                         "~/Scripts/admin/jquery.colorbox.js",
                         "~/Scripts/admin/jquery.cleditor.js",
                         "~/Scripts/admin/jquery.noty.js",
                         "~/Scripts/admin/jquery.elfinder.js",
                         "~/Scripts/admin/jquery.raty.js",
                         "~/Scripts/admin/jquery.iphone.toggle.js",
                         "~/Scripts/admin/jquery.autogrow-textarea.js",
                         "~/Scripts/admin/jquery.uploadify-3.1.js",
                         "~/Scripts/admin/jquery.history.js",
                         "~/Scripts/admin/charisma.js",
                         "~/Scripts/admin/jquery.validate.js",
                         "~/Scripts/admin/jquery.validate.unobtrusive.js",
                         "~/Scripts/admin/jquery.unobtrusive-ajax.js"
                        ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.

            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/home/jquery.tooltip.css",
                        "~/Content/home/prettyPhoto.css",
                        "~/Content/home/flexslider.css",
                        "~/Content/home/jquery-ui.css"
                        ));

            
            // admin
            bundles.Add(new StyleBundle("~/Content/admin").Include(
                        "~/Content/admin/bootstrap-cerulean.css",
                        "~/Content/admin/bootstrap-responsive.css",
                        "~/Content/admin/charisma-app.css",
                        "~/Content/admin/jquery-ui-1.8.21.custom.css",
                        "~/Content/admin/fullcalendar.css",
                        "~/Content/admin/fullcalendar.print.css",
                        "~/Content/admin/chosen.css",
                        "~/Content/admin/uniform.default.css",
                        "~/Content/admin/colorbox.css",
                        "~/Content/admin/jquery.cleditor.css",
                        "~/Content/admin/jquery.noty.css",
                        "~/Content/admin/noty_theme_default.css",
                        "~/Content/admin/elfinder.css",
                        "~/Content/admin/elfinder.theme.css",
                        "~/Content/admin/jquery.iphone.toggle.css",
                        "~/Content/admin/opa-icons.css",
                        "~/Content/admin/mystyle.css",
                        "~/Content/admin/uploadify.css"
                        ));

            

        }
    }
}