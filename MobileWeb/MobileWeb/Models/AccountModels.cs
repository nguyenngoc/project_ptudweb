﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace MobileWeb.Models
{
    public class UsersContext : DbContext
    {
        public UsersContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
    }

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }
    }

    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required(ErrorMessage = "Yêu cầu nhập {0}")]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu hiện tại")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Yêu cầu nhập {0}")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu mới")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Nhập lại mật khẩu")]
        [Compare("NewPassword", ErrorMessage = "Nhập lại mật khẩu không giống với mật khẩu.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required(ErrorMessage = "Yêu cầu nhập {0}")]
        [Display(Name = "Tên đăng nhập")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Yêu cầu nhập {0}")]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu")]
        public string Password { get; set; }

        [Display(Name = "Ghi nhớ?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [Required(ErrorMessage = "Yêu cầu nhập {0}")]
        [StringLength(100, ErrorMessage = "{0} phải ít nhất {2} kí tự.", MinimumLength = 6)]
        [Display(Name = "Tên đăng nhập")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Yêu cầu nhập {0}")]
        [StringLength(100, ErrorMessage = "{0} phải ít nhất {2} kí tự.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Nhập lại mật khẩu")]
        [Compare("Password", ErrorMessage = "Nhập lại mật khẩu không giống với mật khẩu.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Yêu cầu nhập {0}")]
        [Display(Name = "Họ tên")]
        public string TenKH { get; set; }

        [Required(ErrorMessage = "Yêu cầu nhập {0}")]
        [Display(Name = "Địa chỉ")]
        public string DiaChi { get; set; }

        [Required(ErrorMessage = "Yêu cầu nhập {0}")]
        [RegularExpression("^[a-z0-9_\\+-]+(\\.[a-z0-9_\\+-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*\\.([a-z]{2,4})$", ErrorMessage = "Địa chỉ Email không hợp lệ")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Display(Name = "Email")]
        [StringLength(100)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Yêu cầu nhập {0}")]
        [RegularExpression("^([0-9]{6,11})$", ErrorMessage = "Điện thoại liên lạc không hợp lệ, số điện thoại phải từ 6-11 số.")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Display(Name = "Điện thoại liên lạc")]
        [StringLength(15)]
        public string DienThoai { get; set; }

        [Required(ErrorMessage = "Yêu cầu nhập {0}")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Display(Name = "Mã xác nhận")]
        [StringLength(15)]
        public string Capcha { get; set; }
    }

    public class NhanVienModel
    {
        [Required(ErrorMessage = "Yêu cầu nhập {0}")]
        [StringLength(100, ErrorMessage = "{0} phải ít nhất {2} kí tự.", MinimumLength = 6)]
        [Display(Name = "Tên đăng nhập")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Yêu cầu nhập {0}")]
        [StringLength(100, ErrorMessage = "{0} phải ít nhất {2} kí tự.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Nhập lại mật khẩu")]
        [Compare("Password", ErrorMessage = "Nhập lại mật khẩu không giống với mật khẩu.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Yêu cầu nhập {0}")]
        [Display(Name = "Họ tên")]
        public string TenNV { get; set; }

        [Required(ErrorMessage = "Yêu cầu nhập {0}")]
        [Display(Name = "Chức vụ")]
        public string ChucVu { get; set; }
    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }
}
