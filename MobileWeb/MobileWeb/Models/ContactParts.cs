﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace MobileWeb.Models
{
    public class ContactParts
    {
        [Required(ErrorMessage = "Yêu cầu nhập {0}")]
        [Display(Name = "Họ tên")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Yêu cầu nhập {0}")]
        [RegularExpression("^[a-z0-9_\\+-]+(\\.[a-z0-9_\\+-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*\\.([a-z]{2,4})$", ErrorMessage = "Địa chỉ Email không hợp lệ")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Display(Name = "Email")]
        [StringLength(100)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Yêu cầu nhập {0}")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Display(Name = "Nội dung")]
        public string Content { get; set; }
    }
}