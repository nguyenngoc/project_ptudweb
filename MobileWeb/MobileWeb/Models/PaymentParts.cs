﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileWeb.Models
{
    public class PaymentParts
    {
        public long MaHD { get; set; }
        public int MaKH { get; set; }
        public DateTime Expires { get; set; }
        public bool IsValid { get; set; }
    }
}