﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace MobileWeb.Models
{
    public class ProfileUserParts
    {
        [Required(ErrorMessage = "Yêu cầu nhập {0}")]
        [Display(Name = "Họ tên")]
        public string TenKH { get; set; }

        [Required(ErrorMessage = "Yêu cầu nhập {0}")]
        [Display(Name = "Địa chỉ")]
        public string DiaChi { get; set; }

        [Required(ErrorMessage = "Yêu cầu nhập {0}")]
        [RegularExpression("^[a-z0-9_\\+-]+(\\.[a-z0-9_\\+-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*\\.([a-z]{2,4})$", ErrorMessage = "Địa chỉ Email không hợp lệ")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Display(Name = "Email")]
        [StringLength(100)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Yêu cầu nhập {0}")]
        [RegularExpression("^([0-9]{6,11})$", ErrorMessage = "Điện thoại liên lạc không hợp lệ, số điện thoại phải từ 6-11 số.")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Display(Name = "Điện thoại liên lạc")]
        [StringLength(15)]
        public string DienThoai { get; set; }
    }
}