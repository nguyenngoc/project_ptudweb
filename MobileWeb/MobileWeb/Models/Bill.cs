﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace MobileWeb.Models
{
    public class Bill
    {
        [Required(ErrorMessage = "Yêu cầu nhập {0}")]
        [Display(Name = "Tên khách hàng nhận hàng")]
        public string TenKHDat { get; set; }

        [Required(ErrorMessage = "Yêu cầu nhập {0}")]
        [Display(Name = "Điện thoại liên hệ")]
        public string DienThoai { get; set; }

        [Required(ErrorMessage = "Yêu cầu nhập {0}")]
        [Display(Name = "Địa chỉ giao hàng")]
        public string DiaChiGiaoHang { get; set; }

        [Required(ErrorMessage = "Yêu cầu nhập {0}")]
        [Display(Name = "Hình thức giao hàng")]
        public short HinhThucGiaoHang { get; set; }

        [Display(Name = "Hình thức thanh toán")]
        public short HinhThucThanhToan { get; set; }

        public int MaKH { get; set; }
        public decimal TongTienVND { get; set; }
        public decimal TongTienUSD { get; set; }
    }
}