﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileWeb.Models
{
    public class EnableUserParts
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public DateTime Expires { get; set; }
        public bool IsValid { get; set; }
    }
}