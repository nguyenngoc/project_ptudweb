﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace MobileWeb.Models
{
    public class ResetPasswordParts
    {
        public string UserName { get; set; }
        public DateTime Expires { get; set; }
        public bool IsValid { get; set; }
    }
}