﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MobileWebBLL.Models;

namespace MobileWebBLL.BLL
{
    public class LoaiSanPhamBLL
    {

        MobileWebEntities webmob = new MobileWebEntities();

        public List<LoaiSanPham> ShowListLoaiSanPham()
        {
            var loaisp = webmob.LoaiSanPhams.Where(p => p.TrangThai == 1);
            loaisp.OrderBy(c => c.TenLoaiSP);
            return loaisp.ToList();
        }
        public bool CreateLoaiSanPhamDAO(LoaiSanPham LoaiSanPham)
        {
            LoaiSanPham.NgayTao = DateTime.Now;
            LoaiSanPham.NgayCapNhat = DateTime.Now;
            LoaiSanPham.TrangThai = 1;
            webmob.AddToLoaiSanPhams(LoaiSanPham);

            webmob.SaveChanges();
            return true;
        }
        public LoaiSanPham ShowEditLoaiSanPhamDAO(Int16 MaLoaiSP)
        {
            LoaiSanPham lsp = webmob.LoaiSanPhams.Single(l => l.MaLoaiSP == MaLoaiSP);
            return lsp;
        }
        public bool EditLoaiSanPhamDAO(LoaiSanPham LoaiSanPham)
        {
            int MaLoaiSP = int.Parse(LoaiSanPham.MaLoaiSP.ToString());
            LoaiSanPham tempLoaiSanPham = webmob.LoaiSanPhams.Single(c => c.MaLoaiSP == MaLoaiSP);
            tempLoaiSanPham.NgayCapNhat = DateTime.Now;
            tempLoaiSanPham.TenLoaiSP = LoaiSanPham.TenLoaiSP.ToString();
            webmob.SaveChanges();
            return true;
        }

        public bool DeleteLoaiSanPhamDAO(Int16 MaLoaiSP)
        {
            LoaiSanPham delloaisanpham = webmob.LoaiSanPhams.Single(c => c.MaLoaiSP == MaLoaiSP);
            delloaisanpham.TrangThai = 2;
            List<SanPham> listsp = webmob.SanPhams.Where(c => c.MaLoaiSP == MaLoaiSP).ToList();
            for (int i = 0; i < listsp.Count; i++)
            {
                SanPham s = webmob.SanPhams.Single(p => p.MaSP == listsp[i].MaSP);
                ChiTietSanPham ctsp = webmob.ChiTietSanPhams.Single(dp => dp.MaSP == listsp[i].MaSP);
                s.TrangThai = 2;
                ctsp.TrangThai = 2;
            }
            webmob.SaveChanges();
            return true;
        }

        //No Admin//
        public List<LoaiSanPham> ListLoaiSanPham()
        {
            return webmob.LoaiSanPhams.Where(p=>p.TrangThai1.TrangThai1 == "active").ToList();
        }

    }
}
