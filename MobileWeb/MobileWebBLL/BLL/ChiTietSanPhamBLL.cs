﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MobileWebBLL.Models;

namespace MobileWebBLL.BLL
{
    public class ChiTietSanPhamBLL
    {
        MobileWebEntities webmob = new MobileWebEntities();
        public ChiTietSanPham GetChiTietSanPham(int id)
        {
            var ctsanpham = webmob.ChiTietSanPhams.Where(p => p.MaSP == id && p.TrangThai1.TrangThai1 == "active").ToList();
            if (ctsanpham.Count > 0)
                return ctsanpham.First();
            return new ChiTietSanPham();
        }
    }
}
