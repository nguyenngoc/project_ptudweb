﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MobileWebBLL;
using MobileWebBLL.Models;

namespace MobileWebBLL.BLL
{
    public class HoaDonBLL
    {
        MobileWebEntities webmob = new MobileWebEntities();
        ChiTietHoaDonBLL adcthd = new ChiTietHoaDonBLL();

        public List<HoaDon> ShowListBills()
        {
            var Bills = webmob.HoaDons.Where(b => b.TrangThai == 1 && b.TinhTrang != 0).OrderByDescending(b => b.TongTien);
            return Bills.ToList();
        }
        public List<HoaDon> ShowListCancelBills()
        {
            var Bills = webmob.HoaDons.Where(b => b.TrangThai == 1 && b.TinhTrang == 0).OrderByDescending(b => b.TongTien);
            return Bills.ToList();
        }
        public bool UpdateTinhTrangDAO12(long MaHD)
        {
            HoaDon hd = webmob.HoaDons.Single(b => b.MaHD == MaHD);
            hd.TinhTrang = 3;
            webmob.SaveChanges();
            return true;
        }

        public bool UpdateTinhTrangDAO3(long MaHD)
        {
            HoaDon hd = webmob.HoaDons.Single(b => b.MaHD == MaHD);
            hd.TinhTrang = 4;
            webmob.SaveChanges();
            return true;
        }

        public bool UpdateTinhTrangDAO4(long MaHD)
        {
            HoaDon hd = webmob.HoaDons.Single(b => b.MaHD == MaHD);
            hd.TinhTrang = 5;
            webmob.SaveChanges();
            return true;
        }

        public HoaDon GetOneBillToEdit(long MaHD)
        {
            HoaDon hd = webmob.HoaDons.Single(b => b.MaHD == MaHD);
            return hd;
        }

        public bool UpdateBillDAO(HoaDon HoaDon)
        {
            HoaDon hd = webmob.HoaDons.Single(b => b.MaHD == HoaDon.MaHD);
            hd.DienThoai = HoaDon.DienThoai;
            hd.DiaChiGiaoHang = HoaDon.DiaChiGiaoHang;
            webmob.SaveChanges();
            return true;
        }

        public bool DeleteBillDAO(long MaHD)
        {
            HoaDon hd = webmob.HoaDons.Single(b => b.MaHD == MaHD);
            hd.TrangThai = 2;
            List<ChiTietHoaDon> cthd = webmob.ChiTietHoaDons.Where(ct => ct.MaHD == MaHD).ToList();
            for (int i = 0; i < cthd.Count; i++)
            {
                long temp = cthd[i].MaCTHD;
                adcthd.DeleteDetailBill(temp);
            }
            webmob.SaveChanges();
            return true;
        }

        public bool CancelBillDAO(long MaHD)
        {
            HoaDon hd = webmob.HoaDons.Single(b => b.MaHD == MaHD);
            hd.TinhTrang = 0;
            List<ChiTietHoaDon> cthd = webmob.ChiTietHoaDons.Where(ct => ct.MaHD == MaHD).ToList();
            for (int i = 0; i < cthd.Count; i++)
            {
                long temp = cthd[i].MaCTHD;
                adcthd.DeleteDetailBill(temp);
            }
            webmob.SaveChanges();
            return true;
        }

        public int Soluong(long MaSP)
        {
            int soluong = webmob.SanPhams.First(p => p.MaSP == MaSP).ChiTietHoaDons.Sum(p => p.SoLuong);
            return soluong;
        }

        public List<HoaDon> GetBillOverdue()
        {
            DateTime deadline = DateTime.Now.AddHours(-24);
            List<HoaDon> listhd = webmob.HoaDons.Where(p => p.TinhTrang == 1 && p.ThoiGianDatHang <= deadline).ToList();
            return listhd;
        }

        public bool CancelBillOverdue()
        {
            //DateTime deadline = DateTime.Now.AddHours(-24);
            List<HoaDon> listhd = GetBillOverdue();
            for (int i = 0; i < listhd.Count; i++)
            {
                listhd[i].TinhTrang = 0;
                long MaHD = listhd[i].MaHD;
                List<ChiTietHoaDon> cthd = webmob.ChiTietHoaDons.Where(ct => ct.MaHD == MaHD).ToList();
                for (int j = 0; j < cthd.Count; j++)
                {
                    long temp = cthd[j].MaCTHD;
                    adcthd.DeleteDetailBill(temp);
                }
            }
            webmob.SaveChanges();
            return true;
        }
        /*
        public List<HoaDon> RevenueDate()
        {
            var hd = from b in webmob.HoaDons
                     group b by b.ThoiGianDatHang.ToString("MM/dd/yyyy") into g
                     select new { NgayDat = g.Key, DoanhThu = g.Sum(p => p.TongTien)  };
            return hd;

        }

        public List<HoaDon> RevenueMonth()
        {
            var hd = from b in webmob.HoaDons
                     group b by b.ThoiGianDatHang.Year into yg
                     select new
                     {
                         Nam = yg.Key,
                         Thang = from o in yg
                                 group o by o.ThoiGianDatHang.Month into mg
                                 select new { Thang = mg.Key, DoanhThu = mg.Sum(p => p.TongTien)}
                     };
            return hd;
        }

        public List<HoaDon> RevenueYear()
        {
            var hd = from b in webmob.HoaDons
                     group b by b.ThoiGianDatHang.Year into yg
                     select new { Nam = yg.Key, DoanhThu = yg.Sum(p => p.TongTien) };
            return hd;
        }
         
        */

        //No admin
        public long AddHoaDon(List<SanPham> sp, string TenKHDat, string DienThoai, string DiaChiGiaoHang, short HinhThucGiaoHang, short HinhThucThanhToan, int TongTienVND, int MaKH)
        {
            var tt = webmob.TrangThais.Where(p => p.TrangThai1 == "active").ToList();
            HoaDon hd = new HoaDon
            {
                MaKH = MaKH,
                TenKHDat = TenKHDat,
                DienThoai = DienThoai,
                DiaChiGiaoHang = DiaChiGiaoHang,
                HinhThucGiaoHang = HinhThucGiaoHang,
                HinhThucThanhToan = HinhThucThanhToan,
                ThoiGianDatHang = DateTime.Now,
                NgayCapNhat = DateTime.Now,
                NgayTao = DateTime.Now,
                TinhTrang = 1,
                TrangThai = tt.First().MaTrangThai,
                TongTien = TongTienVND
            };
            try
            {
                webmob.AddToHoaDons(hd);
                webmob.SaveChanges();
                var hoadon = webmob.HoaDons.Where(p => p.MaKH == MaKH && p.TongTien == TongTienVND).ToList();
                if (hoadon.Count <= 0)
                    return -1;
                long MaHD = hoadon.Last().MaHD;
                foreach (var item in sp)
                {
                    ProductBLL pro = new ProductBLL();
                    int TT = pro.GetGiaFromMaSP(item.MaSP);
                    ChiTietHoaDon ct = new ChiTietHoaDon
                    {
                        MaHD = MaHD,
                        MaSP = item.MaSP,
                        SoLuong = item.SoLuong,
                        ThanhTien = item.SoLuong * TT,
                        NgayCapNhat = DateTime.Now,
                        NgayTao = DateTime.Now,
                        TrangThai = tt.First().MaTrangThai
                    };
                    webmob.AddToChiTietHoaDons(ct);
                }
                webmob.SaveChanges();
                return MaHD;
            }
            catch
            {
                return -1;
            }
        }

        public bool CheckHoaDon(long MaHD, int MaKH)
        {
            TrangThaiBLL tt = new TrangThaiBLL();
            short status = tt.GetTrangThai("active");
            var hd = webmob.HoaDons.Where(h => h.MaHD == MaHD && h.MaKH == MaKH && h.TrangThai == status).ToList();
            if (hd.Count > 0)
                return true;
            return false;
        }

        public bool UpdateStatusHoaDon(long MaHD, short TinhTrang, short HinhThucThanhToan)
        {
            var hd = webmob.HoaDons.Single(h => h.MaHD == MaHD);
            hd.TinhTrang = TinhTrang;
            // thay đổi hình thức thanh toán
            if (HinhThucThanhToan != -1)
                hd.HinhThucThanhToan = HinhThucThanhToan;
            try
            {
                webmob.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<HoaDon> GetHDFromMaKH(int MaKH, int page, int rowsperpage)
        {
            var hoadons = new List<HoaDon>();
            if (rowsperpage != 0)
                hoadons = webmob.HoaDons.Where(c => c.MaKH == MaKH && c.TrangThai1.TrangThai1 == "active").OrderByDescending(c => c.NgayTao).Skip((page - 1) * rowsperpage).Take(rowsperpage).ToList();
            else hoadons = webmob.HoaDons.Where(c => c.MaKH == MaKH).ToList();
            return hoadons;
        }

        public HoaDon GetHDFromMaHD(long MaHD)
        {
            var hoadons = webmob.HoaDons.Where(p=>p.MaHD == MaHD && p.TrangThai1.TrangThai1 == "active").ToList();
            if (hoadons.Count > 0)
                return hoadons.First();
            return new HoaDon();
        }

        public double CountPagesHoaDon(List<HoaDon> objects, int rowsperpage)
        {
            double pages = Math.Ceiling((float)objects.Count() / rowsperpage);
            return pages;
        }

        public int CountHoaDons(List<HoaDon> objects)
        {
            return objects.Count;
        }

        public List<StatisticParts> Statistic(string fromdate, string todate, int format)
        {
            List<StatisticParts> liststa = new List<StatisticParts>();
            MobileWebEntities web = new MobileWebEntities();
            string[] strfrom = fromdate.Split('/');
            string[] strto = todate.Split('/');
            DateTime muctren = new DateTime(int.Parse(strfrom[2]), int.Parse(strfrom[0]), int.Parse(strfrom[1]));
            DateTime mucduoi = new DateTime(int.Parse(strto[2]), int.Parse(strto[0]), int.Parse(strto[1]));
            
            var hds = web.HoaDons.Where(h => h.ThoiGianDatHang >= muctren && h.ThoiGianDatHang <= mucduoi 
                && h.TrangThai1.TrangThai1 == "active" && (h.TinhTrang == 3 || h.TinhTrang == 4 || h.TinhTrang == 5));

            var cthds = (from hd in hds
                         join ct in web.ChiTietHoaDons
                             on hd.MaHD equals ct.MaHD
                         where ct.TrangThai1.TrangThai1 == "active"
                         select new
                         {
                             Day = hd.ThoiGianDatHang.Value.Day,
                             Month = hd.ThoiGianDatHang.Value.Month,
                             Year = hd.ThoiGianDatHang.Value.Year,
                             Revenue = ct.ThanhTien
                         }
                );

            if (format == 1)
            {
                var tks1 = (from cthd in cthds
                            group cthd by new { cthd.Day, cthd.Month, cthd.Year } into g
                            select new
                            {
                                Day = g.Key.Day,
                                Month = g.Key.Month,
                                Year = g.Key.Year,
                                Revenue = g.Sum(p => p.Revenue)
                            }
                );
                foreach (var item in tks1.ToList())
                {
                    StatisticParts sta = new StatisticParts();
                    sta.Day = item.Day;
                    sta.Month = item.Month;
                    sta.Year = item.Year;
                    sta.Revenue = item.Revenue;
                    liststa.Add(sta);
                }
            }
            if (format == 2)
            {
                var tks2 = (from cthd in cthds
                            group cthd by new { cthd.Month, cthd.Year } into g
                            select new
                            {
                                Month = g.Key.Month,
                                Year = g.Key.Year,
                                Revenue = g.Sum(p => p.Revenue)
                            }
                );
                foreach (var item in tks2.ToList())
                {
                    StatisticParts sta = new StatisticParts();
                    sta.Day = 0;
                    sta.Month = item.Month;
                    sta.Year = item.Year;
                    sta.Revenue = item.Revenue;
                    liststa.Add(sta);
                }
            }
            if (format == 3)
            {
                var tks3 = (from cthd in cthds
                            group cthd by new { cthd.Year } into g
                            select new
                            {
                                Year = g.Key.Year,
                                Revenue = g.Sum(p => p.Revenue)
                            }
                );
                foreach (var item in tks3.ToList())
                {
                    StatisticParts sta = new StatisticParts();
                    sta.Day = 0;
                    sta.Month = 0;
                    sta.Year = item.Year;
                    sta.Revenue = item.Revenue;
                    liststa.Add(sta);
                }
            }
            return liststa;
        }
    }
}
