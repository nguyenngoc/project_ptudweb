﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MobileWebBLL.Models;

namespace MobileWebBLL.BLL
{
    class TrangThaiBLL
    {
        MobileWebEntities webmob = new MobileWebEntities();
        public short GetTrangThai(string TenTrangThai)
        {
            var tt = webmob.TrangThais.Where(p => p.TrangThai1 == TenTrangThai).ToList();
            if (tt.Count > 0)
                return tt.First().MaTrangThai;
            return -1;
        }
    }
}
