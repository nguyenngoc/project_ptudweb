﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MobileWebBLL.Models;

namespace MobileWebBLL.BLL
{
    public class SliderBLL
    {
        MobileWebEntities webmob = new MobileWebEntities();
        public bool InsertSlider(Slider Slider)
        {
            Slider.NgayCapNhat = DateTime.Now;
            Slider.NgayTao = DateTime.Now;
            Slider.TrangThai = 1;
            Slider.Link = "images/home/mobiles/" + Slider.Link;
            webmob.AddToSliders(Slider);
            webmob.SaveChanges();
            return true;
        }

        public bool DeleteSilder(int MaSlider)
        {
            var slider = webmob.Sliders.Single(s => s.MaSlider == MaSlider);
            slider.TrangThai = 2;
            webmob.SaveChanges();
            return true;
        }
        
        //NO ADMIN
        public List<Slider> ShowSlider()
        {
            var sliders = webmob.Sliders.Where(p => p.TrangThai1.TrangThai1 == "active").ToList();
            return sliders;
        }
    }
}
