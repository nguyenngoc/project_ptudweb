﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MobileWebBLL.Models;

namespace MobileWebBLL.BLL
{
    public class NhanVienBLL
    {
        MobileWebEntities webmob = new MobileWebEntities();
        public List<NhanVien> GetListEmployee()
        {
            return webmob.NhanViens.Where(c => c.TrangThai == 1).ToList();
        }
        public NhanVien GetOneEmployee(int MaNV)
        {
            var nhanvien = webmob.NhanViens.First(e => e.MaNV == MaNV);
            return nhanvien;
        }

        public NhanVien GetOneEmployeeString(string username)
        {
            NhanVien nv = webmob.NhanViens.Single(e => e.UserProfile.UserName == username);
            return nv;
        }

        public bool UpdateEmployee(NhanVien nhanvien)
        {
            int MaNV = int.Parse(nhanvien.MaNV.ToString());
            NhanVien tempNV = webmob.NhanViens.Single(c => c.MaNV == MaNV);
            tempNV.TenNV = nhanvien.TenNV;
            tempNV.ChucVu = nhanvien.ChucVu;
            tempNV.NgayCapNhat = DateTime.Now;

            tempNV.UserProfile.UserName = nhanvien.UserProfile.UserName;

            webmob.SaveChanges();
            return true;
        }

        public bool DeleteEmployee(int MaNV)
        {
            NhanVien delEmployee = webmob.NhanViens.Single(c => c.MaNV == MaNV);
            delEmployee.UserId = null;
            delEmployee.TrangThai = 2;
            webmob.SaveChanges();
            return true;
        }

        public bool AddEmployee(int UserId, string TenNV, string ChucVu)
        {
            NhanVien nhanvien = new NhanVien { UserId = UserId, TenNV=TenNV, ChucVu = ChucVu};
            nhanvien.TrangThai = 1;
            nhanvien.NgayTao = DateTime.Now;
            nhanvien.NgayCapNhat = DateTime.Now;
            webmob.AddToNhanViens(nhanvien);
            webmob.SaveChanges();
            return true;
        }
    }
}
