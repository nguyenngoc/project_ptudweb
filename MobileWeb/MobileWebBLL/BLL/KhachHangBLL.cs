﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MobileWebBLL.Models;

namespace MobileWebBLL.BLL
{
    public class KhachHangBLL
    {
        MobileWebEntities webmob = new MobileWebEntities();
        public List<KhachHang> GetListCustomer()
        {
            return webmob.KhachHangs.Where(c => c.TrangThai == 1).ToList();
        }
        public KhachHang GetOneCustomer(int MaKH)
        {
            var khachhang = webmob.KhachHangs.First(c => c.MaKH == MaKH);
            return khachhang;
        }

        public bool UpdateCustomer(KhachHang KhachHang)
        {
            int MaKH = int.Parse(KhachHang.MaKH.ToString());
            KhachHang tempKH = webmob.KhachHangs.Single(c => c.MaKH == MaKH);
            tempKH.TenKH = KhachHang.TenKH;
            tempKH.DiaChi = KhachHang.DiaChi;
            tempKH.Email = KhachHang.Email;
            tempKH.DienThoai = KhachHang.DienThoai;
            tempKH.NgayCapNhat = DateTime.Now;

            tempKH.UserProfile.UserName = KhachHang.UserProfile.UserName;
            webmob.SaveChanges();
            return true;
        }

        public bool DeleteCustomer(int MaKH)
        {
            KhachHang delcustomer = webmob.KhachHangs.Single(c => c.MaKH == MaKH);
            delcustomer.UserId = null;
            delcustomer.TrangThai = 2;
            webmob.SaveChanges();
            return true;
        }

        public string GetEmailAddress(int MaKH)
        {
            string email = webmob.KhachHangs.First(c => c.MaKH == MaKH).Email;
            return email;
        }

        //NO ADMIN
        public int AddKhachHang(int UserId, string TenKH, string DiaChi, string Email, string DienThoai)
        {
            KhachHang kh = new KhachHang();
            kh.UserId = UserId;
            kh.TenKH = TenKH;
            kh.DiaChi = DiaChi;
            kh.DienThoai = DienThoai;
            kh.Email = Email;
            kh.NgayTao = DateTime.Now;
            kh.NgayCapNhat = DateTime.Now;
            TrangThai status = webmob.TrangThais.Where(p => p.TrangThai1 == "expired").First();
            kh.TrangThai = status.MaTrangThai;
            try
            {
                webmob.AddToKhachHangs(kh);
                webmob.SaveChanges();
                return 1;
            }
            catch
            {
                return 0;
            }
        }

        public int EnableUser(int UserId)
        {
            KhachHang kh = webmob.KhachHangs.Single(p => p.UserId == UserId);
            TrangThai status = webmob.TrangThais.Where(p => p.TrangThai1 == "active").First();
            kh.TrangThai = status.MaTrangThai;
            try
            {
                webmob.SaveChanges();
                return 1;
            }
            catch
            {
                return 0;
            }
        }

        public string GetMailFromUserId(int UserId)
        {
            var kh = webmob.KhachHangs.Where(p=>p.UserId == UserId && p.TrangThai1.TrangThai1 == "active").ToList();
            if (kh.Count <= 0)
                return string.Empty;
            else return kh.First().Email;
        }

        public string GetUserNameFromEmail(string mail)
        {
            var kh = webmob.KhachHangs.Where(p => p.Email == mail && p.TrangThai1.TrangThai1 == "active").ToList();
            if (kh.Count <= 0)
            {
                return string.Empty;
            }
            var user = webmob.UserProfiles.Where(u => u.UserId == kh.First().UserId).ToList();
            if (user.Count <= 0)
            {
                return string.Empty;
            }
            else return user.First().UserName;
        }

        public bool CheckMail(string mail)
        {
            var kh = webmob.KhachHangs.Where(p => p.Email == mail && p.TrangThai1.TrangThai1 == "active").ToList();
            if (kh.Count == 0)
                return false;
            else return true;
        }

        public int GetMaKHFromUserName(string UserName)
        {
            return GetCustomerFromUsername(UserName).MaKH;
        }

        public KhachHang GetCustomerFromUsername(string UserName)
        {
            int UserId = -1;
            var user = webmob.UserProfiles.Where(u => u.UserName == UserName).ToList();
            if (user.Count > 0)
                UserId = user.First().UserId;
            else return new KhachHang();
            var kh = webmob.KhachHangs.Where(p => p.UserId == UserId  && p.TrangThai1.TrangThai1 == "active").ToList();
            if (kh.Count > 0)
            {
                return kh.First();
            }
            else return new KhachHang();
        }

        public bool CheckUserEnable(string UserName)
        {
            int UserId = -1;
            var user = webmob.UserProfiles.Where(u => u.UserName == UserName).ToList();
            if (user.Count > 0)
                UserId = user.First().UserId;
            else return false;
            var kh = webmob.KhachHangs.Where(p => p.UserId == UserId && p.TrangThai1.TrangThai1 == "active").ToList();
            if (kh.Count > 0)
            {
                return true;
            }
            else
            {
                var nv = webmob.NhanViens.Where(p => p.UserId == UserId && p.TrangThai1.TrangThai1 == "active").ToList();
                if (nv.Count > 0)
                {
                    return true;
                }
                return false;
            }
        }

        public int UpdateKhachHang(string UserName, string TenKH, string DiaChi, string Email, string DienThoai)
        {
            int UserId = -1;
            var user = webmob.UserProfiles.Where(u => u.UserName == UserName).ToList();
            if (user.Count > 0)
                UserId = user.First().UserId;
            else return 0;
            KhachHang kh = webmob.KhachHangs.Single(p => p.UserId == UserId);
            kh.UserId = UserId;
            kh.TenKH = TenKH;
            kh.DiaChi = DiaChi;
            kh.DienThoai = DienThoai;
            kh.Email = Email;
            try
            {
                webmob.SaveChanges();
                return 1;
            }
            catch
            {
                return 0;
            }
        }
    }
}
