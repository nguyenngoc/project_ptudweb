﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MobileWebBLL;
using MobileWebBLL.Models;

namespace MobileWebBLL.BLL
{
    public class ChiTietHoaDonBLL
    {
        MobileWebEntities webmob = new MobileWebEntities();

        public List<ChiTietHoaDon> ShowDetailsBill(long MaHD)
        {
            var cthd = webmob.ChiTietHoaDons.Where(db => db.MaHD == MaHD && db.TrangThai == 1).OrderBy(db => db.ThanhTien);
            return cthd.ToList();
        }
        public bool DeleteDetailBillDAO(long MaCTHD)
        {
            ChiTietHoaDon cthd = webmob.ChiTietHoaDons.Single(ct => ct.MaCTHD == MaCTHD);
            cthd.HoaDon.TongTien = cthd.HoaDon.TongTien - cthd.ThanhTien;
            cthd.TrangThai = 2;
            webmob.SaveChanges();
            return true;
        }
        public bool DeleteDetailBill(long MaCTHD)
        {
            ChiTietHoaDon cthd = webmob.ChiTietHoaDons.Single(ct => ct.MaCTHD == MaCTHD);
            cthd.TrangThai = 2;
            webmob.SaveChanges();
            return true;
        }
    }
}
