﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MobileWebBLL.Models;

namespace MobileWebBLL.BLL
{
    public class HinhAnhBLL
    {
        MobileWebEntities webmob = new MobileWebEntities();

        public List<HinhAnh> GetHinhAnh(int MaSP)
        {
            var list = webmob.HinhAnhs.Where(p => p.MaSP == MaSP && p.TrangThai == 1);
            return list.ToList();
        }

        public void InsertHinh(HinhAnh img)
        {
            img.NgayCapNhat = DateTime.Now;
            img.NgayTao = DateTime.Now;
            img.TrangThai = 1;
            img.Link = "images/home/mobiles/" + img.Link;
            webmob.AddToHinhAnhs(img);
            webmob.SaveChanges();
        }

        public void DeleteHinh(int MaHinhAnh)
        {
            var img = webmob.HinhAnhs.Single(i => i.MaHinhAnh == MaHinhAnh);
            img.TrangThai = 2;
            webmob.SaveChanges();
        }
    }
}
