﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MobileWebBLL.Models;

namespace MobileWebBLL.BLL
{
    public class NhaSanXuatBLL
    {
        MobileWebEntities webmob = new MobileWebEntities();
        public List<NhaSanXuat> ListNhaSanXuat()
        {
            return webmob.NhaSanXuats.Where(p => p.TrangThai == 1).ToList();
        }

        public bool CreateNhaSanXuatDAO(NhaSanXuat nsx)
        {
            nsx.NgayTao = DateTime.Now;
            nsx.NgayCapNhat = DateTime.Now;
            nsx.TrangThai = 1;
            webmob.AddToNhaSanXuats(nsx);

            webmob.SaveChanges();
            return true;
        }

        public NhaSanXuat ShowEditNhaSanXuatDAO(Int16 MaNSX)
        {
            NhaSanXuat nsx = webmob.NhaSanXuats.Single(i => i.MaNSX == MaNSX);
            return nsx;
        }

        public bool EditNhaSanXuatDAO(NhaSanXuat nsanxuat)
        {
            int MaNSX = int.Parse(nsanxuat.MaNSX.ToString());
            NhaSanXuat temp = webmob.NhaSanXuats.Single(c => c.MaNSX == MaNSX);
            webmob.NhaSanXuats.Attach(temp);
            nsanxuat.NgayCapNhat = DateTime.Now;
            nsanxuat.TrangThai = temp.TrangThai;
            nsanxuat.NgayTao = temp.NgayTao;
            webmob.NhaSanXuats.ApplyCurrentValues(nsanxuat);
            webmob.SaveChanges();
            return true;
        }

        public bool DeleteNhaSanXuatDAO(Int16 MaNSX)
        {
            NhaSanXuat delnsx = webmob.NhaSanXuats.Single(n => n.MaNSX == MaNSX);
            delnsx.TrangThai = 2;
            webmob.SaveChanges();
            return true;
        }       
        
        //No Admin//

        public List<NhaSanXuat> ListSearchNhaSanXuat()
        {
            HashSet<string> already = new HashSet<string>();
            List<NhaSanXuat> kq = new List<NhaSanXuat>();
            var nsx = webmob.NhaSanXuats.ToList();
            foreach (var item in nsx)
            {
                if (already.Add(item.TenNSX))
                {
                    kq.Add(item);
                }
            }
            return kq;
        }

    }
}
