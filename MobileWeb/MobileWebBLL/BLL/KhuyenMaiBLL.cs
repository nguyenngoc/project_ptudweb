﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MobileWebBLL.Models;

namespace MobileWebBLL.BLL
{
    public class KhuyenMaiBLL
    {
        MobileWebEntities webmob = new MobileWebEntities();

        public List<KhuyenMai> ShowListPromotions()
        {
            var promotions = webmob.KhuyenMais.Where(p => p.TrangThai == 1);
            promotions.OrderBy(c => c.MaSP);
            return promotions.ToList();
        }
        public List<QuaTang> GetListGifts()
        {
            var gifts = webmob.QuaTangs.Where(g => g.TrangThai == 1).OrderBy(g => g.MaQuaTang).ToList();
            return gifts;
        }

        public KhuyenMai GetOnePromotion(int MaKhuyenMai)
        {
            var comment = webmob.KhuyenMais.First(c => c.MaKhuyenMai == MaKhuyenMai);
            if (comment.GiamGia == null)
            {
                comment.GiamGia = 0;
            }
            return comment;
        }
        public bool EditPromotionDAO(KhuyenMai KhuyenMai)
        {
            int MaKhuyenMai = int.Parse(KhuyenMai.MaKhuyenMai.ToString());
            KhuyenMai tempPromotion = webmob.KhuyenMais.Single(c => c.MaKhuyenMai == MaKhuyenMai);
            tempPromotion.NgayCapNhat = DateTime.Now;
            tempPromotion.MaSP = int.Parse(KhuyenMai.MaSP.ToString());
            tempPromotion.NgayBD = KhuyenMai.NgayBD;
            tempPromotion.NgayKT = KhuyenMai.NgayKT;
            tempPromotion.QuaTang = KhuyenMai.QuaTang;
            tempPromotion.GiamGia = KhuyenMai.GiamGia;
            webmob.SaveChanges();
            return true;
        }

        public bool DeletePromotionDAO(int MaKhuyenMai)
        {
            KhuyenMai delPromotion = webmob.KhuyenMais.Single(c => c.MaKhuyenMai == MaKhuyenMai);
            delPromotion.TrangThai = 2;
            webmob.SaveChanges();
            return true;
        }
        public bool InsertPromotionDAO(KhuyenMai KhuyenMai)
        {
            KhuyenMai.NgayCapNhat = DateTime.Now;
            KhuyenMai.NgayTao = DateTime.Now;
            webmob.AddToKhuyenMais(KhuyenMai);
            webmob.SaveChanges();
            return true;
        }
    }
}
