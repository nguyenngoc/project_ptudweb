﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MobileWebBLL;
using MobileWebBLL.Models;


namespace MobileWebBLL.BLL
{
    public class UserProfileBLL
    {
        MobileWebEntities webmob = new MobileWebEntities();

        public UserProfile GetOneUserProfile(int? UserId)
        {
            var UserPro = webmob.UserProfiles.First(s => s.UserId == UserId);
            return UserPro;
        }

        public void UpdateUserProfile(UserProfile UserProfile)
        {
            int UserId = int.Parse(UserProfile.UserId.ToString());
            UserProfile tempUser = webmob.UserProfiles.Single(u => u.UserId == UserId);
            tempUser.UserName = UserProfile.UserName;

            webmob.SaveChanges();
        }

    }
}
