﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MobileWebBLL.Models;

namespace MobileWebBLL.BLL
{
    public class CommentBLL
    {
        MobileWebEntities webmob = new MobileWebEntities();

        public List<BinhLuan> ShowListComments()
        {
            var comments = webmob.BinhLuans.Where(p => p.TrangThai == 1);
            comments.OrderBy(c => c.MaSP);
            return comments.ToList();
        }
        public BinhLuan GetOneComment(int MaBL)
        {
            var comment = webmob.BinhLuans.First(c => c.MaBinhLuan == MaBL);
            return comment;
        }
        public bool EditCommentDAO(BinhLuan BinhLuan)
        {
            long MaBL = long.Parse(BinhLuan.MaBinhLuan.ToString());
            BinhLuan tempComment = webmob.BinhLuans.Single(c => c.MaBinhLuan == MaBL);
            tempComment.NgayCapNhat = DateTime.Now;
            tempComment.MaSP = int.Parse(BinhLuan.MaSP.ToString());
            tempComment.TenKH = BinhLuan.TenKH;
            tempComment.Email = BinhLuan.Email;
            tempComment.NoiDung = BinhLuan.NoiDung;
            webmob.SaveChanges();
            return true;
        }

        public bool DeleteCommentDAO(int MaBL)
        {
            BinhLuan delcomments = webmob.BinhLuans.Single(c => c.MaBinhLuan == MaBL);
            delcomments.TrangThai = 2;
            webmob.SaveChanges();
            return true;
        }

        //No admin//
        public void InsertCommentDAO(BinhLuan Binhluan)
        {
            Binhluan.NgayCapNhat = DateTime.Now;
            Binhluan.NgayTao = DateTime.Now;
            Binhluan.ThoiGianBinhLuan = DateTime.Now;
            if (Binhluan.Email == null)
                Binhluan.Email = "";
            Binhluan.TrangThai = webmob.TrangThais.Where(p => p.TrangThai1 == "active").First().MaTrangThai;
            webmob.AddToBinhLuans(Binhluan);
            webmob.SaveChanges();        
        }

        public List<BinhLuan> GetCommentsFromProduct(long MaSP, int page, int rowsperpage)
        {
            var comments = new List<BinhLuan>();
            if (rowsperpage != 0)
                comments = webmob.BinhLuans.Where(c => c.MaSP == MaSP && c.TrangThai1.TrangThai1 == "active").OrderByDescending(c => c.NgayTao).Skip((page - 1) * rowsperpage).Take(rowsperpage).ToList();
            else comments = webmob.BinhLuans.Where(c => c.MaSP == MaSP && c.TrangThai1.TrangThai1 == "active").ToList();
            return comments;
        }

        public double CountPages(List<BinhLuan> objects, int rowsperpage)
        {
            double pages = Math.Ceiling((float)objects.Count() / rowsperpage);
            return pages;
        }

        public int CountComments(List<BinhLuan> objects)
        {
            return objects.Count;
        }
    }
}
