﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MobileWebBLL;
using MobileWebBLL.Models;

namespace MobileWebBLL.BLL
{
    public class ProductBLL
    {
        MobileWebEntities webmob = new MobileWebEntities();

        public List<SanPham> ShowListProduct()
        {
            var products = webmob.SanPhams.Where(p => p.TrangThai == 1);
            products.OrderBy(p => p.MaNSX);
            return products.ToList();
        }

        public List<ChiTietSanPham> ShowDetailsProduct(int MaSP)
        {
            var ctsp = webmob.ChiTietSanPhams.Where(dp => dp.MaSP == MaSP);
            ctsp.OrderBy(ct => ct.MaChiTiet);
            return ctsp.ToList();
        }
        public List<LoaiSanPham> GetListCategories()
        {
            var loaisp = webmob.LoaiSanPhams.ToList();
            return loaisp;
        }

        public SanPham GetLastProduct()
        {
            SanPham last = webmob.SanPhams.ToList().Last();
            return last;
        }

        public List<NhaSanXuat> GetListCompany()
        {
            var nsx = webmob.NhaSanXuats.ToList();
            return nsx;
        }

        public SanPham GetOneProduct(int MaSP)
        {
            var product = webmob.SanPhams.First(s => s.MaSP == MaSP);
            return product;
        }

        public ChiTietSanPham ShowDetailProduct(int MaSP)
        {
            var ctsp = webmob.ChiTietSanPhams.First(dp => dp.MaSP == MaSP);
            return ctsp;
        }

        public void CreateroductDAO(SanPham SanPham, ChiTietSanPham ChiTietSanPham)
        {
            SanPham.NgayTao = DateTime.Now;
            SanPham.NgayCapNhat = DateTime.Now;
            if (SanPham.SoLuong <= 0 || SanPham.SoLuong == null)
            {
                SanPham.SoLuong = 0;
            }
            if (SanPham.SoLuong <= 0)
            {
                SanPham.TinhTrang = false;
            }
            else
            {
                SanPham.TinhTrang = true;
            }
            if (SanPham.BaoHanh <= 0 || SanPham.BaoHanh == null)
            {
                SanPham.BaoHanh = 0;
            }
            SanPham.TrangThai = 1;
            webmob.AddToSanPhams(SanPham);

            ChiTietSanPham.NgayTao = DateTime.Now;
            ChiTietSanPham.NgayCapNhat = DateTime.Now;
            ChiTietSanPham.TrangThai = 1;
            ChiTietSanPham.MaSP = ChiTietSanPham.MaChiTiet;
            webmob.AddToChiTietSanPhams(ChiTietSanPham);
            webmob.SaveChanges();
        }
        public void EditProductDAO(SanPham SanPham, ChiTietSanPham ChiTietSanPham)
        {
            int MaSP = int.Parse(SanPham.MaSP.ToString());
            SanPham tempSanPham = webmob.SanPhams.Single(p => p.MaSP == MaSP);
            webmob.SanPhams.Attach(tempSanPham);
            SanPham.NgayCapNhat = DateTime.Now;
            SanPham.TrangThai = 1;
            webmob.SanPhams.ApplyCurrentValues(SanPham);

            ChiTietSanPham tempChiTietSanPham = webmob.ChiTietSanPhams.Single(ctsp => ctsp.MaSP == MaSP);
            ChiTietSanPham.NgayCapNhat = DateTime.Now;
            ChiTietSanPham.TrangThai = 1;
            ChiTietSanPham.MaChiTiet = tempChiTietSanPham.MaChiTiet;
            ChiTietSanPham.MaSP = tempChiTietSanPham.MaSP;
            webmob.ChiTietSanPhams.Attach(tempChiTietSanPham);
            webmob.ChiTietSanPhams.ApplyCurrentValues(ChiTietSanPham);

            webmob.SaveChanges();

            //tempChiTietSanPham.HDH = ChiTietSanPham.HDH.ToString();
            //tempChiTietSanPham.Mang2G = ChiTietSanPham.Mang2G.ToString();
            //tempChiTietSanPham.Mang3G = ChiTietSanPham.Mang3G.ToString();
            //tempChiTietSanPham.NgayRaMat = DateTime.Parse(ChiTietSanPham.NgayRaMat.ToString());
            //tempChiTietSanPham.NgayCoHang = DateTime.Parse(ChiTietSanPham.NgayCoHang.ToString());
            //tempChiTietSanPham.KichThuocDT = ChiTietSanPham.KichThuocDT.ToString();
            //tempChiTietSanPham.TrongLuong = float.Parse(ChiTietSanPham.TrongLuong.ToString());
            //tempChiTietSanPham.LoaiManHinh = ChiTietSanPham.LoaiManHinh.ToString();
            //tempChiTietSanPham.KichThuocManHinh = ChiTietSanPham.KichThuocManHinh.ToString();
            //tempChiTietSanPham.MoTaKichThuocManHinh = ChiTietSanPham.MoTaKichThuocManHinh.ToString();
            //tempChiTietSanPham.KieuChuong = ChiTietSanPham.KieuChuong.ToString();
            //tempChiTietSanPham.NgoAudio = ChiTietSanPham.NgoAudio;
            //tempChiTietSanPham.DanhBa = ChiTietSanPham.DanhBa.ToString();
            //tempChiTietSanPham.CacSoDaGoi = ChiTietSanPham.CacSoDaGoi.ToString();
            //tempChiTietSanPham.BoNhoTrong = ChiTietSanPham.BoNhoTrong.ToString();
            //tempChiTietSanPham.KheCamTheNho = ChiTietSanPham.KheCamTheNho;
            //tempChiTietSanPham.GPRS = ChiTietSanPham.GPRS;
            //tempChiTietSanPham.EDGE = ChiTietSanPham.EDGE;
            //tempChiTietSanPham.NFC = ChiTietSanPham.NFC;
            //tempChiTietSanPham.TocDo3G = ChiTietSanPham.TocDo3G.ToString();
            //tempChiTietSanPham.WLAN = ChiTietSanPham.WLAN.ToString();
            //tempChiTietSanPham.Bluetooth = ChiTietSanPham.Bluetooth.ToString();
            //tempChiTietSanPham.HongNgoai = ChiTietSanPham.HongNgoai;
            //tempChiTietSanPham.USB = ChiTietSanPham.USB.ToString();
            //tempChiTietSanPham.CameraChinh = ChiTietSanPham.CameraChinh.ToString();
            //tempChiTietSanPham.CameraPhu = ChiTietSanPham.CameraPhu.ToString();
            //tempChiTietSanPham.DacDiem = ChiTietSanPham.DacDiem.ToString();
            //tempChiTietSanPham.QuayPhim = ChiTietSanPham.QuayPhim.ToString();
            //tempChiTietSanPham.BoXuLy = ChiTietSanPham.BoXuLy.ToString();
            //tempChiTietSanPham.Chipset = ChiTietSanPham.Chipset.ToString();
            //tempChiTietSanPham.TrinhDuyet = ChiTietSanPham.TrinhDuyet.ToString();
            //tempChiTietSanPham.TinNhan = ChiTietSanPham.TinNhan.ToString();
            //tempChiTietSanPham.Radio = ChiTietSanPham.Radio;
            //tempChiTietSanPham.TroChoi = ChiTietSanPham.TroChoi.ToString();
            //tempChiTietSanPham.NgonNgu = ChiTietSanPham.NgonNgu.ToString();
            //tempChiTietSanPham.GPS = ChiTietSanPham.GPS.ToString();
            //tempChiTietSanPham.Java = ChiTietSanPham.Java.ToString();
            //tempChiTietSanPham.MoTaJava = ChiTietSanPham.MoTaJava.ToString();
            //tempChiTietSanPham.PinChuan = ChiTietSanPham.PinChuan.ToString();
            //tempChiTietSanPham.ThoiGianCho = ChiTietSanPham.ThoiGianCho.ToString();
            //tempChiTietSanPham.ThoiGianDamThoai = ChiTietSanPham.ThoiGianDamThoai.ToString();
            //tempChiTietSanPham.NgayCapNhat = DateTime.Now;


        }
        public void DeleteProductDAO(int MaSP)
        {
            SanPham s = webmob.SanPhams.Single(p => p.MaSP == MaSP);
            ChiTietSanPham ctsp = webmob.ChiTietSanPhams.Single(dp => dp.MaSP == MaSP);
            s.TrangThai = 2;
            ctsp.TrangThai = 2;
            webmob.SaveChanges();
        }
        
        //No admin//

        public int GetGiaFromMaSP(long MaSP)
        {
            short status = new TrangThaiBLL().GetTrangThai("active");
            var sp = webmob.SanPhams.Where(p => p.MaSP == MaSP && p.TrangThai1.TrangThai1 == "active").ToList();
            if (sp.Count > 0)
            {
                var km = sp.First().KhuyenMais.Where(p => p.NgayBD <= DateTime.Now 
                    && p.NgayKT >= DateTime.Now && p.TrangThai == status
                    && p.MaSP == MaSP && p.TrangThai1.TrangThai1 == "active").ToList();
                if (km.Count > 0)
                {
                    if (km.First().GiamGia == 0 || km.First().GiamGia == null)
                        return sp.First().DonGia;
                    else
                    {
                        int pt = km.First().GiamGia ?? 0;
                        double temp = (sp.First().DonGia - Math.Ceiling((float)sp.First().DonGia / 100 * pt));
                        return int.Parse(temp.ToString());
                    }
                }
                return sp.First().DonGia;
            }
            return 0;
        }

        public int GetGiaThatFromMaSP(long MaSP)
        {
            short status = new TrangThaiBLL().GetTrangThai("active");
            var sp = webmob.SanPhams.Where(p => p.MaSP == MaSP && p.TrangThai == status).ToList();
            if (sp.Count > 0)
            {
                return sp.First().DonGia;
            }
            return 0;
        }
        public List<SanPham> ShowSanPhamMoiNhat()
        {
            var sanphammoinhats = new MobileWebEntities().SanPhams.Where(p=>p.TrangThai1.TrangThai1 == "active").OrderByDescending(p => p.NgayTao).Take(8).ToList();
            foreach(var item in sanphammoinhats)
            {
                item.DonGia = GetGiaFromMaSP(item.MaSP);
            }
            return sanphammoinhats;
        }

        public List<KhuyenMai> ShowKhuyenMaiMoiNhat()
        {
            MobileWebEntities web = new MobileWebEntities();
            short status = new TrangThaiBLL().GetTrangThai("active");
            var khuyenmais = web.KhuyenMais.Where(p => p.NgayBD <= DateTime.Now && p.NgayKT >= DateTime.Now && p.TrangThai == status).OrderByDescending(p=>p.NgayTao);
            return khuyenmais.ToList();
        }
        /*Trang khuyến mãi*/
        public List<ProductM> ShowDanhMucKhuyenMai()
        {
            MobileWebEntities web = new MobileWebEntities();
            List<ProductM> kq = new List<ProductM>();
            short status = new TrangThaiBLL().GetTrangThai("active");
            var khuyenmais = web.KhuyenMais.Where(p => p.NgayBD <= DateTime.Now && p.NgayKT >= DateTime.Now && p.TrangThai == status).OrderByDescending(p => p.NgayTao);
            foreach (var item in khuyenmais.ToList())
            {
                ProductM pro = new ProductM();
                pro.KhuyenMai = item;
                pro.GiaKhuyenMai = GetGiaFromMaSP(item.MaSP);
                kq.Add(pro);
            }
            return kq;
        }

        public double CountPageProductM(List<ProductM> sanpham, int rows)
        {
            double pages = Math.Ceiling((float)sanpham.Count() / rows);
            return pages;
        }

        public List<ProductM> SortProductM(List<ProductM> sanphams, int pageNumber, int ssort, int rows)
        {
            if (ssort == -1 || ssort == 0)
            {
                sanphams = sanphams.OrderByDescending(p => p.KhuyenMai.NgayTao).Skip((pageNumber - 1) * rows).Take(rows).ToList();
            }
            else if (ssort == 1)
                sanphams = sanphams.OrderBy(p => p.KhuyenMai.SanPham.TenSP).Skip((pageNumber - 1) * rows).Take(rows).ToList();
            else if (ssort == 2)
                sanphams = sanphams.OrderBy(p => p.GiaKhuyenMai).Skip((pageNumber - 1) * rows).Take(rows).ToList();
            else if (ssort == 3)
                sanphams = sanphams.OrderByDescending(p => p.GiaKhuyenMai).Skip((pageNumber - 1) * rows).Take(rows).ToList();
            else if (ssort == 4)
                sanphams = sanphams.OrderByDescending(p => p.KhuyenMai.SanPham.SoLuong).Skip((pageNumber - 1) * rows).Take(rows).ToList();

            return sanphams;
        }
        /*Trang khuyến mãi*/

        public List<SanPham> ShowDSSanPhamTheoLoaiSanPham(string category, string carrier)
        {
            if (carrier != "All")
            {
                var mobiles = new MobileWebEntities().SanPhams.Where(p => p.LoaiSanPham.LinkName == category
                && p.NhaSanXuat.TenNSX == carrier && p.TrangThai1.TrangThai1 == "active").ToList();
                foreach (var item in mobiles)
                {
                    item.DonGia = GetGiaFromMaSP(item.MaSP);
                }
                return mobiles;
            }
            else
            {
                var mobiles = new MobileWebEntities().SanPhams.Where(p => p.LoaiSanPham.LinkName == category
                && p.TrangThai1.TrangThai1 == "active").ToList();
                foreach (var item in mobiles)
                {
                    item.DonGia = GetGiaFromMaSP(item.MaSP);
                }
                return mobiles;
            }
        }

        public List<SanPham> ShowDSSanPhamTheoTieuChi(SanPham sp)
        {
            var sanphams = new MobileWebEntities().SanPhams.ToList();

            if (sp.TenSP != "" && sp.TenSP != null)
            {
                sanphams = sanphams.Where(p => p.TenSP.ToLower().Contains(sp.TenSP.ToLower()) 
                    && p.TrangThai1.TrangThai1 == "active").ToList();
            }
            if (sp.MaNSX != -1)
            {
                string TenNSX = webmob.NhaSanXuats.Where(sx => sx.MaNSX == sp.MaNSX && sp.TrangThai1.TrangThai1 == "active").First().TenNSX;
                sanphams = sanphams.Where(p => p.NhaSanXuat.TenNSX == TenNSX).ToList();
            }

            if (sp.MaLoaiSP != -1)
            {
                sanphams = sanphams.Where(p => p.MaLoaiSP == sp.MaLoaiSP && sp.TrangThai1.TrangThai1 == "active").ToList();
            }

            if (sp.DonGia != 0)
            {
                if (sp.DonGia == 1)
                    sanphams = sanphams.Where(p => p.DonGia < 1500 && p.TrangThai1.TrangThai1 == "active").ToList();
                else if (sp.DonGia == 2)
                    sanphams = sanphams.Where(p => p.DonGia >= 1500 && p.DonGia < 4000 && p.TrangThai1.TrangThai1 == "active").ToList();
                else if (sp.DonGia == 3)
                    sanphams = sanphams.Where(p => p.DonGia >= 4000 && p.DonGia < 6000 && p.TrangThai1.TrangThai1 == "active").ToList();
                else if (sp.DonGia == 4)
                    sanphams = sanphams.Where(p => p.DonGia >= 6000 && p.DonGia < 9000 && p.TrangThai1.TrangThai1 == "active").ToList();
                else if (sp.DonGia == 5)
                    sanphams = sanphams.Where(p => p.DonGia >= 9000 && p.DonGia < 12000 && p.TrangThai1.TrangThai1 == "active").ToList();
                else if (sp.DonGia == 6)
                    sanphams = sanphams.Where(p => p.DonGia >= 1200 && p.TrangThai1.TrangThai1 == "active").ToList();
            }
            foreach (var item in sanphams)
            {
                item.DonGia = GetGiaFromMaSP(item.MaSP);
            }
            return sanphams;
        }

        public List<SanPham> ShowDSSanPhamTheoMucGia(int idSanPham)
        {
            int dongia = GetGiaFromMaSP(idSanPham);
            double mucgia = (dongia * 0.15);
            var sanphams = new MobileWebEntities().SanPhams.Where(p => p.DonGia <= dongia + mucgia && 
                p.DonGia >= dongia - mucgia && p.MaSP != idSanPham && p.TrangThai1.TrangThai1 == "active").Take(5).ToList();
            foreach (var item in sanphams)
            {
                item.DonGia = GetGiaFromMaSP(item.MaSP);
            }
            return sanphams;
        }
        
        public double CountPageProduct(List<SanPham> sanpham)
        {
            double pages = Math.Ceiling((float)sanpham.Count() / 9);
            return pages;
        }

        public List<SanPham> SortProduct(List<SanPham> sanphams, int pageNumber, int ssort)
        {
            if (ssort == -1 || ssort == 0)
            {
                sanphams = sanphams.OrderByDescending(p => p.NgayTao).Skip((pageNumber - 1) * 9).Take(9).ToList();
                ssort = 0;
            }else if (ssort == 1)
                sanphams = sanphams.OrderBy(p => p.TenSP).Skip((pageNumber - 1) * 9).Take(9).ToList();
            else if (ssort == 2)
                sanphams = sanphams.OrderBy(p => p.DonGia).Skip((pageNumber - 1) * 9).Take(9).ToList();
            else if (ssort == 3)
                sanphams = sanphams.OrderByDescending(p => p.DonGia).Skip((pageNumber - 1) * 9).Take(9).ToList();
            else if (ssort == 4)
                sanphams = sanphams.OrderByDescending(p => p.SoLuong).Skip((pageNumber - 1) * 9).Take(9).ToList();

            return sanphams;
        }

        public SanPham GetSanPham(int id)
        {
            var sp = new MobileWebEntities().SanPhams.Where(p => p.MaSP == id && p.TrangThai1.TrangThai1 == "active").ToList();
            if (sp.Count > 0)
            {
                var itemFirst = sp.First();
                itemFirst.DonGia = GetGiaFromMaSP(itemFirst.MaSP);
                return itemFirst;
            }
            return new SanPham();
        }

        public List<SanPham> ShowDSSanPhamBanChay()
        {
            MobileWebEntities web = new MobileWebEntities();
            DateTime now = DateTime.Now;
            DateTime muc = now.AddDays(-1800);
            var hds = web.HoaDons.Where(p=>p.TrangThai1.TrangThai1 == "active" && (p.TinhTrang == 3 || p.TinhTrang == 4 || p.TinhTrang == 5));
            var masps = ( from hd in hds join ct in web.ChiTietHoaDons
                             on hd.MaHD equals ct.MaHD
                             where hd.NgayTao >= muc
                             group ct by ct.MaSP into g
                             select new { MaSP = g.Key, SoLuongBan = g.Count()}
                );
            var sanphams = (from sp in web.SanPhams
                            join masp in masps
                            on sp.MaSP equals masp.MaSP
                            orderby masp.SoLuongBan descending
                            select sp);
            var pro = sanphams.Take(8).ToList();
            foreach (var item in pro)
            {
                item.DonGia = GetGiaFromMaSP(item.MaSP);
            }
            return pro;
        }

        public List<SanPham> ShowDSSanPhamLienQuan(int MaSP)
        {
            MobileWebEntities web = new MobileWebEntities();
            var dshd = web.HoaDons.Where(p => p.TrangThai1.TrangThai1 == "active" 
                && (p.TinhTrang == 3 || p.TinhTrang == 4 || p.TinhTrang == 5));
            var hds = (from hd in dshd
                       join ct in web.ChiTietHoaDons
                           on hd.MaHD equals ct.MaHD
                       where ct.MaSP == MaSP
                       select hd
                );
            var masps = (from hd in hds
                         join ct in web.ChiTietHoaDons
                             on hd.MaHD equals ct.MaHD
                         where ct.MaSP != MaSP
                         group ct by ct.MaSP into g
                         select new { MaSP = g.Key, SoLuongBan = g.Count() }
                );
            var sanphams = (from sp in web.SanPhams
                            join masp in masps
                                on sp.MaSP equals masp.MaSP
                            orderby masp.SoLuongBan descending
                            select sp);
            foreach (var item in sanphams)
            {
                item.DonGia = GetGiaFromMaSP(item.MaSP);
            }
            return sanphams.Take(5).ToList();
        }

        public void UpdateView(int MaSP)
        {            
            try
            {
                var sp = webmob.SanPhams.SingleOrDefault(p => p.MaSP == MaSP);
                sp.SoLuotXem++;
                webmob.SaveChanges();
            }
            catch
            {
                return;
            }
        }
        //No admin//
    }
}
