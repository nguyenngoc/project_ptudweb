﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MobileWebBLL.Models
{
    public class StatisticParts
    {
        public int Day { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public int Revenue { get; set; }
    }
}
