USE [master]
GO
/****** Object:  Database [MobileWeb]    Script Date: 6/23/2013 10:49:58 PM ******/
CREATE DATABASE [MobileWeb]
GO
ALTER DATABASE [MobileWeb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [MobileWeb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [MobileWeb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [MobileWeb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [MobileWeb] SET ARITHABORT OFF 
GO
ALTER DATABASE [MobileWeb] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [MobileWeb] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [MobileWeb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [MobileWeb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [MobileWeb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [MobileWeb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [MobileWeb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [MobileWeb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [MobileWeb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [MobileWeb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [MobileWeb] SET  ENABLE_BROKER 
GO
ALTER DATABASE [MobileWeb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [MobileWeb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [MobileWeb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [MobileWeb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [MobileWeb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [MobileWeb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [MobileWeb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [MobileWeb] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [MobileWeb] SET  MULTI_USER 
GO
ALTER DATABASE [MobileWeb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [MobileWeb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [MobileWeb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [MobileWeb] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [MobileWeb]
GO
/****** Object:  Table [dbo].[BinhLuan]    Script Date: 6/23/2013 10:49:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BinhLuan](
	[MaBinhLuan] [bigint] IDENTITY(1,1) NOT NULL,
	[MaSP] [bigint] NOT NULL,
	[NoiDung] [ntext] NOT NULL,
	[TenKH] [nvarchar](50) NULL,
	[Email] [varchar](100) NULL,
	[ThoiGianBinhLuan] [datetime] NULL,
	[TrangThai] [smallint] NULL,
	[NgayTao] [datetime] NULL,
	[NgayCapNhat] [datetime] NULL,
 CONSTRAINT [PK_BinhLuan] PRIMARY KEY CLUSTERED 
(
	[MaBinhLuan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ChiTietHoaDon]    Script Date: 6/23/2013 10:49:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChiTietHoaDon](
	[MaCTHD] [bigint] IDENTITY(1,1) NOT NULL,
	[MaHD] [bigint] NOT NULL,
	[MaSP] [bigint] NOT NULL,
	[SoLuong] [int] NOT NULL,
	[ThanhTien] [int] NOT NULL,
	[TrangThai] [smallint] NULL,
	[NgayTao] [datetime] NULL,
	[NgayCapNhat] [datetime] NULL,
 CONSTRAINT [PK_ChiTietHoaDon] PRIMARY KEY CLUSTERED 
(
	[MaCTHD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ChiTietSanPham]    Script Date: 6/23/2013 10:49:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChiTietSanPham](
	[MaChiTiet] [bigint] IDENTITY(1,1) NOT NULL,
	[MaSP] [bigint] NULL,
	[HDH] [nvarchar](50) NULL,
	[Mang2G] [nvarchar](100) NULL,
	[Mang3G] [nvarchar](100) NULL,
	[NgayRaMat] [date] NULL,
	[NgayCoHang] [date] NULL,
	[KichThuocDT] [nvarchar](60) NULL,
	[TrongLuong] [float] NULL,
	[LoaiManHinh] [ntext] NULL,
	[KichThuocManHinh] [ntext] NULL,
	[MoTaKichThuocManHinh] [ntext] NULL,
	[KieuChuong] [ntext] NULL,
	[NgoAudio] [bit] NOT NULL,
	[DanhBa] [ntext] NULL,
	[CacSoDaGoi] [ntext] NULL,
	[BoNhoTrong] [ntext] NULL,
	[KheCamTheNho] [bit] NOT NULL,
	[GPRS] [bit] NOT NULL,
	[EDGE] [bit] NOT NULL,
	[TocDo3G] [ntext] NULL,
	[NFC] [bit] NOT NULL,
	[WLAN] [ntext] NULL,
	[Bluetooth] [ntext] NULL,
	[HongNgoai] [bit] NOT NULL,
	[USB] [nvarchar](50) NULL,
	[CameraChinh] [ntext] NULL,
	[CameraPhu] [ntext] NULL,
	[DacDiem] [ntext] NULL,
	[QuayPhim] [ntext] NULL,
	[BoXuLy] [ntext] NULL,
	[Chipset] [nvarchar](300) NULL,
	[TinNhan] [ntext] NULL,
	[TrinhDuyet] [nvarchar](50) NULL,
	[Radio] [bit] NOT NULL,
	[TroChoi] [ntext] NULL,
	[NgonNgu] [ntext] NULL,
	[GPS] [ntext] NULL,
	[Java] [ntext] NULL,
	[MoTaJava] [ntext] NULL,
	[PinChuan] [ntext] NULL,
	[ThoiGianCho] [ntext] NULL,
	[ThoiGianDamThoai] [ntext] NULL,
	[TrangThai] [smallint] NULL,
	[NgayTao] [datetime] NULL,
	[NgayCapNhat] [datetime] NULL,
 CONSTRAINT [PK_ChiTietSanPham] PRIMARY KEY CLUSTERED 
(
	[MaChiTiet] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HinhAnh]    Script Date: 6/23/2013 10:49:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HinhAnh](
	[MaHinhAnh] [bigint] IDENTITY(1,1) NOT NULL,
	[MaSP] [bigint] NOT NULL,
	[Link] [varchar](100) NULL,
	[TrangThai] [smallint] NULL,
	[NgayTao] [datetime] NULL,
	[NgayCapNhat] [datetime] NULL,
 CONSTRAINT [PK_HinhAnh] PRIMARY KEY CLUSTERED 
(
	[MaHinhAnh] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HoaDon]    Script Date: 6/23/2013 10:49:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HoaDon](
	[MaHD] [bigint] IDENTITY(1,1) NOT NULL,
	[MaKH] [int] NULL,
	[TenKHDat] [nvarchar](50) NULL,
	[DienThoai] [varchar](15) NULL,
	[DiaChiGiaoHang] [nvarchar](60) NULL,
	[ThoiGianDatHang] [datetime] NULL,
	[HinhThucGiaoHang] [smallint] NOT NULL,
	[HinhThucThanhToan] [smallint] NOT NULL,
	[TongTien] [int] NULL,
	[TinhTrang] [smallint] NOT NULL,
	[TrangThai] [smallint] NULL,
	[NgayTao] [datetime] NULL,
	[NgayCapNhat] [datetime] NULL,
 CONSTRAINT [PK_HoaDon] PRIMARY KEY CLUSTERED 
(
	[MaHD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KhachHang]    Script Date: 6/23/2013 10:49:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KhachHang](
	[MaKH] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[TenKH] [nvarchar](50) NOT NULL,
	[DiaChi] [nvarchar](60) NULL,
	[Email] [varchar](100) NOT NULL,
	[DienThoai] [varchar](15) NULL,
	[TrangThai] [smallint] NULL,
	[NgayTao] [datetime] NULL,
	[NgayCapNhat] [datetime] NULL,
 CONSTRAINT [PK_KhachHang] PRIMARY KEY CLUSTERED 
(
	[MaKH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KhuyenMai]    Script Date: 6/23/2013 10:49:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KhuyenMai](
	[MaKhuyenMai] [bigint] IDENTITY(1,1) NOT NULL,
	[MaSP] [bigint] NOT NULL,
	[QuaTang] [int] NULL,
	[GiamGia] [int] NULL,
	[NgayBD] [date] NOT NULL,
	[NgayKT] [date] NOT NULL,
	[TrangThai] [smallint] NULL,
	[NgayTao] [datetime] NULL,
	[NgayCapNhat] [datetime] NULL,
 CONSTRAINT [PK_KhuyenMai] PRIMARY KEY CLUSTERED 
(
	[MaKhuyenMai] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiSanPham]    Script Date: 6/23/2013 10:49:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[LoaiSanPham](
	[MaLoaiSP] [smallint] IDENTITY(1,1) NOT NULL,
	[TenLoaiSP] [nvarchar](60) NOT NULL,
	[TrangThai] [smallint] NULL,
	[NgayTao] [datetime] NULL,
	[NgayCapNhat] [datetime] NULL,
	[LinkName] [varchar](30) NULL,
 CONSTRAINT [PK_LoaiSanPham] PRIMARY KEY CLUSTERED 
(
	[MaLoaiSP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MauSac]    Script Date: 6/23/2013 10:49:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MauSac](
	[MaMauSac] [int] IDENTITY(1,1) NOT NULL,
	[TenMauSac] [nvarchar](50) NOT NULL,
	[TrangThai] [smallint] NULL,
	[NgayTao] [datetime] NULL,
	[NgayCapNhat] [datetime] NULL,
 CONSTRAINT [PK_MauSac] PRIMARY KEY CLUSTERED 
(
	[MaMauSac] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MauSacSanPham]    Script Date: 6/23/2013 10:49:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MauSacSanPham](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MaSP] [bigint] NOT NULL,
	[MaMS] [int] NOT NULL,
	[SoLuong] [int] NOT NULL,
	[TrangThai] [smallint] NULL,
	[NgayTao] [datetime] NULL,
	[NgayCapNhat] [datetime] NULL,
 CONSTRAINT [PK_MauSacSanPham] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NhanVien]    Script Date: 6/23/2013 10:49:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NhanVien](
	[MaNV] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[TenNV] [nvarchar](50) NULL,
	[ChucVu] [nvarchar](50) NULL,
	[TrangThai] [smallint] NULL,
	[NgayTao] [datetime] NULL,
	[NgayCapNhat] [datetime] NULL,
 CONSTRAINT [PK_NhanVien] PRIMARY KEY CLUSTERED 
(
	[MaNV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NhaSanXuat]    Script Date: 6/23/2013 10:49:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NhaSanXuat](
	[MaNSX] [int] IDENTITY(1,1) NOT NULL,
	[TenNSX] [nvarchar](50) NOT NULL,
	[LoaiSP] [smallint] NULL,
	[DiaChi] [nvarchar](60) NULL,
	[DienThoai] [varchar](50) NULL,
	[TrangThai] [smallint] NULL,
	[NgayTao] [datetime] NULL,
	[NgayCapNhat] [datetime] NULL,
 CONSTRAINT [PK_NhaSanXuat] PRIMARY KEY CLUSTERED 
(
	[MaNSX] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QuaTang]    Script Date: 6/23/2013 10:49:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuaTang](
	[MaQuaTang] [int] IDENTITY(1,1) NOT NULL,
	[TenQuaTang] [nvarchar](100) NOT NULL,
	[TrangThai] [smallint] NULL,
	[NgayTao] [datetime] NULL,
	[NgayCapNhat] [datetime] NULL,
 CONSTRAINT [PK_QuanTang] PRIMARY KEY CLUSTERED 
(
	[MaQuaTang] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SanPham]    Script Date: 6/23/2013 10:49:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SanPham](
	[MaSP] [bigint] IDENTITY(1,1) NOT NULL,
	[TenSP] [nvarchar](100) NOT NULL,
	[MaLoaiSP] [smallint] NOT NULL,
	[MaNSX] [int] NOT NULL,
	[SoLuong] [int] NOT NULL,
	[DonGia] [int] NOT NULL,
	[TinhTrang] [bit] NOT NULL,
	[BaoHanh] [smallint] NULL,
	[SoLuotXem] [int] NOT NULL,
	[SanPhamKemTheo] [ntext] NULL,
	[TrangThai] [smallint] NULL,
	[NgayTao] [datetime] NULL,
	[NgayCapNhat] [datetime] NULL,
 CONSTRAINT [PK_SanPham] PRIMARY KEY CLUSTERED 
(
	[MaSP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Slider]    Script Date: 6/23/2013 10:49:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Slider](
	[MaSlider] [int] IDENTITY(1,1) NOT NULL,
	[Link] [varchar](100) NULL,
	[TrangThai] [smallint] NULL,
	[NgayTao] [datetime] NULL,
	[NgayCapNhat] [datetime] NULL,
 CONSTRAINT [PK_Slider] PRIMARY KEY CLUSTERED 
(
	[MaSlider] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TrangThai]    Script Date: 6/23/2013 10:49:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TrangThai](
	[MaTrangThai] [smallint] IDENTITY(1,1) NOT NULL,
	[TrangThai] [nvarchar](30) NOT NULL,
 CONSTRAINT [PK_TrangThai] PRIMARY KEY CLUSTERED 
(
	[MaTrangThai] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserProfile]    Script Date: 6/23/2013 10:49:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserProfile](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[webpages_Membership]    Script Date: 6/23/2013 10:49:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_Membership](
	[UserId] [int] NOT NULL,
	[CreateDate] [datetime] NULL,
	[ConfirmationToken] [nvarchar](128) NULL,
	[IsConfirmed] [bit] NULL,
	[LastPasswordFailureDate] [datetime] NULL,
	[PasswordFailuresSinceLastSuccess] [int] NOT NULL,
	[Password] [nvarchar](128) NOT NULL,
	[PasswordChangedDate] [datetime] NULL,
	[PasswordSalt] [nvarchar](128) NOT NULL,
	[PasswordVerificationToken] [nvarchar](128) NULL,
	[PasswordVerificationTokenExpirationDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[webpages_OAuthMembership]    Script Date: 6/23/2013 10:49:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_OAuthMembership](
	[Provider] [nvarchar](30) NOT NULL,
	[ProviderUserId] [nvarchar](100) NOT NULL,
	[UserId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Provider] ASC,
	[ProviderUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[webpages_Roles]    Script Date: 6/23/2013 10:49:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_Roles](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](256) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[webpages_UsersInRoles]    Script Date: 6/23/2013 10:49:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_UsersInRoles](
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[BinhLuan] ON 

INSERT [dbo].[BinhLuan] ([MaBinhLuan], [MaSP], [NoiDung], [TenKH], [Email], [ThoiGianBinhLuan], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (1, 37, N'Thật đẹp !! Rất muốn mua !! Thật tuyệt vời!!', N'anhminh', N'', CAST(0x0000A1E2014DC3F8 AS DateTime), 1, CAST(0x0000A1E2014DC3F7 AS DateTime), CAST(0x0000A1E2014DC3F7 AS DateTime))
INSERT [dbo].[BinhLuan] ([MaBinhLuan], [MaSP], [NoiDung], [TenKH], [Email], [ThoiGianBinhLuan], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (2, 19, N'Rất muốn mua !! Thật tuyệt vời!!', N'anhminh', N'', CAST(0x0000A1E2014DD344 AS DateTime), 1, CAST(0x0000A1E2014DD344 AS DateTime), CAST(0x0000A1E2014DD344 AS DateTime))
INSERT [dbo].[BinhLuan] ([MaBinhLuan], [MaSP], [NoiDung], [TenKH], [Email], [ThoiGianBinhLuan], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (3, 36, N'Rất muốn mua !! Thật tuyệt vời!!', N'thaichau', N'', CAST(0x0000A1E2014DFF9E AS DateTime), 1, CAST(0x0000A1E2014DFF9E AS DateTime), CAST(0x0000A1E2014DFF9E AS DateTime))
INSERT [dbo].[BinhLuan] ([MaBinhLuan], [MaSP], [NoiDung], [TenKH], [Email], [ThoiGianBinhLuan], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (4, 27, N'Wow!! Beautiful!!!', N'mydung', N'', CAST(0x0000A1E600DC7CA2 AS DateTime), 1, CAST(0x0000A1E600DC7CA2 AS DateTime), CAST(0x0000A1E600DC7CA2 AS DateTime))
INSERT [dbo].[BinhLuan] ([MaBinhLuan], [MaSP], [NoiDung], [TenKH], [Email], [ThoiGianBinhLuan], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (5, 42, N'Wow !!', N'Minh', N'anhminh@gmail.com', CAST(0x0000A1E600E071D9 AS DateTime), 1, CAST(0x0000A1E600E071D9 AS DateTime), CAST(0x0000A1E600E071D9 AS DateTime))
INSERT [dbo].[BinhLuan] ([MaBinhLuan], [MaSP], [NoiDung], [TenKH], [Email], [ThoiGianBinhLuan], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (6, 42, N'Wow !!', N'Minh', N'anhminh@gmail.com', CAST(0x0000A1E600E0929A AS DateTime), 1, CAST(0x0000A1E600E0929A AS DateTime), CAST(0x0000A1E600E0929A AS DateTime))
INSERT [dbo].[BinhLuan] ([MaBinhLuan], [MaSP], [NoiDung], [TenKH], [Email], [ThoiGianBinhLuan], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (7, 42, N'Wow !!', N'Minh', N'anhminh@gmail.com', CAST(0x0000A1E600E0944A AS DateTime), 1, CAST(0x0000A1E600E0944A AS DateTime), CAST(0x0000A1E600E0944A AS DateTime))
INSERT [dbo].[BinhLuan] ([MaBinhLuan], [MaSP], [NoiDung], [TenKH], [Email], [ThoiGianBinhLuan], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (8, 42, N'Wow !!', N'Minh', N'anhminh@gmail.com', CAST(0x0000A1E600E09801 AS DateTime), 1, CAST(0x0000A1E600E09801 AS DateTime), CAST(0x0000A1E600E09801 AS DateTime))
INSERT [dbo].[BinhLuan] ([MaBinhLuan], [MaSP], [NoiDung], [TenKH], [Email], [ThoiGianBinhLuan], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (9, 42, N'Wow !!', N'Minh', N'anhminh@gmail.com', CAST(0x0000A1E600E09A00 AS DateTime), 1, CAST(0x0000A1E600E09A00 AS DateTime), CAST(0x0000A1E600E09A00 AS DateTime))
INSERT [dbo].[BinhLuan] ([MaBinhLuan], [MaSP], [NoiDung], [TenKH], [Email], [ThoiGianBinhLuan], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (10, 42, N'Wow !!', N'Minh', N'anhminh@gmail.com', CAST(0x0000A1E600E0A076 AS DateTime), 1, CAST(0x0000A1E600E0A076 AS DateTime), CAST(0x0000A1E600E0A076 AS DateTime))
SET IDENTITY_INSERT [dbo].[BinhLuan] OFF
SET IDENTITY_INSERT [dbo].[ChiTietHoaDon] ON 

INSERT [dbo].[ChiTietHoaDon] ([MaCTHD], [MaHD], [MaSP], [SoLuong], [ThanhTien], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (1, 1, 42, 2, 3900, 1, CAST(0x0000A1E201454C18 AS DateTime), CAST(0x0000A1E201454C18 AS DateTime))
INSERT [dbo].[ChiTietHoaDon] ([MaCTHD], [MaHD], [MaSP], [SoLuong], [ThanhTien], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (2, 2, 42, 1, 1950, 1, CAST(0x0000A1E20145B1B7 AS DateTime), CAST(0x0000A1E20145B1B7 AS DateTime))
INSERT [dbo].[ChiTietHoaDon] ([MaCTHD], [MaHD], [MaSP], [SoLuong], [ThanhTien], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (3, 2, 41, 1, 1590, 1, CAST(0x0000A1E20145B1B8 AS DateTime), CAST(0x0000A1E20145B1B8 AS DateTime))
INSERT [dbo].[ChiTietHoaDon] ([MaCTHD], [MaHD], [MaSP], [SoLuong], [ThanhTien], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (4, 3, 37, 1, 1190, 1, CAST(0x0000A1E201480CEE AS DateTime), CAST(0x0000A1E201480CEE AS DateTime))
INSERT [dbo].[ChiTietHoaDon] ([MaCTHD], [MaHD], [MaSP], [SoLuong], [ThanhTien], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (5, 4, 37, 1, 1190, 1, CAST(0x0000A1E2014AFACD AS DateTime), CAST(0x0000A1E2014AFACD AS DateTime))
INSERT [dbo].[ChiTietHoaDon] ([MaCTHD], [MaHD], [MaSP], [SoLuong], [ThanhTien], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (6, 4, 36, 1, 1270, 1, CAST(0x0000A1E2014AFACD AS DateTime), CAST(0x0000A1E2014AFACD AS DateTime))
INSERT [dbo].[ChiTietHoaDon] ([MaCTHD], [MaHD], [MaSP], [SoLuong], [ThanhTien], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (7, 5, 36, 1, 1270, 1, CAST(0x0000A1E2014B5DD9 AS DateTime), CAST(0x0000A1E2014B5DD9 AS DateTime))
INSERT [dbo].[ChiTietHoaDon] ([MaCTHD], [MaHD], [MaSP], [SoLuong], [ThanhTien], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (8, 5, 31, 1, 5420, 1, CAST(0x0000A1E2014B5DDA AS DateTime), CAST(0x0000A1E2014B5DDA AS DateTime))
INSERT [dbo].[ChiTietHoaDon] ([MaCTHD], [MaHD], [MaSP], [SoLuong], [ThanhTien], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (9, 6, 31, 1, 5420, 1, CAST(0x0000A1E2014BA2A1 AS DateTime), CAST(0x0000A1E2014BA2A1 AS DateTime))
INSERT [dbo].[ChiTietHoaDon] ([MaCTHD], [MaHD], [MaSP], [SoLuong], [ThanhTien], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (10, 6, 30, 2, 29780, 1, CAST(0x0000A1E2014BA2A2 AS DateTime), CAST(0x0000A1E2014BA2A2 AS DateTime))
INSERT [dbo].[ChiTietHoaDon] ([MaCTHD], [MaHD], [MaSP], [SoLuong], [ThanhTien], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (11, 7, 19, 2, 19780, 1, CAST(0x0000A1E2015EF60F AS DateTime), CAST(0x0000A1E2015EF60F AS DateTime))
INSERT [dbo].[ChiTietHoaDon] ([MaCTHD], [MaHD], [MaSP], [SoLuong], [ThanhTien], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (12, 7, 27, 1, 7250, 1, CAST(0x0000A1E2015EF610 AS DateTime), CAST(0x0000A1E2015EF610 AS DateTime))
INSERT [dbo].[ChiTietHoaDon] ([MaCTHD], [MaHD], [MaSP], [SoLuong], [ThanhTien], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (13, 8, 19, 1, 9890, 1, CAST(0x0000A1E60030162A AS DateTime), CAST(0x0000A1E60030162A AS DateTime))
INSERT [dbo].[ChiTietHoaDon] ([MaCTHD], [MaHD], [MaSP], [SoLuong], [ThanhTien], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (14, 8, 14, 1, 11029, 1, CAST(0x0000A1E60030162F AS DateTime), CAST(0x0000A1E60030162F AS DateTime))
INSERT [dbo].[ChiTietHoaDon] ([MaCTHD], [MaHD], [MaSP], [SoLuong], [ThanhTien], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (15, 9, 19, 1, 9890, 1, CAST(0x0000A1E60030F892 AS DateTime), CAST(0x0000A1E60030F892 AS DateTime))
INSERT [dbo].[ChiTietHoaDon] ([MaCTHD], [MaHD], [MaSP], [SoLuong], [ThanhTien], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (16, 9, 14, 1, 11029, 1, CAST(0x0000A1E60030F893 AS DateTime), CAST(0x0000A1E60030F893 AS DateTime))
INSERT [dbo].[ChiTietHoaDon] ([MaCTHD], [MaHD], [MaSP], [SoLuong], [ThanhTien], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (17, 9, 9, 1, 2090, 1, CAST(0x0000A1E60030F894 AS DateTime), CAST(0x0000A1E60030F894 AS DateTime))
INSERT [dbo].[ChiTietHoaDon] ([MaCTHD], [MaHD], [MaSP], [SoLuong], [ThanhTien], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (18, 9, 8, 1, 2386, 1, CAST(0x0000A1E60030F896 AS DateTime), CAST(0x0000A1E60030F896 AS DateTime))
INSERT [dbo].[ChiTietHoaDon] ([MaCTHD], [MaHD], [MaSP], [SoLuong], [ThanhTien], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (19, 10, 19, 1, 9890, 1, CAST(0x0000A1E600CB239B AS DateTime), CAST(0x0000A1E600CB239B AS DateTime))
INSERT [dbo].[ChiTietHoaDon] ([MaCTHD], [MaHD], [MaSP], [SoLuong], [ThanhTien], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (20, 11, 36, 1, 1270, 1, CAST(0x0000A1E600E2FEC4 AS DateTime), CAST(0x0000A1E600E2FEC4 AS DateTime))
SET IDENTITY_INSERT [dbo].[ChiTietHoaDon] OFF
SET IDENTITY_INSERT [dbo].[ChiTietSanPham] ON 

INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (1, 6, N'Microsoft Windows Phone 8', N'GSM 850 / 900 / 1800 / 1900', N'HSDPA 850 / 900 / 1900 / 2100', CAST(0x2B370B00 AS Date), CAST(0x3D370B00 AS Date), N'130.3 x 70.8 x 10.7 mm, 99 cc', 185, N'Màn hình cảm ứng điện dung IPS TFT, 16 triệu màu', N'768 x 1280 pixels, 4.5 inches', N'- Cảm ứng đa điểm
- Mặt kính Corning Gorilla Glass
- Công nghệ hiển thị PureMotion HD+
- Cảm biến gia tốc
- Cảm biến ánh sáng
- Cảm biến con quay hồi chuyển
- Cảm biến la bàn số', N'Báo rung, nhạc chuông MP3, WAV', 1, N'Khả năng lưu các mục và fields không giới hạn, danh bạ hình ảnh', N'Khả năng lưu không giới hạn', N'32 GB storage, 1 GB RAM', 0, 1, 1, N'HSDPA, 42 Mbps; HSUPA, 5.76 Mbps', 0, N'Wi-Fi 802.11 a/b/g/n, Wi-Fi Direct', N'Có, v3.1 với A2DP, EDR', 0, N'Có, microUSB v2.0, hỗ trợ USB On-the-go', N'8.7 MP, 3264 x 2448 pixels, Carl Zeiss optics, optical image stabilization, autofocus, LED flash', N'Có, 1.2MP', N'Công nghệ PureView, geo-tagging', N'Có, 1080p@30fps', N'Dual-core 1.5 GHz Krait, Adreno 225 GPU, chipset Qualcomm MSM8960 Snapdragon', NULL, N'SMS (threaded view), MMS, Email, Push Email, IM', N'Internet Explorer 10 ', 0, N'	Có, có thể tải thêm', N'Tiếng Anh, Tiếng Việt', N'Có, hỗ trợ A-GPS và GLONASS', N'Không', NULL, N'Li-Ion 2000mAh', NULL, NULL, 1, CAST(0x0000A1E2010A3AF3 AS DateTime), CAST(0x0000A1E2010A3AF3 AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (2, 7, N'Microsoft Windows Phone 7.5 Mango', N'GSM 850 / 900 / 1800 / 1900', N'HSDPA 14.4 Mbps, HSUPA 5.76 Mbps', CAST(0x2F370B00 AS Date), CAST(0x2F370B00 AS Date), N'119 x 62.4 x 12.5 mm, 81.1 cc', 125.5, N'Màn hình cảm ứng điện dung TFT, 16 triệu màu', N'480 x 800 pixels, 3.7 inches', N'- Công nghệ hiển thị Nokia ClearBlack
- Cảm ứng đa điểm
- Cảm biến gia tốc
- Cảm biến ánh sáng', N'Báo rung, nhạc chuông Mp3, WAV', 1, N'Khả năng lưu không giới hạn các mục và fields, danh bạ hình ảnh', N'Có nhiều', N'8 GB, 512 MB RAM', 0, 1, 1, N'HSDPA 14.4 Mbps, HSUPA 5.76 Mbps', 0, N'Wi-Fi 802.11 b/g/n', N'Có, v2.1 với A2DP, EDR', 0, N'Có, microUSB v2.0', N'5 MP, 2592х1944 pixels, autofocus, LED flash', N'Không', N'Geo-tagging, nhận diện khuôn mặt', N'720p@30fps', N'1.4 GHz, Qualcomm MSM8255 chipset, 3D Graphics HW Accelerator', NULL, N'SMS (threaded view), MMS, Email, Push Email, IM', N'WAP 2.0/xHTML, HTML5, RSS feeds', 1, N'Có, có thể tải thêm', N'Tiếng Anh', N'Có, hỗ trợ A-GPS', N'Có, MIDP 2.1', N'- Chỉ hỗ trợ MicroSIM
- Tích hợp mạng xã hội
- Chống ồn với mic chuyên dụng
- La bàn số
- Nghe nhạc MP3/WAV/eAAC+/WMA
- Xem video MP4/H.264/H.263/WMV
- Chỉnh sửa văn bản với Microsoft Office
- Chỉnh sửa hình ảnh / video
- Ghi âm / Quay số / Ra lệnh bằng giọng nói
- Nhập liệu đoán trước từ ', N'Pin chuẩn Li-Ion 1300 mAh (BP-3L)', NULL, NULL, 1, CAST(0x0000A1E2010EBA02 AS DateTime), CAST(0x0000A1E2010EBA02 AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (3, 8, N'Nokia S.40', NULL, NULL, NULL, NULL, NULL, 95, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, NULL, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A1E201141D56 AS DateTime), CAST(0x0000A1E201141D56 AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (4, 9, N'Nokia S.40', N'GSM 850 / 900 / 1800 / 1900', N'HSDPA 850 / 900 / 1700 / 1900 / 2100', CAST(0x2B370B00 AS Date), CAST(0x3D370B00 AS Date), N'130.3 x 70.8 x 10.7 mm, 99 cc', 95, N'Màn hình cảm ứng điện trở TFT, 56K màu', N'240 x 400 pixels, 3.0 inches', N'- Cảm ứng đa điểm
- Mặt kính Corning Gorilla Glass
- Cảm biến gia tốc
- Cảm biến ánh sáng', N'Báo rung, nhạc chuông MP3, WAV', 1, N'Có nhiều, danh bạ hình ảnh', N'Có nhiều', N'140 MB, 256 MB ROM, 128 MB RAM', 1, 1, 0, N'HSDPA, 14.4 Mbps; HSUPA, 5.76 Mbps', 0, N'Wi-Fi 802.11 b/g/n', N'Có, v2.1 với EDR', 0, N'Có, microUSB v2.0, hỗ trợ USB On-the-go', N'3.15 MP, 2048x1536 pixels', N'Không', NULL, N'	Có, VGA@25fps', N'1 GHz', NULL, N'SMS, MMS, Email, IM', N'WAP 2.0/xHTML, HTML', 1, N'Có, có thể tải thêm', N'Tiếng Anh, Tiếng Việt', N'Không', N'Có, MIDP 2.1', N'- Tích hợp mạng xã hội
- Xem video MP4/H.264/H.263/WMV
- Nghe nhạc MP3/WAV/WMA/AAC
- Lịch tổ chức
- Ghi âm giọng nói
- Nhập liệu đoán trước từ', N'Li-Ion 1110 mAh (BL-4U)', NULL, NULL, 1, NULL, CAST(0x0000A1E20115985B AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (5, 10, N'Nokia S.40', N'GSM 850 / 900 / 1800 / 1900', N'HSDPA 850 / 900 / 1700 / 1900 / 2100', CAST(0x2B370B00 AS Date), CAST(0x3D370B00 AS Date), N'130.3 x 70.8 x 10.7 mm, 99 cc', 95, N'Màn hình cảm ứng điện trở TFT, 56K màu', N'240 x 400 pixels, 3.0 inches', N'- Cảm ứng đa điểm
- Mặt kính Corning Gorilla Glass
- Cảm biến gia tốc
- Cảm biến ánh sáng', N'Báo rung, nhạc chuông MP3, WAV', 1, N'Có nhiều, danh bạ hình ảnh', N'Có nhiều', N'140 MB, 256 MB ROM, 128 MB RAM', 1, 1, 0, N'HSDPA, 14.4 Mbps; HSUPA, 5.76 Mbps', 0, N'Wi-Fi 802.11 b/g/n', N'Có, v2.1 với EDR', 0, N'Có, microUSB v2.0, hỗ trợ USB On-the-go', N'3.15 MP, 2048x1536 pixels', N'Không', NULL, N'	Có, VGA@25fps', N'1 GHz', NULL, N'SMS, MMS, Email, IM', N'WAP 2.0/xHTML, HTML', 1, N'Có, có thể tải thêm', N'Tiếng Anh, Tiếng Việt', N'Không', N'Có, MIDP 2.1', N'- Tích hợp mạng xã hội
- Xem video MP4/H.264/H.263/WMV
- Nghe nhạc MP3/WAV/WMA/AAC
- Lịch tổ chức
- Ghi âm giọng nói
- Nhập liệu đoán trước từ', N'Li-Ion 1110 mAh (BL-4U)', NULL, NULL, 1, CAST(0x0000A1E20115E443 AS DateTime), CAST(0x0000A1E20115E443 AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (6, 11, N'Nokia S.40', N'GSM 850 / 900 / 1800 / 1900', N'HSDPA 850 / 900 / 1700 / 1900 / 2100', CAST(0x2B370B00 AS Date), CAST(0x3D370B00 AS Date), N'130.3 x 70.8 x 10.7 mm, 99 cc', 95, N'Màn hình cảm ứng điện trở TFT, 56K màu', N'240 x 400 pixels, 3.0 inches', N'- Cảm ứng đa điểm
- Mặt kính Corning Gorilla Glass
- Cảm biến gia tốc
- Cảm biến ánh sáng', N'Báo rung, nhạc chuông MP3, WAV', 1, N'Có nhiều, danh bạ hình ảnh', N'Có nhiều', N'140 MB, 256 MB ROM, 128 MB RAM', 1, 1, 0, N'HSDPA, 14.4 Mbps; HSUPA, 5.76 Mbps', 0, N'Wi-Fi 802.11 b/g/n', N'Có, v2.1 với EDR', 0, N'Có, microUSB v2.0, hỗ trợ USB On-the-go', N'3.15 MP, 2048x1536 pixels', N'Không', NULL, N'	Có, VGA@25fps', N'1 GHz', NULL, N'SMS, MMS, Email, IM', N'WAP 2.0/xHTML, HTML', 1, N'Có, có thể tải thêm', N'Tiếng Anh, Tiếng Việt', N'Không', N'Có, MIDP 2.1', N'- Tích hợp mạng xã hội
- Xem video MP4/H.264/H.263/WMV
- Nghe nhạc MP3/WAV/WMA/AAC
- Lịch tổ chức
- Ghi âm giọng nói
- Nhập liệu đoán trước từ', N'Li-Ion 1110 mAh (BL-4U)', NULL, NULL, 1, CAST(0x0000A1E201168A88 AS DateTime), CAST(0x0000A1E201168A88 AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (7, 12, N'Nokia S.40', N'GSM 850 / 900 / 1800 / 1900', N'HSDPA 850 / 900 / 1700 / 1900 / 2100', CAST(0x2B370B00 AS Date), CAST(0x3D370B00 AS Date), N'130.3 x 70.8 x 10.7 mm, 99 cc', 95, N'Màn hình cảm ứng điện trở TFT, 56K màu', N'240 x 400 pixels, 3.0 inches', N'- Cảm ứng đa điểm
- Mặt kính Corning Gorilla Glass
- Cảm biến gia tốc
- Cảm biến ánh sáng', N'Báo rung, nhạc chuông MP3, WAV', 1, N'Có nhiều, danh bạ hình ảnh', N'Có nhiều', N'140 MB, 256 MB ROM, 128 MB RAM', 1, 1, 0, N'HSDPA, 14.4 Mbps; HSUPA, 5.76 Mbps', 0, N'Wi-Fi 802.11 b/g/n', N'Có, v2.1 với EDR', 0, N'Có, microUSB v2.0, hỗ trợ USB On-the-go', N'3.15 MP, 2048x1536 pixels', N'Không', NULL, N'	Có, VGA@25fps', N'1 GHz', NULL, N'SMS, MMS, Email, IM', N'WAP 2.0/xHTML, HTML', 1, N'Có, có thể tải thêm', N'Tiếng Anh, Tiếng Việt', N'Không', N'Có, MIDP 2.1', N'- Tích hợp mạng xã hội
- Xem video MP4/H.264/H.263/WMV
- Nghe nhạc MP3/WAV/WMA/AAC
- Lịch tổ chức
- Ghi âm giọng nói
- Nhập liệu đoán trước từ', N'Li-Ion 1110 mAh (BL-4U)', NULL, NULL, 1, CAST(0x0000A1E201172F7B AS DateTime), CAST(0x0000A1E201172F7B AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (8, 13, N'Nokia S.40', N'GSM 850 / 900 / 1800 / 1900', N'HSDPA 850 / 900 / 1700 / 1900 / 2100', CAST(0x2B370B00 AS Date), CAST(0x3D370B00 AS Date), N'130.3 x 70.8 x 10.7 mm, 99 cc', 95, N'Màn hình cảm ứng điện trở TFT, 56K màu', N'240 x 400 pixels, 3.0 inches', N'- Cảm ứng đa điểm
- Mặt kính Corning Gorilla Glass
- Cảm biến gia tốc
- Cảm biến ánh sáng', N'Báo rung, nhạc chuông MP3, WAV', 1, N'Có nhiều, danh bạ hình ảnh', N'Có nhiều', N'140 MB, 256 MB ROM, 128 MB RAM', 1, 1, 0, N'HSDPA, 14.4 Mbps; HSUPA, 5.76 Mbps', 0, N'Wi-Fi 802.11 b/g/n', N'Có, v2.1 với EDR', 0, N'Có, microUSB v2.0, hỗ trợ USB On-the-go', N'3.15 MP, 2048x1536 pixels', N'Không', NULL, N'	Có, VGA@25fps', N'1 GHz', NULL, N'SMS, MMS, Email, IM', N'WAP 2.0/xHTML, HTML', 1, N'Có, có thể tải thêm', N'Tiếng Anh, Tiếng Việt', N'Không', N'Có, MIDP 2.1', N'- Tích hợp mạng xã hội
- Xem video MP4/H.264/H.263/WMV
- Nghe nhạc MP3/WAV/WMA/AAC
- Lịch tổ chức
- Ghi âm giọng nói
- Nhập liệu đoán trước từ', N'Li-Ion 1110 mAh (BL-4U)', NULL, NULL, 1, CAST(0x0000A1E20117FF1E AS DateTime), CAST(0x0000A1E20117FF1E AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (9, 14, N'Nokia S.40', N'GSM 850 / 900 / 1800 / 1900', N'HSDPA 850 / 900 / 1700 / 1900 / 2100', CAST(0x2B370B00 AS Date), CAST(0x3D370B00 AS Date), N'130.3 x 70.8 x 10.7 mm, 99 cc', 95, N'Màn hình cảm ứng điện trở TFT, 56K màu', N'240 x 400 pixels, 3.0 inches', N'- Cảm ứng đa điểm
- Mặt kính Corning Gorilla Glass
- Cảm biến gia tốc
- Cảm biến ánh sáng', N'Báo rung, nhạc chuông MP3, WAV', 1, N'Có nhiều, danh bạ hình ảnh', N'Có nhiều', N'140 MB, 256 MB ROM, 128 MB RAM', 1, 1, 0, N'HSDPA, 14.4 Mbps; HSUPA, 5.76 Mbps', 0, N'Wi-Fi 802.11 b/g/n', N'Có, v2.1 với EDR', 0, N'Có, microUSB v2.0, hỗ trợ USB On-the-go', N'3.15 MP, 2048x1536 pixels', N'Không', NULL, N'	Có, VGA@25fps', N'1 GHz', NULL, N'SMS, MMS, Email, IM', N'WAP 2.0/xHTML, HTML', 1, N'Có, có thể tải thêm', N'Tiếng Anh, Tiếng Việt', N'Không', N'Có, MIDP 2.1', N'- Tích hợp mạng xã hội
- Xem video MP4/H.264/H.263/WMV
- Nghe nhạc MP3/WAV/WMA/AAC
- Lịch tổ chức
- Ghi âm giọng nói
- Nhập liệu đoán trước từ', N'Li-Ion 1110 mAh (BL-4U)', NULL, NULL, 1, CAST(0x0000A1E20118C02D AS DateTime), CAST(0x0000A1E20118C02D AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (10, 15, N'Nokia S.40', N'GSM 850 / 900 / 1800 / 1900', N'HSDPA 850 / 900 / 1700 / 1900 / 2100', CAST(0x2B370B00 AS Date), CAST(0x3D370B00 AS Date), N'130.3 x 70.8 x 10.7 mm, 99 cc', 95, N'Màn hình cảm ứng điện trở TFT, 56K màu', N'240 x 400 pixels, 3.0 inches', N'- Cảm ứng đa điểm
- Mặt kính Corning Gorilla Glass
- Cảm biến gia tốc
- Cảm biến ánh sáng', N'Báo rung, nhạc chuông MP3, WAV', 1, N'Có nhiều, danh bạ hình ảnh', N'Có nhiều', N'140 MB, 256 MB ROM, 128 MB RAM', 1, 1, 0, N'HSDPA, 14.4 Mbps; HSUPA, 5.76 Mbps', 0, N'Wi-Fi 802.11 b/g/n', N'Có, v2.1 với EDR', 0, N'Có, microUSB v2.0, hỗ trợ USB On-the-go', N'3.15 MP, 2048x1536 pixels', N'Không', NULL, N'	Có, VGA@25fps', N'1 GHz', NULL, N'SMS, MMS, Email, IM', N'WAP 2.0/xHTML, HTML', 1, N'Có, có thể tải thêm', N'Tiếng Anh, Tiếng Việt', N'Không', N'Có, MIDP 2.1', N'- Tích hợp mạng xã hội
- Xem video MP4/H.264/H.263/WMV
- Nghe nhạc MP3/WAV/WMA/AAC
- Lịch tổ chức
- Ghi âm giọng nói
- Nhập liệu đoán trước từ', N'Li-Ion 1110 mAh (BL-4U)', NULL, NULL, 1, CAST(0x0000A1E2011A734C AS DateTime), CAST(0x0000A1E2011A734C AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (11, 16, N'Nokia S.40', N'GSM 850 / 900 / 1800 / 1900', N'HSDPA 850 / 900 / 1700 / 1900 / 2100', CAST(0x2B370B00 AS Date), CAST(0x3D370B00 AS Date), N'130.3 x 70.8 x 10.7 mm, 99 cc', 95, N'Màn hình cảm ứng điện trở TFT, 56K màu', N'240 x 400 pixels, 3.0 inches', N'- Cảm ứng đa điểm
- Mặt kính Corning Gorilla Glass
- Cảm biến gia tốc
- Cảm biến ánh sáng', N'Báo rung, nhạc chuông MP3, WAV', 1, N'Có nhiều, danh bạ hình ảnh', N'Có nhiều', N'140 MB, 256 MB ROM, 128 MB RAM', 1, 1, 0, N'HSDPA, 14.4 Mbps; HSUPA, 5.76 Mbps', 0, N'Wi-Fi 802.11 b/g/n', N'Có, v2.1 với EDR', 0, N'Có, microUSB v2.0, hỗ trợ USB On-the-go', N'3.15 MP, 2048x1536 pixels', N'Không', NULL, N'	Có, VGA@25fps', N'1 GHz', NULL, N'SMS, MMS, Email, IM', N'WAP 2.0/xHTML, HTML', 1, N'Có, có thể tải thêm', N'Tiếng Anh, Tiếng Việt', N'Không', N'Có, MIDP 2.1', N'- Tích hợp mạng xã hội
- Xem video MP4/H.264/H.263/WMV
- Nghe nhạc MP3/WAV/WMA/AAC
- Lịch tổ chức
- Ghi âm giọng nói
- Nhập liệu đoán trước từ', N'Li-Ion 1110 mAh (BL-4U)', NULL, NULL, 1, CAST(0x0000A1E2011AFA52 AS DateTime), CAST(0x0000A1E2011AFA52 AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (12, 17, N'Nokia S.40', N'GSM 850 / 900 / 1800 / 1900', N'HSDPA 850 / 900 / 1700 / 1900 / 2100', CAST(0x2B370B00 AS Date), CAST(0x3D370B00 AS Date), N'130.3 x 70.8 x 10.7 mm, 99 cc', 95, N'Màn hình cảm ứng điện trở TFT, 56K màu', N'240 x 400 pixels, 3.0 inches', N'- Cảm ứng đa điểm
- Mặt kính Corning Gorilla Glass
- Cảm biến gia tốc
- Cảm biến ánh sáng', N'Báo rung, nhạc chuông MP3, WAV', 1, N'Có nhiều, danh bạ hình ảnh', N'Có nhiều', N'140 MB, 256 MB ROM, 128 MB RAM', 1, 1, 0, N'HSDPA, 14.4 Mbps; HSUPA, 5.76 Mbps', 0, N'Wi-Fi 802.11 b/g/n', N'Có, v2.1 với EDR', 0, N'Có, microUSB v2.0, hỗ trợ USB On-the-go', N'3.15 MP, 2048x1536 pixels', N'Không', NULL, N'	Có, VGA@25fps', N'1 GHz', NULL, N'SMS, MMS, Email, IM', N'WAP 2.0/xHTML, HTML', 1, N'Có, có thể tải thêm', N'Tiếng Anh, Tiếng Việt', N'Không', N'Có, MIDP 2.1', N'- Tích hợp mạng xã hội
- Xem video MP4/H.264/H.263/WMV
- Nghe nhạc MP3/WAV/WMA/AAC
- Lịch tổ chức
- Ghi âm giọng nói
- Nhập liệu đoán trước từ', N'Li-Ion 1110 mAh (BL-4U)', NULL, NULL, 1, NULL, CAST(0x0000A1E2011B9D87 AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (13, 18, N'Nokia S.40', N'GSM 850 / 900 / 1800 / 1900', N'HSDPA 850 / 900 / 1700 / 1900 / 2100', CAST(0x2B370B00 AS Date), CAST(0x3D370B00 AS Date), N'130.3 x 70.8 x 10.7 mm, 99 cc', 95, N'Màn hình cảm ứng điện trở TFT, 56K màu', N'240 x 400 pixels, 3.0 inches', N'- Cảm ứng đa điểm
- Mặt kính Corning Gorilla Glass
- Cảm biến gia tốc
- Cảm biến ánh sáng', N'Báo rung, nhạc chuông MP3, WAV', 1, N'Có nhiều, danh bạ hình ảnh', N'Có nhiều', N'140 MB, 256 MB ROM, 128 MB RAM', 1, 1, 0, N'HSDPA, 14.4 Mbps; HSUPA, 5.76 Mbps', 0, N'Wi-Fi 802.11 b/g/n', N'Có, v2.1 với EDR', 0, N'Có, microUSB v2.0, hỗ trợ USB On-the-go', N'3.15 MP, 2048x1536 pixels', N'Không', NULL, N'	Có, VGA@25fps', N'1 GHz', NULL, N'SMS, MMS, Email, IM', N'WAP 2.0/xHTML, HTML', 1, N'Có, có thể tải thêm', N'Tiếng Anh, Tiếng Việt', N'Không', N'Có, MIDP 2.1', N'- Tích hợp mạng xã hội
- Xem video MP4/H.264/H.263/WMV
- Nghe nhạc MP3/WAV/WMA/AAC
- Lịch tổ chức
- Ghi âm giọng nói
- Nhập liệu đoán trước từ', N'Li-Ion 1110 mAh (BL-4U)', NULL, NULL, 1, CAST(0x0000A1E2011BCF35 AS DateTime), CAST(0x0000A1E2011BCF35 AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (14, 19, N'Nokia S.40', N'GSM 850 / 900 / 1800 / 1900', N'HSDPA 850 / 900 / 1700 / 1900 / 2100', CAST(0x2B370B00 AS Date), CAST(0x3D370B00 AS Date), N'130.3 x 70.8 x 10.7 mm, 99 cc', 95, N'Màn hình cảm ứng điện trở TFT, 56K màu', N'240 x 400 pixels, 3.0 inches', N'- Cảm ứng đa điểm
- Mặt kính Corning Gorilla Glass
- Cảm biến gia tốc
- Cảm biến ánh sáng', N'Báo rung, nhạc chuông MP3, WAV', 1, N'Có nhiều, danh bạ hình ảnh', N'Có nhiều', N'140 MB, 256 MB ROM, 128 MB RAM', 1, 1, 0, N'HSDPA, 14.4 Mbps; HSUPA, 5.76 Mbps', 0, N'Wi-Fi 802.11 b/g/n', N'Có, v2.1 với EDR', 0, N'Có, microUSB v2.0, hỗ trợ USB On-the-go', N'3.15 MP, 2048x1536 pixels', N'Không', NULL, N'	Có, VGA@25fps', N'1 GHz', NULL, N'SMS, MMS, Email, IM', N'WAP 2.0/xHTML, HTML', 1, N'Có, có thể tải thêm', N'Tiếng Anh, Tiếng Việt', N'Không', N'Có, MIDP 2.1', N'- Tích hợp mạng xã hội
- Xem video MP4/H.264/H.263/WMV
- Nghe nhạc MP3/WAV/WMA/AAC
- Lịch tổ chức
- Ghi âm giọng nói
- Nhập liệu đoán trước từ', N'Li-Ion 1110 mAh (BL-4U)', NULL, NULL, 1, CAST(0x0000A1E2011C8614 AS DateTime), CAST(0x0000A1E2011C8614 AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (15, 20, N'Nokia S.40', N'GSM 850 / 900 / 1800 / 1900', N'HSDPA 850 / 900 / 1700 / 1900 / 2100', CAST(0x2B370B00 AS Date), CAST(0x3D370B00 AS Date), N'130.3 x 70.8 x 10.7 mm, 99 cc', 95, N'Màn hình cảm ứng điện trở TFT, 56K màu', N'240 x 400 pixels, 3.0 inches', N'- Cảm ứng đa điểm
- Mặt kính Corning Gorilla Glass
- Cảm biến gia tốc
- Cảm biến ánh sáng', N'Báo rung, nhạc chuông MP3, WAV', 1, N'Có nhiều, danh bạ hình ảnh', N'Có nhiều', N'140 MB, 256 MB ROM, 128 MB RAM', 1, 1, 0, N'HSDPA, 14.4 Mbps; HSUPA, 5.76 Mbps', 0, N'Wi-Fi 802.11 b/g/n', N'Có, v2.1 với EDR', 0, N'Có, microUSB v2.0, hỗ trợ USB On-the-go', N'3.15 MP, 2048x1536 pixels', N'Không', NULL, N'	Có, VGA@25fps', N'1 GHz', NULL, N'SMS, MMS, Email, IM', N'WAP 2.0/xHTML, HTML', 1, N'Có, có thể tải thêm', N'Tiếng Anh, Tiếng Việt', N'Không', N'Có, MIDP 2.1', N'- Tích hợp mạng xã hội
- Xem video MP4/H.264/H.263/WMV
- Nghe nhạc MP3/WAV/WMA/AAC
- Lịch tổ chức
- Ghi âm giọng nói
- Nhập liệu đoán trước từ', N'Li-Ion 1110 mAh (BL-4U)', NULL, NULL, 1, CAST(0x0000A1E2011D2816 AS DateTime), CAST(0x0000A1E2011D2816 AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (16, 21, N'Nokia S.40', N'GSM 850 / 900 / 1800 / 1900', N'HSDPA 850 / 900 / 1700 / 1900 / 2100', CAST(0x2B370B00 AS Date), CAST(0x3D370B00 AS Date), N'130.3 x 70.8 x 10.7 mm, 99 cc', 95, N'Màn hình cảm ứng điện trở TFT, 56K màu', N'240 x 400 pixels, 3.0 inches', N'- Cảm ứng đa điểm
- Mặt kính Corning Gorilla Glass
- Cảm biến gia tốc
- Cảm biến ánh sáng', N'Báo rung, nhạc chuông MP3, WAV', 1, N'Có nhiều, danh bạ hình ảnh', N'Có nhiều', N'140 MB, 256 MB ROM, 128 MB RAM', 1, 1, 0, N'HSDPA, 14.4 Mbps; HSUPA, 5.76 Mbps', 0, N'Wi-Fi 802.11 b/g/n', N'Có, v2.1 với EDR', 0, N'Có, microUSB v2.0, hỗ trợ USB On-the-go', N'3.15 MP, 2048x1536 pixels', N'Không', NULL, N'	Có, VGA@25fps', N'1 GHz', NULL, N'SMS, MMS, Email, IM', N'WAP 2.0/xHTML, HTML', 1, N'Có, có thể tải thêm', N'Tiếng Anh, Tiếng Việt', N'Không', N'Có, MIDP 2.1', N'- Tích hợp mạng xã hội
- Xem video MP4/H.264/H.263/WMV
- Nghe nhạc MP3/WAV/WMA/AAC
- Lịch tổ chức
- Ghi âm giọng nói
- Nhập liệu đoán trước từ', N'Li-Ion 1110 mAh (BL-4U)', NULL, NULL, 1, CAST(0x0000A1E2011DA9F5 AS DateTime), CAST(0x0000A1E2011DA9F5 AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (17, 22, N'Nokia S.40', N'GSM 850 / 900 / 1800 / 1900', N'HSDPA 850 / 900 / 1700 / 1900 / 2100', CAST(0x2B370B00 AS Date), CAST(0x3D370B00 AS Date), N'130.3 x 70.8 x 10.7 mm, 99 cc', 95, N'Màn hình cảm ứng điện trở TFT, 56K màu', N'240 x 400 pixels, 3.0 inches', N'- Cảm ứng đa điểm
- Mặt kính Corning Gorilla Glass
- Cảm biến gia tốc
- Cảm biến ánh sáng', N'Báo rung, nhạc chuông MP3, WAV', 1, N'Có nhiều, danh bạ hình ảnh', N'Có nhiều', N'140 MB, 256 MB ROM, 128 MB RAM', 1, 1, 0, N'HSDPA, 14.4 Mbps; HSUPA, 5.76 Mbps', 0, N'Wi-Fi 802.11 b/g/n', N'Có, v2.1 với EDR', 0, N'Có, microUSB v2.0, hỗ trợ USB On-the-go', N'3.15 MP, 2048x1536 pixels', N'Không', NULL, N'	Có, VGA@25fps', N'1 GHz', NULL, N'SMS, MMS, Email, IM', N'WAP 2.0/xHTML, HTML', 1, N'Có, có thể tải thêm', N'Tiếng Anh, Tiếng Việt', N'Không', N'Có, MIDP 2.1', N'- Tích hợp mạng xã hội
- Xem video MP4/H.264/H.263/WMV
- Nghe nhạc MP3/WAV/WMA/AAC
- Lịch tổ chức
- Ghi âm giọng nói
- Nhập liệu đoán trước từ', N'Li-Ion 1110 mAh (BL-4U)', NULL, NULL, 1, CAST(0x0000A1E2011E7358 AS DateTime), CAST(0x0000A1E2011E7358 AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (18, 23, N'iOS 6', N'GSM 850 / 900 / 1800 / 1900 ', N'HSDPA 850 / 900 / 1900 / 2100', CAST(0x2F370B00 AS Date), CAST(0x2F370B00 AS Date), N'200 x 134.7 x 7.2 mm', 312, N'Màn hình cảm ứng điện dung LED-backlit IPS LCD, 16 triệu màu', N'768 x 1024 pixels, 7.9 inches', N'- Mặt kính phủ lớp chống thấm
- Cảm ứng đa điểm
- Cảm biến gia tốc
- Cảm biến con quay hồi chuyển
- Cảm biến la bàn số', N'Báo rung, nhạc chuông M4R', 1, N'Khả năng lưu các mục và fields không giới hạn, danh bạ hình ảnh', N'Không hỗ trợ', N'32 GB', 0, 1, 1, N'DC-HSDPA, 42 Mbps; HSDPA, 21 Mbps; HSUPA, 5.76 Mbps, LTE, 100 Mbps; Rev. A, up to 3.1 Mbps', 0, N'Wi-Fi 802.11 a/b/g/n, dual-band', N'Có, v4.0 với A2DP, EDR', 0, N'Có, v2.0, cổng Lightning', N'5 MP, 2592x1944 pixels, autofocus', N'Có, 1.2 MP, 720p@30fps, nhận diện khuôn mặt, videocalling trên mạng Wi-Fi hoặc 3G', N'Geo-tagging, chạm lấy nét, nhận diện khuôn mặt', N'Có, 1080p@30fps, chống rung', N'Dual-core 1 GHz Cortex-A9, PowerVR SGX543MP2 GPU, chipset Apple A5', NULL, N'iMessage, Email, Push Email, IM', N'HTML (Safari)', 0, N'Có, có thể tải thêm ', N'Tiếng Anh, Tiếng Việt', N'Có, hỗ trợ A-GPS và GLONASS', N'Không', N'- Chỉ sử dụng nano Sim
- Dịch vụ điện toán đám mây iCloud
- Tích hợp Twitter và Facebook
- Apple Maps
- Xem / Chỉnh sửa video
- Xem / Chỉnh sửa hình ảnh
- Ghi âm giọng nói
- TV-out
- Xem văn bản
- Nhập liệu đoán trước từ', N'Li-Po', NULL, NULL, 1, NULL, CAST(0x0000A1E2013C8CE7 AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (19, 24, N'iOS 6', N'GSM 850 / 900 / 1800 / 1900 ', N'HSDPA 850 / 900 / 1900 / 2100', CAST(0x2F370B00 AS Date), CAST(0x2F370B00 AS Date), N'200 x 134.7 x 7.2 mm', 312, N'Màn hình cảm ứng điện dung LED-backlit IPS LCD, 16 triệu màu', N'768 x 1024 pixels, 7.9 inches', N'- Mặt kính phủ lớp chống thấm
- Cảm ứng đa điểm
- Cảm biến gia tốc
- Cảm biến con quay hồi chuyển
- Cảm biến la bàn số', N'Báo rung, nhạc chuông M4R', 1, N'Khả năng lưu các mục và fields không giới hạn, danh bạ hình ảnh', N'Không hỗ trợ', N'32 GB', 0, 1, 1, N'DC-HSDPA, 42 Mbps; HSDPA, 21 Mbps; HSUPA, 5.76 Mbps, LTE, 100 Mbps; Rev. A, up to 3.1 Mbps', 0, N'Wi-Fi 802.11 a/b/g/n, dual-band', N'Có, v4.0 với A2DP, EDR', 0, N'Có, v2.0, cổng Lightning', N'5 MP, 2592x1944 pixels, autofocus', N'Có, 1.2 MP, 720p@30fps, nhận diện khuôn mặt, videocalling trên mạng Wi-Fi hoặc 3G', N'Geo-tagging, chạm lấy nét, nhận diện khuôn mặt', N'Có, 1080p@30fps, chống rung', N'Dual-core 1 GHz Cortex-A9, PowerVR SGX543MP2 GPU, chipset Apple A5', NULL, N'iMessage, Email, Push Email, IM', N'HTML (Safari)', 0, N'Có, có thể tải thêm ', N'Tiếng Anh, Tiếng Việt', N'Có, hỗ trợ A-GPS và GLONASS', N'Không', N'- Chỉ sử dụng nano Sim
- Dịch vụ điện toán đám mây iCloud
- Tích hợp Twitter và Facebook
- Apple Maps
- Xem / Chỉnh sửa video
- Xem / Chỉnh sửa hình ảnh
- Ghi âm giọng nói
- TV-out
- Xem văn bản
- Nhập liệu đoán trước từ', N'Li-Po', NULL, NULL, 1, NULL, CAST(0x0000A1E2013C6D53 AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (20, 25, N'iOS 6', N'GSM 850 / 900 / 1800 / 1900 ', N'HSDPA 850 / 900 / 1900 / 2100', CAST(0x2F370B00 AS Date), CAST(0x2F370B00 AS Date), N'200 x 134.7 x 7.2 mm', 312, N'Màn hình cảm ứng điện dung LED-backlit IPS LCD, 16 triệu màu', N'768 x 1024 pixels, 7.9 inches', N'- Mặt kính phủ lớp chống thấm
- Cảm ứng đa điểm
- Cảm biến gia tốc
- Cảm biến con quay hồi chuyển
- Cảm biến la bàn số', N'Báo rung, nhạc chuông M4R', 1, N'Khả năng lưu các mục và fields không giới hạn, danh bạ hình ảnh', N'Không hỗ trợ', N'32 GB', 0, 1, 1, N'DC-HSDPA, 42 Mbps; HSDPA, 21 Mbps; HSUPA, 5.76 Mbps, LTE, 100 Mbps; Rev. A, up to 3.1 Mbps', 0, N'Wi-Fi 802.11 a/b/g/n, dual-band', N'Có, v4.0 với A2DP, EDR', 0, N'Có, v2.0, cổng Lightning', N'5 MP, 2592x1944 pixels, autofocus', N'Có, 1.2 MP, 720p@30fps, nhận diện khuôn mặt, videocalling trên mạng Wi-Fi hoặc 3G', N'Geo-tagging, chạm lấy nét, nhận diện khuôn mặt', N'Có, 1080p@30fps, chống rung', N'Dual-core 1 GHz Cortex-A9, PowerVR SGX543MP2 GPU, chipset Apple A5', NULL, N'iMessage, Email, Push Email, IM', N'HTML (Safari)', 0, N'Có, có thể tải thêm ', N'Tiếng Anh, Tiếng Việt', N'Có, hỗ trợ A-GPS và GLONASS', N'Không', N'- Chỉ sử dụng nano Sim
- Dịch vụ điện toán đám mây iCloud
- Tích hợp Twitter và Facebook
- Apple Maps
- Xem / Chỉnh sửa video
- Xem / Chỉnh sửa hình ảnh
- Ghi âm giọng nói
- TV-out
- Xem văn bản
- Nhập liệu đoán trước từ', N'Li-Po', NULL, NULL, 1, NULL, CAST(0x0000A1E2013CAE1D AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (21, 26, N'iOS 6', N'GSM 850 / 900 / 1800 / 1900 ', N'HSDPA 850 / 900 / 1900 / 2100', CAST(0x2F370B00 AS Date), CAST(0x2F370B00 AS Date), N'200 x 134.7 x 7.2 mm', 312, N'Màn hình cảm ứng điện dung LED-backlit IPS LCD, 16 triệu màu', N'768 x 1024 pixels, 7.9 inches', N'- Mặt kính phủ lớp chống thấm
- Cảm ứng đa điểm
- Cảm biến gia tốc
- Cảm biến con quay hồi chuyển
- Cảm biến la bàn số', N'Báo rung, nhạc chuông M4R', 1, N'Khả năng lưu các mục và fields không giới hạn, danh bạ hình ảnh', N'Không hỗ trợ', N'32 GB', 0, 1, 1, N'DC-HSDPA, 42 Mbps; HSDPA, 21 Mbps; HSUPA, 5.76 Mbps, LTE, 100 Mbps; Rev. A, up to 3.1 Mbps', 0, N'Wi-Fi 802.11 a/b/g/n, dual-band', N'Có, v4.0 với A2DP, EDR', 0, N'Có, v2.0, cổng Lightning', N'5 MP, 2592x1944 pixels, autofocus', N'Có, 1.2 MP, 720p@30fps, nhận diện khuôn mặt, videocalling trên mạng Wi-Fi hoặc 3G', N'Geo-tagging, chạm lấy nét, nhận diện khuôn mặt', N'Có, 1080p@30fps, chống rung', N'Dual-core 1 GHz Cortex-A9, PowerVR SGX543MP2 GPU, chipset Apple A5', NULL, N'iMessage, Email, Push Email, IM', N'HTML (Safari)', 0, N'Có, có thể tải thêm ', N'Tiếng Anh, Tiếng Việt', N'Có, hỗ trợ A-GPS và GLONASS', N'Không', N'- Chỉ sử dụng nano Sim
- Dịch vụ điện toán đám mây iCloud
- Tích hợp Twitter và Facebook
- Apple Maps
- Xem / Chỉnh sửa video
- Xem / Chỉnh sửa hình ảnh
- Ghi âm giọng nói
- TV-out
- Xem văn bản
- Nhập liệu đoán trước từ', N'Li-Po', NULL, NULL, 1, CAST(0x0000A1E2013CF308 AS DateTime), CAST(0x0000A1E2013CF308 AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (22, 27, N'iOS 6', N'GSM 850 / 900 / 1800 / 1900 ', N'HSDPA 850 / 900 / 1900 / 2100', CAST(0x2F370B00 AS Date), CAST(0x2F370B00 AS Date), N'200 x 134.7 x 7.2 mm', 312, N'Màn hình cảm ứng điện dung LED-backlit IPS LCD, 16 triệu màu', N'768 x 1024 pixels, 7.9 inches', N'- Mặt kính phủ lớp chống thấm
- Cảm ứng đa điểm
- Cảm biến gia tốc
- Cảm biến con quay hồi chuyển
- Cảm biến la bàn số', N'Báo rung, nhạc chuông M4R', 1, N'Khả năng lưu các mục và fields không giới hạn, danh bạ hình ảnh', N'Không hỗ trợ', N'32 GB', 0, 1, 1, N'DC-HSDPA, 42 Mbps; HSDPA, 21 Mbps; HSUPA, 5.76 Mbps, LTE, 100 Mbps; Rev. A, up to 3.1 Mbps', 0, N'Wi-Fi 802.11 a/b/g/n, dual-band', N'Có, v4.0 với A2DP, EDR', 0, N'Có, v2.0, cổng Lightning', N'5 MP, 2592x1944 pixels, autofocus', N'Có, 1.2 MP, 720p@30fps, nhận diện khuôn mặt, videocalling trên mạng Wi-Fi hoặc 3G', N'Geo-tagging, chạm lấy nét, nhận diện khuôn mặt', N'Có, 1080p@30fps, chống rung', N'Dual-core 1 GHz Cortex-A9, PowerVR SGX543MP2 GPU, chipset Apple A5', NULL, N'iMessage, Email, Push Email, IM', N'HTML (Safari)', 0, N'Có, có thể tải thêm ', N'Tiếng Anh, Tiếng Việt', N'Có, hỗ trợ A-GPS và GLONASS', N'Không', N'- Chỉ sử dụng nano Sim
- Dịch vụ điện toán đám mây iCloud
- Tích hợp Twitter và Facebook
- Apple Maps
- Xem / Chỉnh sửa video
- Xem / Chỉnh sửa hình ảnh
- Ghi âm giọng nói
- TV-out
- Xem văn bản
- Nhập liệu đoán trước từ', N'Li-Po', NULL, NULL, 1, CAST(0x0000A1E2013D78E7 AS DateTime), CAST(0x0000A1E2013D78E7 AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (23, 28, N'iOS 6', N'GSM 850 / 900 / 1800 / 1900 ', N'HSDPA 850 / 900 / 1900 / 2100', CAST(0x2F370B00 AS Date), CAST(0x2F370B00 AS Date), N'200 x 134.7 x 7.2 mm', 312, N'Màn hình cảm ứng điện dung LED-backlit IPS LCD, 16 triệu màu', N'768 x 1024 pixels, 7.9 inches', N'- Mặt kính phủ lớp chống thấm
- Cảm ứng đa điểm
- Cảm biến gia tốc
- Cảm biến con quay hồi chuyển
- Cảm biến la bàn số', N'Báo rung, nhạc chuông M4R', 1, N'Khả năng lưu các mục và fields không giới hạn, danh bạ hình ảnh', N'Không hỗ trợ', N'32 GB', 0, 1, 1, N'DC-HSDPA, 42 Mbps; HSDPA, 21 Mbps; HSUPA, 5.76 Mbps, LTE, 100 Mbps; Rev. A, up to 3.1 Mbps', 0, N'Wi-Fi 802.11 a/b/g/n, dual-band', N'Có, v4.0 với A2DP, EDR', 0, N'Có, v2.0, cổng Lightning', N'5 MP, 2592x1944 pixels, autofocus', N'Có, 1.2 MP, 720p@30fps, nhận diện khuôn mặt, videocalling trên mạng Wi-Fi hoặc 3G', N'Geo-tagging, chạm lấy nét, nhận diện khuôn mặt', N'Có, 1080p@30fps, chống rung', N'Dual-core 1 GHz Cortex-A9, PowerVR SGX543MP2 GPU, chipset Apple A5', NULL, N'iMessage, Email, Push Email, IM', N'HTML (Safari)', 0, N'Có, có thể tải thêm ', N'Tiếng Anh, Tiếng Việt', N'Có, hỗ trợ A-GPS và GLONASS', N'Không', N'- Chỉ sử dụng nano Sim
- Dịch vụ điện toán đám mây iCloud
- Tích hợp Twitter và Facebook
- Apple Maps
- Xem / Chỉnh sửa video
- Xem / Chỉnh sửa hình ảnh
- Ghi âm giọng nói
- TV-out
- Xem văn bản
- Nhập liệu đoán trước từ', N'Li-Po', NULL, NULL, 1, NULL, CAST(0x0000A1E2013E21CD AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (24, 29, N'iOS 6', N'GSM 850 / 900 / 1800 / 1900 ', N'HSDPA 850 / 900 / 1900 / 2100', CAST(0x2F370B00 AS Date), CAST(0x2F370B00 AS Date), N'200 x 134.7 x 7.2 mm', 312, N'Màn hình cảm ứng điện dung LED-backlit IPS LCD, 16 triệu màu', N'768 x 1024 pixels, 7.9 inches', N'- Mặt kính phủ lớp chống thấm
- Cảm ứng đa điểm
- Cảm biến gia tốc
- Cảm biến con quay hồi chuyển
- Cảm biến la bàn số', N'Báo rung, nhạc chuông M4R', 1, N'Khả năng lưu các mục và fields không giới hạn, danh bạ hình ảnh', N'Không hỗ trợ', N'32 GB', 0, 1, 1, N'DC-HSDPA, 42 Mbps; HSDPA, 21 Mbps; HSUPA, 5.76 Mbps, LTE, 100 Mbps; Rev. A, up to 3.1 Mbps', 0, N'Wi-Fi 802.11 a/b/g/n, dual-band', N'Có, v4.0 với A2DP, EDR', 0, N'Có, v2.0, cổng Lightning', N'5 MP, 2592x1944 pixels, autofocus', N'Có, 1.2 MP, 720p@30fps, nhận diện khuôn mặt, videocalling trên mạng Wi-Fi hoặc 3G', N'Geo-tagging, chạm lấy nét, nhận diện khuôn mặt', N'Có, 1080p@30fps, chống rung', N'Dual-core 1 GHz Cortex-A9, PowerVR SGX543MP2 GPU, chipset Apple A5', NULL, N'iMessage, Email, Push Email, IM', N'HTML (Safari)', 0, N'Có, có thể tải thêm ', N'Tiếng Anh, Tiếng Việt', N'Có, hỗ trợ A-GPS và GLONASS', N'Không', N'- Chỉ sử dụng nano Sim
- Dịch vụ điện toán đám mây iCloud
- Tích hợp Twitter và Facebook
- Apple Maps
- Xem / Chỉnh sửa video
- Xem / Chỉnh sửa hình ảnh
- Ghi âm giọng nói
- TV-out
- Xem văn bản
- Nhập liệu đoán trước từ', N'Li-Po', NULL, NULL, 1, NULL, CAST(0x0000A1E2013EA356 AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (25, 30, N'iOS 6', N'GSM 850 / 900 / 1800 / 1900 ', N'HSDPA 850 / 900 / 1900 / 2100', CAST(0x2F370B00 AS Date), CAST(0x2F370B00 AS Date), N'200 x 134.7 x 7.2 mm', 312, N'Màn hình cảm ứng điện dung LED-backlit IPS LCD, 16 triệu màu', N'768 x 1024 pixels, 7.9 inches', N'- Mặt kính phủ lớp chống thấm
- Cảm ứng đa điểm
- Cảm biến gia tốc
- Cảm biến con quay hồi chuyển
- Cảm biến la bàn số', N'Báo rung, nhạc chuông M4R', 1, N'Khả năng lưu các mục và fields không giới hạn, danh bạ hình ảnh', N'Không hỗ trợ', N'32 GB', 0, 1, 1, N'DC-HSDPA, 42 Mbps; HSDPA, 21 Mbps; HSUPA, 5.76 Mbps, LTE, 100 Mbps; Rev. A, up to 3.1 Mbps', 0, N'Wi-Fi 802.11 a/b/g/n, dual-band', N'Có, v4.0 với A2DP, EDR', 0, N'Có, v2.0, cổng Lightning', N'5 MP, 2592x1944 pixels, autofocus', N'Có, 1.2 MP, 720p@30fps, nhận diện khuôn mặt, videocalling trên mạng Wi-Fi hoặc 3G', N'Geo-tagging, chạm lấy nét, nhận diện khuôn mặt', N'Có, 1080p@30fps, chống rung', N'Dual-core 1 GHz Cortex-A9, PowerVR SGX543MP2 GPU, chipset Apple A5', NULL, N'iMessage, Email, Push Email, IM', N'HTML (Safari)', 0, N'Có, có thể tải thêm ', N'Tiếng Anh, Tiếng Việt', N'Có, hỗ trợ A-GPS và GLONASS', N'Không', N'- Chỉ sử dụng nano Sim
- Dịch vụ điện toán đám mây iCloud
- Tích hợp Twitter và Facebook
- Apple Maps
- Xem / Chỉnh sửa video
- Xem / Chỉnh sửa hình ảnh
- Ghi âm giọng nói
- TV-out
- Xem văn bản
- Nhập liệu đoán trước từ', N'Li-Po', NULL, NULL, 1, CAST(0x0000A1E2013F48A0 AS DateTime), CAST(0x0000A1E2013F48A0 AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (26, 31, N'iOS 6', N'GSM 850 / 900 / 1800 / 1900 ', N'HSDPA 850 / 900 / 1900 / 2100', CAST(0x2F370B00 AS Date), CAST(0x2F370B00 AS Date), N'200 x 134.7 x 7.2 mm', 312, N'Màn hình cảm ứng điện dung LED-backlit IPS LCD, 16 triệu màu', N'768 x 1024 pixels, 7.9 inches', N'- Mặt kính phủ lớp chống thấm
- Cảm ứng đa điểm
- Cảm biến gia tốc
- Cảm biến con quay hồi chuyển
- Cảm biến la bàn số', N'Báo rung, nhạc chuông M4R', 1, N'Khả năng lưu các mục và fields không giới hạn, danh bạ hình ảnh', N'Không hỗ trợ', N'32 GB', 0, 1, 1, N'DC-HSDPA, 42 Mbps; HSDPA, 21 Mbps; HSUPA, 5.76 Mbps, LTE, 100 Mbps; Rev. A, up to 3.1 Mbps', 0, N'Wi-Fi 802.11 a/b/g/n, dual-band', N'Có, v4.0 với A2DP, EDR', 0, N'Có, v2.0, cổng Lightning', N'5 MP, 2592x1944 pixels, autofocus', N'Có, 1.2 MP, 720p@30fps, nhận diện khuôn mặt, videocalling trên mạng Wi-Fi hoặc 3G', N'Geo-tagging, chạm lấy nét, nhận diện khuôn mặt', N'Có, 1080p@30fps, chống rung', N'Dual-core 1 GHz Cortex-A9, PowerVR SGX543MP2 GPU, chipset Apple A5', NULL, N'iMessage, Email, Push Email, IM', N'HTML (Safari)', 0, N'Có, có thể tải thêm ', N'Tiếng Anh, Tiếng Việt', N'Có, hỗ trợ A-GPS và GLONASS', N'Không', N'- Chỉ sử dụng nano Sim
- Dịch vụ điện toán đám mây iCloud
- Tích hợp Twitter và Facebook
- Apple Maps
- Xem / Chỉnh sửa video
- Xem / Chỉnh sửa hình ảnh
- Ghi âm giọng nói
- TV-out
- Xem văn bản
- Nhập liệu đoán trước từ', N'Li-Po', NULL, NULL, 1, CAST(0x0000A1E2013FF05B AS DateTime), CAST(0x0000A1E2013FF05B AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (27, 32, N'iOS 6', N'GSM 850 / 900 / 1800 / 1900 ', N'HSDPA 850 / 900 / 1900 / 2100', CAST(0x2F370B00 AS Date), CAST(0x2F370B00 AS Date), N'200 x 134.7 x 7.2 mm', 312, N'Màn hình cảm ứng điện dung LED-backlit IPS LCD, 16 triệu màu', N'768 x 1024 pixels, 7.9 inches', N'- Mặt kính phủ lớp chống thấm
- Cảm ứng đa điểm
- Cảm biến gia tốc
- Cảm biến con quay hồi chuyển
- Cảm biến la bàn số', N'Báo rung, nhạc chuông M4R', 1, N'Khả năng lưu các mục và fields không giới hạn, danh bạ hình ảnh', N'Không hỗ trợ', N'32 GB', 0, 1, 1, N'DC-HSDPA, 42 Mbps; HSDPA, 21 Mbps; HSUPA, 5.76 Mbps, LTE, 100 Mbps; Rev. A, up to 3.1 Mbps', 0, N'Wi-Fi 802.11 a/b/g/n, dual-band', N'Có, v4.0 với A2DP, EDR', 0, N'Có, v2.0, cổng Lightning', N'5 MP, 2592x1944 pixels, autofocus', N'Có, 1.2 MP, 720p@30fps, nhận diện khuôn mặt, videocalling trên mạng Wi-Fi hoặc 3G', N'Geo-tagging, chạm lấy nét, nhận diện khuôn mặt', N'Có, 1080p@30fps, chống rung', N'Dual-core 1 GHz Cortex-A9, PowerVR SGX543MP2 GPU, chipset Apple A5', NULL, N'iMessage, Email, Push Email, IM', N'HTML (Safari)', 0, N'Có, có thể tải thêm ', N'Tiếng Anh, Tiếng Việt', N'Có, hỗ trợ A-GPS và GLONASS', N'Không', N'- Chỉ sử dụng nano Sim
- Dịch vụ điện toán đám mây iCloud
- Tích hợp Twitter và Facebook
- Apple Maps
- Xem / Chỉnh sửa video
- Xem / Chỉnh sửa hình ảnh
- Ghi âm giọng nói
- TV-out
- Xem văn bản
- Nhập liệu đoán trước từ', N'Li-Po', NULL, NULL, 1, CAST(0x0000A1E2014062B8 AS DateTime), CAST(0x0000A1E2014062B8 AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (28, 33, N'iOS 6', N'GSM 850 / 900 / 1800 / 1900 ', N'HSDPA 850 / 900 / 1900 / 2100', CAST(0x2F370B00 AS Date), CAST(0x2F370B00 AS Date), N'200 x 134.7 x 7.2 mm', 312, N'Màn hình cảm ứng điện dung LED-backlit IPS LCD, 16 triệu màu', N'768 x 1024 pixels, 7.9 inches', N'- Mặt kính phủ lớp chống thấm
- Cảm ứng đa điểm
- Cảm biến gia tốc
- Cảm biến con quay hồi chuyển
- Cảm biến la bàn số', N'Báo rung, nhạc chuông M4R', 1, N'Khả năng lưu các mục và fields không giới hạn, danh bạ hình ảnh', N'Không hỗ trợ', N'32 GB', 0, 1, 1, N'DC-HSDPA, 42 Mbps; HSDPA, 21 Mbps; HSUPA, 5.76 Mbps, LTE, 100 Mbps; Rev. A, up to 3.1 Mbps', 0, N'Wi-Fi 802.11 a/b/g/n, dual-band', N'Có, v4.0 với A2DP, EDR', 0, N'Có, v2.0, cổng Lightning', N'5 MP, 2592x1944 pixels, autofocus', N'Có, 1.2 MP, 720p@30fps, nhận diện khuôn mặt, videocalling trên mạng Wi-Fi hoặc 3G', N'Geo-tagging, chạm lấy nét, nhận diện khuôn mặt', N'Có, 1080p@30fps, chống rung', N'Dual-core 1 GHz Cortex-A9, PowerVR SGX543MP2 GPU, chipset Apple A5', NULL, N'iMessage, Email, Push Email, IM', N'HTML (Safari)', 0, N'Có, có thể tải thêm ', N'Tiếng Anh, Tiếng Việt', N'Có, hỗ trợ A-GPS và GLONASS', N'Không', N'- Chỉ sử dụng nano Sim
- Dịch vụ điện toán đám mây iCloud
- Tích hợp Twitter và Facebook
- Apple Maps
- Xem / Chỉnh sửa video
- Xem / Chỉnh sửa hình ảnh
- Ghi âm giọng nói
- TV-out
- Xem văn bản
- Nhập liệu đoán trước từ', N'Li-Po', NULL, NULL, 1, CAST(0x0000A1E20140A731 AS DateTime), CAST(0x0000A1E20140A731 AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (29, 34, N'iOS 6', N'GSM 850 / 900 / 1800 / 1900 ', N'HSDPA 850 / 900 / 1900 / 2100', CAST(0x2F370B00 AS Date), CAST(0x2F370B00 AS Date), N'200 x 134.7 x 7.2 mm', 312, N'Màn hình cảm ứng điện dung LED-backlit IPS LCD, 16 triệu màu', N'768 x 1024 pixels, 7.9 inches', N'- Mặt kính phủ lớp chống thấm
- Cảm ứng đa điểm
- Cảm biến gia tốc
- Cảm biến con quay hồi chuyển
- Cảm biến la bàn số', N'Báo rung, nhạc chuông M4R', 1, N'Khả năng lưu các mục và fields không giới hạn, danh bạ hình ảnh', N'Không hỗ trợ', N'32 GB', 0, 1, 1, N'DC-HSDPA, 42 Mbps; HSDPA, 21 Mbps; HSUPA, 5.76 Mbps, LTE, 100 Mbps; Rev. A, up to 3.1 Mbps', 0, N'Wi-Fi 802.11 a/b/g/n, dual-band', N'Có, v4.0 với A2DP, EDR', 0, N'Có, v2.0, cổng Lightning', N'5 MP, 2592x1944 pixels, autofocus', N'Có, 1.2 MP, 720p@30fps, nhận diện khuôn mặt, videocalling trên mạng Wi-Fi hoặc 3G', N'Geo-tagging, chạm lấy nét, nhận diện khuôn mặt', N'Có, 1080p@30fps, chống rung', N'Dual-core 1 GHz Cortex-A9, PowerVR SGX543MP2 GPU, chipset Apple A5', NULL, N'iMessage, Email, Push Email, IM', N'HTML (Safari)', 0, N'Có, có thể tải thêm ', N'Tiếng Anh, Tiếng Việt', N'Có, hỗ trợ A-GPS và GLONASS', N'Không', N'- Chỉ sử dụng nano Sim
- Dịch vụ điện toán đám mây iCloud
- Tích hợp Twitter và Facebook
- Apple Maps
- Xem / Chỉnh sửa video
- Xem / Chỉnh sửa hình ảnh
- Ghi âm giọng nói
- TV-out
- Xem văn bản
- Nhập liệu đoán trước từ', N'Li-Po', NULL, NULL, 1, CAST(0x0000A1E2014105FA AS DateTime), CAST(0x0000A1E2014105FA AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (30, 35, N'iOS 6', N'GSM 850 / 900 / 1800 / 1900 ', N'HSDPA 850 / 900 / 1900 / 2100', CAST(0x2F370B00 AS Date), CAST(0x2F370B00 AS Date), N'200 x 134.7 x 7.2 mm', 312, N'Màn hình cảm ứng điện dung LED-backlit IPS LCD, 16 triệu màu', N'768 x 1024 pixels, 7.9 inches', N'- Mặt kính phủ lớp chống thấm
- Cảm ứng đa điểm
- Cảm biến gia tốc
- Cảm biến con quay hồi chuyển
- Cảm biến la bàn số', N'Báo rung, nhạc chuông M4R', 1, N'Khả năng lưu các mục và fields không giới hạn, danh bạ hình ảnh', N'Không hỗ trợ', N'32 GB', 0, 1, 1, N'DC-HSDPA, 42 Mbps; HSDPA, 21 Mbps; HSUPA, 5.76 Mbps, LTE, 100 Mbps; Rev. A, up to 3.1 Mbps', 0, N'Wi-Fi 802.11 a/b/g/n, dual-band', N'Có, v4.0 với A2DP, EDR', 0, N'Có, v2.0, cổng Lightning', N'5 MP, 2592x1944 pixels, autofocus', N'Có, 1.2 MP, 720p@30fps, nhận diện khuôn mặt, videocalling trên mạng Wi-Fi hoặc 3G', N'Geo-tagging, chạm lấy nét, nhận diện khuôn mặt', N'Có, 1080p@30fps, chống rung', N'Dual-core 1 GHz Cortex-A9, PowerVR SGX543MP2 GPU, chipset Apple A5', NULL, N'iMessage, Email, Push Email, IM', N'HTML (Safari)', 0, N'Có, có thể tải thêm ', N'Tiếng Anh, Tiếng Việt', N'Có, hỗ trợ A-GPS và GLONASS', N'Không', N'- Chỉ sử dụng nano Sim
- Dịch vụ điện toán đám mây iCloud
- Tích hợp Twitter và Facebook
- Apple Maps
- Xem / Chỉnh sửa video
- Xem / Chỉnh sửa hình ảnh
- Ghi âm giọng nói
- TV-out
- Xem văn bản
- Nhập liệu đoán trước từ', N'Li-Po', NULL, NULL, 1, CAST(0x0000A1E201414042 AS DateTime), CAST(0x0000A1E201414042 AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (31, 36, N'iOS 6', N'GSM 850 / 900 / 1800 / 1900 ', N'HSDPA 850 / 900 / 1900 / 2100', CAST(0x2F370B00 AS Date), CAST(0x2F370B00 AS Date), N'200 x 134.7 x 7.2 mm', 312, N'Màn hình cảm ứng điện dung LED-backlit IPS LCD, 16 triệu màu', N'768 x 1024 pixels, 7.9 inches', N'- Mặt kính phủ lớp chống thấm
- Cảm ứng đa điểm
- Cảm biến gia tốc
- Cảm biến con quay hồi chuyển
- Cảm biến la bàn số', N'Báo rung, nhạc chuông M4R', 1, N'Khả năng lưu các mục và fields không giới hạn, danh bạ hình ảnh', N'Không hỗ trợ', N'32 GB', 0, 1, 1, N'DC-HSDPA, 42 Mbps; HSDPA, 21 Mbps; HSUPA, 5.76 Mbps, LTE, 100 Mbps; Rev. A, up to 3.1 Mbps', 0, N'Wi-Fi 802.11 a/b/g/n, dual-band', N'Có, v4.0 với A2DP, EDR', 0, N'Có, v2.0, cổng Lightning', N'5 MP, 2592x1944 pixels, autofocus', N'Có, 1.2 MP, 720p@30fps, nhận diện khuôn mặt, videocalling trên mạng Wi-Fi hoặc 3G', N'Geo-tagging, chạm lấy nét, nhận diện khuôn mặt', N'Có, 1080p@30fps, chống rung', N'Dual-core 1 GHz Cortex-A9, PowerVR SGX543MP2 GPU, chipset Apple A5', NULL, N'iMessage, Email, Push Email, IM', N'HTML (Safari)', 0, N'Có, có thể tải thêm ', N'Tiếng Anh, Tiếng Việt', N'Có, hỗ trợ A-GPS và GLONASS', N'Không', N'- Chỉ sử dụng nano Sim
- Dịch vụ điện toán đám mây iCloud
- Tích hợp Twitter và Facebook
- Apple Maps
- Xem / Chỉnh sửa video
- Xem / Chỉnh sửa hình ảnh
- Ghi âm giọng nói
- TV-out
- Xem văn bản
- Nhập liệu đoán trước từ', N'Li-Po', NULL, NULL, 1, CAST(0x0000A1E2014175FC AS DateTime), CAST(0x0000A1E2014175FC AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (32, 37, N'iOS 6', N'GSM 850 / 900 / 1800 / 1900 ', N'HSDPA 850 / 900 / 1900 / 2100', CAST(0x2F370B00 AS Date), CAST(0x2F370B00 AS Date), N'200 x 134.7 x 7.2 mm', 312, N'Màn hình cảm ứng điện dung LED-backlit IPS LCD, 16 triệu màu', N'768 x 1024 pixels, 7.9 inches', N'- Mặt kính phủ lớp chống thấm
- Cảm ứng đa điểm
- Cảm biến gia tốc
- Cảm biến con quay hồi chuyển
- Cảm biến la bàn số', N'Báo rung, nhạc chuông M4R', 1, N'Khả năng lưu các mục và fields không giới hạn, danh bạ hình ảnh', N'Không hỗ trợ', N'32 GB', 0, 1, 1, N'DC-HSDPA, 42 Mbps; HSDPA, 21 Mbps; HSUPA, 5.76 Mbps, LTE, 100 Mbps; Rev. A, up to 3.1 Mbps', 0, N'Wi-Fi 802.11 a/b/g/n, dual-band', N'Có, v4.0 với A2DP, EDR', 0, N'Có, v2.0, cổng Lightning', N'5 MP, 2592x1944 pixels, autofocus', N'Có, 1.2 MP, 720p@30fps, nhận diện khuôn mặt, videocalling trên mạng Wi-Fi hoặc 3G', N'Geo-tagging, chạm lấy nét, nhận diện khuôn mặt', N'Có, 1080p@30fps, chống rung', N'Dual-core 1 GHz Cortex-A9, PowerVR SGX543MP2 GPU, chipset Apple A5', NULL, N'iMessage, Email, Push Email, IM', N'HTML (Safari)', 0, N'Có, có thể tải thêm ', N'Tiếng Anh, Tiếng Việt', N'Có, hỗ trợ A-GPS và GLONASS', N'Không', N'- Chỉ sử dụng nano Sim
- Dịch vụ điện toán đám mây iCloud
- Tích hợp Twitter và Facebook
- Apple Maps
- Xem / Chỉnh sửa video
- Xem / Chỉnh sửa hình ảnh
- Ghi âm giọng nói
- TV-out
- Xem văn bản
- Nhập liệu đoán trước từ', N'Li-Po', NULL, NULL, 1, CAST(0x0000A1E20141C055 AS DateTime), CAST(0x0000A1E20141C055 AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (33, 38, N'iOS 6', N'GSM 850 / 900 / 1800 / 1900 ', N'HSDPA 850 / 900 / 1900 / 2100', CAST(0x2F370B00 AS Date), CAST(0x2F370B00 AS Date), N'200 x 134.7 x 7.2 mm', 312, N'Màn hình cảm ứng điện dung LED-backlit IPS LCD, 16 triệu màu', N'768 x 1024 pixels, 7.9 inches', N'- Mặt kính phủ lớp chống thấm
- Cảm ứng đa điểm
- Cảm biến gia tốc
- Cảm biến con quay hồi chuyển
- Cảm biến la bàn số', N'Báo rung, nhạc chuông M4R', 1, N'Khả năng lưu các mục và fields không giới hạn, danh bạ hình ảnh', N'Không hỗ trợ', N'32 GB', 0, 1, 1, N'DC-HSDPA, 42 Mbps; HSDPA, 21 Mbps; HSUPA, 5.76 Mbps, LTE, 100 Mbps; Rev. A, up to 3.1 Mbps', 0, N'Wi-Fi 802.11 a/b/g/n, dual-band', N'Có, v4.0 với A2DP, EDR', 0, N'Có, v2.0, cổng Lightning', N'5 MP, 2592x1944 pixels, autofocus', N'Có, 1.2 MP, 720p@30fps, nhận diện khuôn mặt, videocalling trên mạng Wi-Fi hoặc 3G', N'Geo-tagging, chạm lấy nét, nhận diện khuôn mặt', N'Có, 1080p@30fps, chống rung', N'Dual-core 1 GHz Cortex-A9, PowerVR SGX543MP2 GPU, chipset Apple A5', NULL, N'iMessage, Email, Push Email, IM', N'HTML (Safari)', 0, N'Có, có thể tải thêm ', N'Tiếng Anh, Tiếng Việt', N'Có, hỗ trợ A-GPS và GLONASS', N'Không', N'- Chỉ sử dụng nano Sim
- Dịch vụ điện toán đám mây iCloud
- Tích hợp Twitter và Facebook
- Apple Maps
- Xem / Chỉnh sửa video
- Xem / Chỉnh sửa hình ảnh
- Ghi âm giọng nói
- TV-out
- Xem văn bản
- Nhập liệu đoán trước từ', N'Li-Po', NULL, NULL, 1, CAST(0x0000A1E201421943 AS DateTime), CAST(0x0000A1E201421943 AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (34, 39, N'iOS 6', N'GSM 850 / 900 / 1800 / 1900 ', N'HSDPA 850 / 900 / 1900 / 2100', CAST(0x2F370B00 AS Date), CAST(0x2F370B00 AS Date), N'200 x 134.7 x 7.2 mm', 312, N'Màn hình cảm ứng điện dung LED-backlit IPS LCD, 16 triệu màu', N'768 x 1024 pixels, 7.9 inches', N'- Mặt kính phủ lớp chống thấm
- Cảm ứng đa điểm
- Cảm biến gia tốc
- Cảm biến con quay hồi chuyển
- Cảm biến la bàn số', N'Báo rung, nhạc chuông M4R', 1, N'Khả năng lưu các mục và fields không giới hạn, danh bạ hình ảnh', N'Không hỗ trợ', N'32 GB', 0, 1, 1, N'DC-HSDPA, 42 Mbps; HSDPA, 21 Mbps; HSUPA, 5.76 Mbps, LTE, 100 Mbps; Rev. A, up to 3.1 Mbps', 0, N'Wi-Fi 802.11 a/b/g/n, dual-band', N'Có, v4.0 với A2DP, EDR', 0, N'Có, v2.0, cổng Lightning', N'5 MP, 2592x1944 pixels, autofocus', N'Có, 1.2 MP, 720p@30fps, nhận diện khuôn mặt, videocalling trên mạng Wi-Fi hoặc 3G', N'Geo-tagging, chạm lấy nét, nhận diện khuôn mặt', N'Có, 1080p@30fps, chống rung', N'Dual-core 1 GHz Cortex-A9, PowerVR SGX543MP2 GPU, chipset Apple A5', NULL, N'iMessage, Email, Push Email, IM', N'HTML (Safari)', 0, N'Có, có thể tải thêm ', N'Tiếng Anh, Tiếng Việt', N'Có, hỗ trợ A-GPS và GLONASS', N'Không', N'- Chỉ sử dụng nano Sim
- Dịch vụ điện toán đám mây iCloud
- Tích hợp Twitter và Facebook
- Apple Maps
- Xem / Chỉnh sửa video
- Xem / Chỉnh sửa hình ảnh
- Ghi âm giọng nói
- TV-out
- Xem văn bản
- Nhập liệu đoán trước từ', N'Li-Po', NULL, NULL, 1, CAST(0x0000A1E201425F8B AS DateTime), CAST(0x0000A1E201425F8B AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (35, 40, N'iOS 6', N'GSM 850 / 900 / 1800 / 1900 ', N'HSDPA 850 / 900 / 1900 / 2100', CAST(0x2F370B00 AS Date), CAST(0x2F370B00 AS Date), N'200 x 134.7 x 7.2 mm', 312, N'Màn hình cảm ứng điện dung LED-backlit IPS LCD, 16 triệu màu', N'768 x 1024 pixels, 7.9 inches', N'- Mặt kính phủ lớp chống thấm
- Cảm ứng đa điểm
- Cảm biến gia tốc
- Cảm biến con quay hồi chuyển
- Cảm biến la bàn số', N'Báo rung, nhạc chuông M4R', 1, N'Khả năng lưu các mục và fields không giới hạn, danh bạ hình ảnh', N'Không hỗ trợ', N'32 GB', 0, 1, 1, N'DC-HSDPA, 42 Mbps; HSDPA, 21 Mbps; HSUPA, 5.76 Mbps, LTE, 100 Mbps; Rev. A, up to 3.1 Mbps', 0, N'Wi-Fi 802.11 a/b/g/n, dual-band', N'Có, v4.0 với A2DP, EDR', 0, N'Có, v2.0, cổng Lightning', N'5 MP, 2592x1944 pixels, autofocus', N'Có, 1.2 MP, 720p@30fps, nhận diện khuôn mặt, videocalling trên mạng Wi-Fi hoặc 3G', N'Geo-tagging, chạm lấy nét, nhận diện khuôn mặt', N'Có, 1080p@30fps, chống rung', N'Dual-core 1 GHz Cortex-A9, PowerVR SGX543MP2 GPU, chipset Apple A5', NULL, N'iMessage, Email, Push Email, IM', N'HTML (Safari)', 0, N'Có, có thể tải thêm ', N'Tiếng Anh, Tiếng Việt', N'Có, hỗ trợ A-GPS và GLONASS', N'Không', N'- Chỉ sử dụng nano Sim
- Dịch vụ điện toán đám mây iCloud
- Tích hợp Twitter và Facebook
- Apple Maps
- Xem / Chỉnh sửa video
- Xem / Chỉnh sửa hình ảnh
- Ghi âm giọng nói
- TV-out
- Xem văn bản
- Nhập liệu đoán trước từ', N'Li-Po', NULL, NULL, 1, CAST(0x0000A1E201429CE3 AS DateTime), CAST(0x0000A1E201429CE3 AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (36, 41, N'iOS 6', N'GSM 850 / 900 / 1800 / 1900 ', N'HSDPA 850 / 900 / 1900 / 2100', CAST(0x2F370B00 AS Date), CAST(0x2F370B00 AS Date), N'200 x 134.7 x 7.2 mm', 312, N'Màn hình cảm ứng điện dung LED-backlit IPS LCD, 16 triệu màu', N'768 x 1024 pixels, 7.9 inches', N'- Mặt kính phủ lớp chống thấm
- Cảm ứng đa điểm
- Cảm biến gia tốc
- Cảm biến con quay hồi chuyển
- Cảm biến la bàn số', N'Báo rung, nhạc chuông M4R', 1, N'Khả năng lưu các mục và fields không giới hạn, danh bạ hình ảnh', N'Không hỗ trợ', N'32 GB', 0, 1, 1, N'DC-HSDPA, 42 Mbps; HSDPA, 21 Mbps; HSUPA, 5.76 Mbps, LTE, 100 Mbps; Rev. A, up to 3.1 Mbps', 0, N'Wi-Fi 802.11 a/b/g/n, dual-band', N'Có, v4.0 với A2DP, EDR', 0, N'Có, v2.0, cổng Lightning', N'5 MP, 2592x1944 pixels, autofocus', N'Có, 1.2 MP, 720p@30fps, nhận diện khuôn mặt, videocalling trên mạng Wi-Fi hoặc 3G', N'Geo-tagging, chạm lấy nét, nhận diện khuôn mặt', N'Có, 1080p@30fps, chống rung', N'Dual-core 1 GHz Cortex-A9, PowerVR SGX543MP2 GPU, chipset Apple A5', NULL, N'iMessage, Email, Push Email, IM', N'HTML (Safari)', 0, N'Có, có thể tải thêm ', N'Tiếng Anh, Tiếng Việt', N'Có, hỗ trợ A-GPS và GLONASS', N'Không', N'- Chỉ sử dụng nano Sim
- Dịch vụ điện toán đám mây iCloud
- Tích hợp Twitter và Facebook
- Apple Maps
- Xem / Chỉnh sửa video
- Xem / Chỉnh sửa hình ảnh
- Ghi âm giọng nói
- TV-out
- Xem văn bản
- Nhập liệu đoán trước từ', N'Li-Po', NULL, NULL, 1, CAST(0x0000A1E20142E204 AS DateTime), CAST(0x0000A1E20142E204 AS DateTime))
INSERT [dbo].[ChiTietSanPham] ([MaChiTiet], [MaSP], [HDH], [Mang2G], [Mang3G], [NgayRaMat], [NgayCoHang], [KichThuocDT], [TrongLuong], [LoaiManHinh], [KichThuocManHinh], [MoTaKichThuocManHinh], [KieuChuong], [NgoAudio], [DanhBa], [CacSoDaGoi], [BoNhoTrong], [KheCamTheNho], [GPRS], [EDGE], [TocDo3G], [NFC], [WLAN], [Bluetooth], [HongNgoai], [USB], [CameraChinh], [CameraPhu], [DacDiem], [QuayPhim], [BoXuLy], [Chipset], [TinNhan], [TrinhDuyet], [Radio], [TroChoi], [NgonNgu], [GPS], [Java], [MoTaJava], [PinChuan], [ThoiGianCho], [ThoiGianDamThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (37, 42, N'iOS 6', N'GSM 850 / 900 / 1800 / 1900 ', N'HSDPA 850 / 900 / 1900 / 2100', CAST(0x2F370B00 AS Date), CAST(0x2F370B00 AS Date), N'200 x 134.7 x 7.2 mm', 312, N'Màn hình cảm ứng điện dung LED-backlit IPS LCD, 16 triệu màu', N'768 x 1024 pixels, 7.9 inches', N'- Mặt kính phủ lớp chống thấm
- Cảm ứng đa điểm
- Cảm biến gia tốc
- Cảm biến con quay hồi chuyển
- Cảm biến la bàn số', N'Báo rung, nhạc chuông M4R', 1, N'Khả năng lưu các mục và fields không giới hạn, danh bạ hình ảnh', N'Không hỗ trợ', N'32 GB', 0, 1, 1, N'DC-HSDPA, 42 Mbps; HSDPA, 21 Mbps; HSUPA, 5.76 Mbps, LTE, 100 Mbps; Rev. A, up to 3.1 Mbps', 0, N'Wi-Fi 802.11 a/b/g/n, dual-band', N'Có, v4.0 với A2DP, EDR', 0, N'Có, v2.0, cổng Lightning', N'5 MP, 2592x1944 pixels, autofocus', N'Có, 1.2 MP, 720p@30fps, nhận diện khuôn mặt, videocalling trên mạng Wi-Fi hoặc 3G', N'Geo-tagging, chạm lấy nét, nhận diện khuôn mặt', N'Có, 1080p@30fps, chống rung', N'Dual-core 1 GHz Cortex-A9, PowerVR SGX543MP2 GPU, chipset Apple A5', NULL, N'iMessage, Email, Push Email, IM', N'HTML (Safari)', 0, N'Có, có thể tải thêm ', N'Tiếng Anh, Tiếng Việt', N'Có, hỗ trợ A-GPS và GLONASS', N'Không', N'- Chỉ sử dụng nano Sim
- Dịch vụ điện toán đám mây iCloud
- Tích hợp Twitter và Facebook
- Apple Maps
- Xem / Chỉnh sửa video
- Xem / Chỉnh sửa hình ảnh
- Ghi âm giọng nói
- TV-out
- Xem văn bản
- Nhập liệu đoán trước từ', N'Li-Po', NULL, NULL, 1, CAST(0x0000A1E201430B1C AS DateTime), CAST(0x0000A1E201430B1C AS DateTime))
SET IDENTITY_INSERT [dbo].[ChiTietSanPham] OFF
SET IDENTITY_INSERT [dbo].[HinhAnh] ON 

INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (1, 6, N'images/home/mobiles/327115822_nokia-lumia-920-.jpg', 1, CAST(0x0000A1E2010B5B60 AS DateTime), CAST(0x0000A1E2010B5B60 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (2, 6, N'images/home/mobiles/6036663363_nokia-lumia-920-.jpg', 1, CAST(0x0000A1E2010B668B AS DateTime), CAST(0x0000A1E2010B668B AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (3, 6, N'images/home/mobiles/6722260146_nokia-lumia-920-.jpg', 1, CAST(0x0000A1E2010B6DBC AS DateTime), CAST(0x0000A1E2010B6DBC AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (4, 7, N'images/home/mobiles/5e5add10f1711ae09ee98a7ff45acbd91320183328.jpg', 1, CAST(0x0000A1E2010EC2F5 AS DateTime), CAST(0x0000A1E2010EC2F5 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (5, 7, N'images/home/mobiles/33104b586bb4d11868ed468cc37a5c391320183328.jpg', 1, CAST(0x0000A1E2010ECFD9 AS DateTime), CAST(0x0000A1E2010ECFD9 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (6, 8, N'images/home/mobiles/828415265_nokia-asha-311-.jpg', 1, CAST(0x0000A1E201153233 AS DateTime), CAST(0x0000A1E201153233 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (7, 9, N'images/home/mobiles/5e5add10f1711ae09ee98a7ff45acbd91320183328.jpg', 1, CAST(0x0000A1E201161CD6 AS DateTime), CAST(0x0000A1E201161CD6 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (8, 11, N'images/home/mobiles/5e5add10f1711ae09ee98a7ff45acbd91320183328.jpg', 1, CAST(0x0000A1E20116B95F AS DateTime), CAST(0x0000A1E20116B95F AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (9, 11, N'images/home/mobiles/3251180849_blackberry-p9981-porsche-design-.jpg', 1, CAST(0x0000A1E20116C333 AS DateTime), CAST(0x0000A1E20116C333 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (10, 11, N'images/home/mobiles/8075651559_blackberry-p9981-porsche-design-.jpg', 1, CAST(0x0000A1E20116FD3B AS DateTime), CAST(0x0000A1E20116FD3B AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (11, 12, N'images/home/mobiles/3928153212_blackberry-q10-.jpg', 1, CAST(0x0000A1E20117878C AS DateTime), CAST(0x0000A1E20117878C AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (12, 12, N'images/home/mobiles/5670267590_blackberry-q10-.jpg', 1, CAST(0x0000A1E201179BE9 AS DateTime), CAST(0x0000A1E201179BE9 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (13, 12, N'images/home/mobiles/5963938087_blackberry-q10-.jpg', 1, CAST(0x0000A1E20117BDB1 AS DateTime), CAST(0x0000A1E20117BDB1 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (14, 13, N'images/home/mobiles/3928153212_blackberry-q10-.jpg', 1, CAST(0x0000A1E201183353 AS DateTime), CAST(0x0000A1E201183353 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (15, 13, N'images/home/mobiles/5670267590_blackberry-q10-.jpg', 1, CAST(0x0000A1E201183E83 AS DateTime), CAST(0x0000A1E201183E83 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (16, 13, N'images/home/mobiles/5963938087_blackberry-q10-.jpg', 1, CAST(0x0000A1E20118716E AS DateTime), CAST(0x0000A1E20118716E AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (17, 14, N'images/home/mobiles/224617039_blackberry-z10-.jpg', 1, CAST(0x0000A1E2011A0A6B AS DateTime), CAST(0x0000A1E2011A0A6B AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (18, 14, N'images/home/mobiles/6041310914_blackberry-z10-.jpg', 1, CAST(0x0000A1E2011A149C AS DateTime), CAST(0x0000A1E2011A149C AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (19, 14, N'images/home/mobiles/7166842330_blackberry-z10-.jpg', 1, CAST(0x0000A1E2011A3A28 AS DateTime), CAST(0x0000A1E2011A3A28 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (20, 15, N'images/home/mobiles/5682536d4eb6c40fce6094a29a3dcc2d1314182814.jpg', 1, CAST(0x0000A1E2011ABEE5 AS DateTime), CAST(0x0000A1E2011ABEE5 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (21, 15, N'images/home/mobiles/ebb9f988fa7338d5eaa5f2611db227231314182813.jpg', 1, CAST(0x0000A1E2011AC7F2 AS DateTime), CAST(0x0000A1E2011AC7F2 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (22, 16, N'images/home/mobiles/8641988309_blackberry-bold-9790--cty-.jpg', 1, CAST(0x0000A1E2011B1EC0 AS DateTime), CAST(0x0000A1E2011B1EC0 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (23, 18, N'images/home/mobiles/48594619_sony-galaxy-note-ii-n7100-.jpg', 1, CAST(0x0000A1E2011C35AD AS DateTime), CAST(0x0000A1E2011C35AD AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (24, 18, N'images/home/mobiles/4679314238_sony-galaxy-note-ii-n7100-.jpg', 1, CAST(0x0000A1E2011C412D AS DateTime), CAST(0x0000A1E2011C412D AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (25, 18, N'images/home/mobiles/5367080113_sony-galaxy-note-ii-n7100-.jpg', 1, CAST(0x0000A1E2011C5072 AS DateTime), CAST(0x0000A1E2011C5072 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (26, 19, N'images/home/mobiles/246761481_samsung-galaxy-s-iii-i9300-.jpg', 1, CAST(0x0000A1E2011CBD59 AS DateTime), CAST(0x0000A1E2011CBD59 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (27, 19, N'images/home/mobiles/297980101_samsung-galaxy-s-iii-i9300-.jpg', 1, CAST(0x0000A1E2011CC83E AS DateTime), CAST(0x0000A1E2011CC83E AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (28, 19, N'images/home/mobiles/375733320_samsung-i9300-galaxy-s-iii-16gb--cty-.jpg', 1, CAST(0x0000A1E2011CD122 AS DateTime), CAST(0x0000A1E2011CD122 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (29, 20, N'images/home/mobiles/2241601431_lg-e960-nexus-4-.jpg', 1, CAST(0x0000A1E2011D5471 AS DateTime), CAST(0x0000A1E2011D5471 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (30, 20, N'images/home/mobiles/8386554248_lg-e960-nexus-4-.jpg', 1, CAST(0x0000A1E2011D607A AS DateTime), CAST(0x0000A1E2011D607A AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (31, 21, N'images/home/mobiles/3818347752_lg-p768-optimus-l9-.jpg', 1, CAST(0x0000A1E2011DF4AB AS DateTime), CAST(0x0000A1E2011DF4AB AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (32, 21, N'images/home/mobiles/7897612318_lg-p768-optimus-l9-.jpg', 1, CAST(0x0000A1E2011E0827 AS DateTime), CAST(0x0000A1E2011E0827 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (33, 22, N'images/home/mobiles/14030825_lg-e612-optimus-l5-.jpg', 1, CAST(0x0000A1E2011EAFB1 AS DateTime), CAST(0x0000A1E2011EAFB1 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (34, 22, N'images/home/mobiles/693684769_lg-e612-optimus-l5-.jpg', 1, CAST(0x0000A1E2011EB7BE AS DateTime), CAST(0x0000A1E2011EB7BE AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (35, 22, N'images/home/mobiles/824827861_lg-e612-optimus-l5-.jpg', 1, CAST(0x0000A1E2011EC16D AS DateTime), CAST(0x0000A1E2011EC16D AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (36, 23, N'images/home/mobiles/82787661_apple-ipad-mini-wi-fi-+-cellular.jpg', 1, CAST(0x0000A1E2013A88E9 AS DateTime), CAST(0x0000A1E2013A88E9 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (37, 23, N'images/home/mobiles/4629301135_apple-ipad-mini-wi-fi-+-cellular.jpg', 1, CAST(0x0000A1E2013A92FF AS DateTime), CAST(0x0000A1E2013A92FF AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (38, 24, N'images/home/mobiles/82787661_apple-ipad-mini-wi-fi-+-cellular.jpg', 1, CAST(0x0000A1E2013B9F78 AS DateTime), CAST(0x0000A1E2013B9F78 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (39, 24, N'images/home/mobiles/4629301135_apple-ipad-mini-wi-fi-+-cellular.jpg', 1, CAST(0x0000A1E2013BABD2 AS DateTime), CAST(0x0000A1E2013BABD2 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (40, 25, N'images/home/mobiles/82787661_apple-ipad-mini-wi-fi-+-cellular.jpg', 1, CAST(0x0000A1E2013C3B09 AS DateTime), CAST(0x0000A1E2013C3B09 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (41, 25, N'images/home/mobiles/4629301135_apple-ipad-mini-wi-fi-+-cellular.jpg', 1, CAST(0x0000A1E2013C4274 AS DateTime), CAST(0x0000A1E2013C4274 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (42, 26, N'images/home/mobiles/7417412903_samsung-galaxy-note-8-0-n5100-.jpg', 1, CAST(0x0000A1E2013D371E AS DateTime), CAST(0x0000A1E2013D371E AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (43, 26, N'images/home/mobiles/8585552783_samsung-galaxy-note-8-0-n5100-.jpg', 1, CAST(0x0000A1E2013D4047 AS DateTime), CAST(0x0000A1E2013D4047 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (44, 27, N'images/home/mobiles/261812226_samsung-galaxy-tab-2-7-0-p3100-.jpg', 1, CAST(0x0000A1E2013DA3DE AS DateTime), CAST(0x0000A1E2013DA3DE AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (45, 27, N'images/home/mobiles/847476582_samsung-galaxy-tab-2-7-0-p3100-.jpg', 1, CAST(0x0000A1E2013DAC37 AS DateTime), CAST(0x0000A1E2013DAC37 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (46, 28, N'images/home/mobiles/599648228_asus-google-nexus-7-.jpg', 1, CAST(0x0000A1E2013E1833 AS DateTime), CAST(0x0000A1E2013E1833 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (47, 28, N'images/home/mobiles/761301166_asus-google-nexus-7-.jpg', 1, CAST(0x0000A1E2013E2C77 AS DateTime), CAST(0x0000A1E2013E2C77 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (48, 29, N'images/home/mobiles/1713295942_asus-memo-pad-me172v-wifi-8gb.jpg', 1, CAST(0x0000A1E2013EAC31 AS DateTime), CAST(0x0000A1E2013EAC31 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (49, 29, N'images/home/mobiles/3182167467_asus-memo-pad-me172v-wifi-8gb.jpg', 1, CAST(0x0000A1E2013EB429 AS DateTime), CAST(0x0000A1E2013EB429 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (50, 29, N'images/home/mobiles/5526731536_asus-memo-pad-me172v-wifi-8gb.jpg', 1, CAST(0x0000A1E2013EBE2A AS DateTime), CAST(0x0000A1E2013EBE2A AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (51, 29, N'images/home/mobiles/6711880416_asus-memo-pad-me172v-wifi-8gb.jpg', 1, CAST(0x0000A1E2013EC6A7 AS DateTime), CAST(0x0000A1E2013EC6A7 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (52, 29, N'images/home/mobiles/8130484545_asus-memo-pad-me172v-8gb.jpg', 1, CAST(0x0000A1E2013ED009 AS DateTime), CAST(0x0000A1E2013ED009 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (53, 29, N'images/home/mobiles/8133041789_asus-memo-pad-me172v-wifi-8gb.jpg', 1, CAST(0x0000A1E2013EDB14 AS DateTime), CAST(0x0000A1E2013EDB14 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (54, 30, N'images/home/mobiles/4861480296_sony-xperia-z.jpg', 1, CAST(0x0000A1E2013F8011 AS DateTime), CAST(0x0000A1E2013F8011 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (55, 30, N'images/home/mobiles/1094666081_sony-xperia-z.jpg', 1, CAST(0x0000A1E2013F8FC3 AS DateTime), CAST(0x0000A1E2013F8FC3 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (56, 30, N'images/home/mobiles/5360340048_sony-xperia-z.jpg', 1, CAST(0x0000A1E2013F98F0 AS DateTime), CAST(0x0000A1E2013F98F0 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (57, 30, N'images/home/mobiles/5730818766_sony-xperia-z.jpg', 1, CAST(0x0000A1E2013FA0E1 AS DateTime), CAST(0x0000A1E2013FA0E1 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (58, 30, N'images/home/mobiles/6525305975_sony-xperia-z.jpg', 1, CAST(0x0000A1E2013FAC47 AS DateTime), CAST(0x0000A1E2013FAC47 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (59, 31, N'images/home/mobiles/2887459886_philips-w732--2-sim-.jpg', 1, CAST(0x0000A1E20140204A AS DateTime), CAST(0x0000A1E20140204A AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (60, 31, N'images/home/mobiles/7236084063_philips-w732--2-sim-.jpg', 1, CAST(0x0000A1E201402702 AS DateTime), CAST(0x0000A1E201402702 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (61, 31, N'images/home/mobiles/7236182429_philips-w626--2-sim-.jpg', 1, CAST(0x0000A1E201402F21 AS DateTime), CAST(0x0000A1E201402F21 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (62, 32, N'images/home/mobiles/202724765_motorola-ex119-.jpg', 1, CAST(0x0000A1E201407E99 AS DateTime), CAST(0x0000A1E201407E99 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (63, 33, N'images/home/mobiles/902717635_nokia-301-dual-sim-2-sim-2-song-.jpg', 1, CAST(0x0000A1E20140CDFE AS DateTime), CAST(0x0000A1E20140CDFE AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (64, 33, N'images/home/mobiles/971723618_nokia-301-dual-sim-2-sim-2-song-.jpg', 1, CAST(0x0000A1E20140D67E AS DateTime), CAST(0x0000A1E20140D67E AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (65, 33, N'images/home/mobiles/8992661066_nokia-301-dual-sim-2-sim-2-song-.jpg', 1, CAST(0x0000A1E20140DED5 AS DateTime), CAST(0x0000A1E20140DED5 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (66, 34, N'images/home/mobiles/8861421477_nokia-asha-308---2-sim--.jpg', 1, CAST(0x0000A1E201411FC9 AS DateTime), CAST(0x0000A1E201411FC9 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (67, 35, N'images/home/mobiles/408454407_nokia-x2-02---2-sim--.jpg', 1, CAST(0x0000A1E20141590F AS DateTime), CAST(0x0000A1E20141590F AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (68, 36, N'images/home/mobiles/26a88a8254dea444199b781dcd431e691314352400.jpg', 1, CAST(0x0000A1E201419DCB AS DateTime), CAST(0x0000A1E201419DCB AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (69, 37, N'images/home/mobiles/88ad20569c6852f61ad3d4207cfbb1221276766294.jpg', 1, CAST(0x0000A1E20141DC3A AS DateTime), CAST(0x0000A1E20141DC3A AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (70, 38, N'images/home/mobiles/5242911200_nokia-asha-205-.jpg', 1, CAST(0x0000A1E201422ABD AS DateTime), CAST(0x0000A1E201422ABD AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (71, 39, N'images/home/mobiles/26a88a8254dea444199b781dcd431e691314352400.jpg', 1, CAST(0x0000A1E2014270BF AS DateTime), CAST(0x0000A1E2014270BF AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (72, 40, N'images/home/mobiles/373777279_nokia-asha-306-.jpg', 1, CAST(0x0000A1E20142B192 AS DateTime), CAST(0x0000A1E20142B192 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (73, 41, N'images/home/mobiles/184d83d75ffd0d10de7c1bbcbd21f2cc1320182179.jpg', 1, CAST(0x0000A1E20142EF24 AS DateTime), CAST(0x0000A1E20142EF24 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (74, 42, N'images/home/mobiles/bb0b3a24d5626c5b63aec5d383c4b9121265202658.jpg', 1, CAST(0x0000A1E2014325E1 AS DateTime), CAST(0x0000A1E2014325E1 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (75, 42, N'images/home/mobiles/bd38184ee56f9f286f9a4e649a740e421252984134.jpg', 1, CAST(0x0000A1E201432CD5 AS DateTime), CAST(0x0000A1E201432CD5 AS DateTime))
INSERT [dbo].[HinhAnh] ([MaHinhAnh], [MaSP], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (76, 42, N'images/home/mobiles/f17a33840b3e520949d36b24ebad7fb01252984134.jpg', 1, CAST(0x0000A1E2014333D3 AS DateTime), CAST(0x0000A1E2014333D3 AS DateTime))
SET IDENTITY_INSERT [dbo].[HinhAnh] OFF
SET IDENTITY_INSERT [dbo].[HoaDon] ON 

INSERT [dbo].[HoaDon] ([MaHD], [MaKH], [TenKHDat], [DienThoai], [DiaChiGiaoHang], [ThoiGianDatHang], [HinhThucGiaoHang], [HinhThucThanhToan], [TongTien], [TinhTrang], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (1, 1, N'Trần Thanh Phúc', N'0122876858', N'27 Trần Phú, Q.5, TP.HCM', CAST(0x0000A1E201454C05 AS DateTime), 1, 2, 3900, 4, 1, CAST(0x0000A1E201454C05 AS DateTime), CAST(0x0000A1E201454C05 AS DateTime))
INSERT [dbo].[HoaDon] ([MaHD], [MaKH], [TenKHDat], [DienThoai], [DiaChiGiaoHang], [ThoiGianDatHang], [HinhThucGiaoHang], [HinhThucThanhToan], [TongTien], [TinhTrang], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (2, 1, N'Trần Thanh Phúc', N'0122876858', N'27 Trần Phú, Q.5, TP.HCM', CAST(0x0000A1E20145B1B4 AS DateTime), 1, 2, 3540, 4, 1, CAST(0x0000A1E20145B1B4 AS DateTime), CAST(0x0000A1E20145B1B4 AS DateTime))
INSERT [dbo].[HoaDon] ([MaHD], [MaKH], [TenKHDat], [DienThoai], [DiaChiGiaoHang], [ThoiGianDatHang], [HinhThucGiaoHang], [HinhThucThanhToan], [TongTien], [TinhTrang], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (3, 2, N'Nguyễn Trần Minh Loan', N'0122876858', N'27 Trần Phú, Q.5, TP.HCM', CAST(0x0000A1E201480CDC AS DateTime), 1, 1, 1190, 3, 1, CAST(0x0000A1E201480CDC AS DateTime), CAST(0x0000A1E201480CDC AS DateTime))
INSERT [dbo].[HoaDon] ([MaHD], [MaKH], [TenKHDat], [DienThoai], [DiaChiGiaoHang], [ThoiGianDatHang], [HinhThucGiaoHang], [HinhThucThanhToan], [TongTien], [TinhTrang], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (4, 7, N'Phan Hoàng Mỹ Dung', N'0122876855', N'27 Trần Phú, Q.5, TP.HCM', CAST(0x0000A1E2014AFABB AS DateTime), 1, 1, 2460, 2, 1, CAST(0x0000A1E2014AFABB AS DateTime), CAST(0x0000A1E2014AFABB AS DateTime))
INSERT [dbo].[HoaDon] ([MaHD], [MaKH], [TenKHDat], [DienThoai], [DiaChiGiaoHang], [ThoiGianDatHang], [HinhThucGiaoHang], [HinhThucThanhToan], [TongTien], [TinhTrang], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (5, 7, N'Phan Hoàng Mỹ Dung', N'0122876855', N'27 Trần Phú, Q.5, TP.HCM', CAST(0x0000A1E2014B5DD7 AS DateTime), 1, 2, 6690, 1, 1, CAST(0x0000A1E2014B5DD7 AS DateTime), CAST(0x0000A1E2014B5DD7 AS DateTime))
INSERT [dbo].[HoaDon] ([MaHD], [MaKH], [TenKHDat], [DienThoai], [DiaChiGiaoHang], [ThoiGianDatHang], [HinhThucGiaoHang], [HinhThucThanhToan], [TongTien], [TinhTrang], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (6, 7, N'Phan Hoàng Mỹ Dung', N'0122876855', N'27 Trần Phú, Q.5, TP.HCM', CAST(0x0000A1E2014BA299 AS DateTime), 1, 2, 35200, 5, 1, CAST(0x0000A1E2014BA299 AS DateTime), CAST(0x0000A1E2014BA299 AS DateTime))
INSERT [dbo].[HoaDon] ([MaHD], [MaKH], [TenKHDat], [DienThoai], [DiaChiGiaoHang], [ThoiGianDatHang], [HinhThucGiaoHang], [HinhThucThanhToan], [TongTien], [TinhTrang], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (7, 7, N'Phan Hoàng Mỹ Dung', N'0122876855', N'27 Trần Phú, Q.5, TP.HCM', CAST(0x0000A1E2015EF5C5 AS DateTime), 1, 2, 27030, 4, 1, CAST(0x0000A1E2015EF5C5 AS DateTime), CAST(0x0000A1E2015EF5C5 AS DateTime))
INSERT [dbo].[HoaDon] ([MaHD], [MaKH], [TenKHDat], [DienThoai], [DiaChiGiaoHang], [ThoiGianDatHang], [HinhThucGiaoHang], [HinhThucThanhToan], [TongTien], [TinhTrang], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (8, 7, N'Phan Hoàng Mỹ Dung', N'0122876855', N'27 Trần Phú, Q.10, TP.HCM', CAST(0x0000A1E60030150E AS DateTime), 1, 2, 20919, 3, 1, CAST(0x0000A1E60030150E AS DateTime), CAST(0x0000A1E60030150E AS DateTime))
INSERT [dbo].[HoaDon] ([MaHD], [MaKH], [TenKHDat], [DienThoai], [DiaChiGiaoHang], [ThoiGianDatHang], [HinhThucGiaoHang], [HinhThucThanhToan], [TongTien], [TinhTrang], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (9, 2, N'Nguyễn Trần Minh Loan', N'0122876858', N'27 Trần Phú, Q.5, TP.HCM', CAST(0x0000A1E60030F88D AS DateTime), 1, 1, 25395, 1, 1, CAST(0x0000A1E60030F88D AS DateTime), CAST(0x0000A1E60030F88D AS DateTime))
INSERT [dbo].[HoaDon] ([MaHD], [MaKH], [TenKHDat], [DienThoai], [DiaChiGiaoHang], [ThoiGianDatHang], [HinhThucGiaoHang], [HinhThucThanhToan], [TongTien], [TinhTrang], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (10, 7, N'Phan Hoàng Mỹ Dung', N'0122876855', N'27 Trần Phú, Q.10, TP.HCM', CAST(0x0000A1E600CB2379 AS DateTime), 1, 2, 9890, 1, 1, CAST(0x0000A1E600CB2379 AS DateTime), CAST(0x0000A1E600CB2379 AS DateTime))
INSERT [dbo].[HoaDon] ([MaHD], [MaKH], [TenKHDat], [DienThoai], [DiaChiGiaoHang], [ThoiGianDatHang], [HinhThucGiaoHang], [HinhThucThanhToan], [TongTien], [TinhTrang], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (11, 13, N'Nguyễn Thanh Bùi', N'01223360352', N'243 Lê Quang Định', CAST(0x0000A1E600E2FEBE AS DateTime), 1, 1, 1270, 2, 1, CAST(0x0000A1E600E2FEBE AS DateTime), CAST(0x0000A1E600E2FEBE AS DateTime))
SET IDENTITY_INSERT [dbo].[HoaDon] OFF
SET IDENTITY_INSERT [dbo].[KhachHang] ON 

INSERT [dbo].[KhachHang] ([MaKH], [UserId], [TenKH], [DiaChi], [Email], [DienThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (1, 7, N'Trần Thanh Phúc', N'27 Trần Phú, Q.5, TP.HCM', N'tranthanhphuc@gmail.com', N'0122876858', 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime))
INSERT [dbo].[KhachHang] ([MaKH], [UserId], [TenKH], [DiaChi], [Email], [DienThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (2, 8, N'Nguyễn Trần Minh Loan', N'27 Trần Phú, Q.5, TP.HCM', N'nguyentranminhloan@gmail.com', N'0122876858', 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime))
INSERT [dbo].[KhachHang] ([MaKH], [UserId], [TenKH], [DiaChi], [Email], [DienThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (3, 9, N'Trần Thiện Phú', N'27 Trần Phú, Q.5, TP.HCM', N'tranthienphu@gmail.com', N'0122876851', 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime))
INSERT [dbo].[KhachHang] ([MaKH], [UserId], [TenKH], [DiaChi], [Email], [DienThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (4, 10, N'Nguyễn Ngọc Minh Thư', N'27 Trần Phú, Q.5, TP.HCM', N'minhthu@gmail.com', N'0122876852', 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime))
INSERT [dbo].[KhachHang] ([MaKH], [UserId], [TenKH], [DiaChi], [Email], [DienThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (5, 11, N'Châu Ngọc Kim Yến', N'27 Trần Phú, Q.5, TP.HCM', N'kimyen@gmail.com', N'0122876853', 2, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1D40104E45D AS DateTime))
INSERT [dbo].[KhachHang] ([MaKH], [UserId], [TenKH], [DiaChi], [Email], [DienThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (6, 12, N'Nguyễn Thị Ngọc Giao', N'27 Trần Phú, Q.5, TP.HCM', N'ngocgiao@gmail.com', N'0122876854', 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime))
INSERT [dbo].[KhachHang] ([MaKH], [UserId], [TenKH], [DiaChi], [Email], [DienThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (7, 13, N'Phan Hoàng Mỹ Dung', N'27 Trần Phú, Q.10, TP.HCM', N'mydung@gmail.com', N'0122876855', 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime))
INSERT [dbo].[KhachHang] ([MaKH], [UserId], [TenKH], [DiaChi], [Email], [DienThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (8, 14, N'Lê Phước Thịnh', N'27 Trần Phú, Quận 5, TP.HCM', N'phuocthinh@gmail.com', N'0122876856', 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1DC01133B9C AS DateTime))
INSERT [dbo].[KhachHang] ([MaKH], [UserId], [TenKH], [DiaChi], [Email], [DienThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (9, 16, N'Nguyễn Ngọc Minh Thư', N'243 Lê Quang Định', N'anhminh06.ashura.vuong@gmail.com', N'01223360352', 2, CAST(0x0000A1E2015F94B2 AS DateTime), CAST(0x0000A1E2015F94B2 AS DateTime))
INSERT [dbo].[KhachHang] ([MaKH], [UserId], [TenKH], [DiaChi], [Email], [DienThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (10, 17, N'Tạ Đình Phong', N'132 Nguyễn Thị Minh Khai', N'nguyenngocanhminh06@gmail.com', N'01223360352', 2, CAST(0x0000A1E60005A192 AS DateTime), CAST(0x0000A1E60005A192 AS DateTime))
INSERT [dbo].[KhachHang] ([MaKH], [UserId], [TenKH], [DiaChi], [Email], [DienThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (11, 18, N'Tạ Đình Phong', N'132 Nguyễn Thị Minh Khai', N'nguyenngocanhminh06@gmail.com', N'01223360352', 2, CAST(0x0000A1E60006056A AS DateTime), CAST(0x0000A1E60006056A AS DateTime))
INSERT [dbo].[KhachHang] ([MaKH], [UserId], [TenKH], [DiaChi], [Email], [DienThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (12, 19, N'Tạ Đình Phong', N'132 Nguyễn Thị Minh Khai', N'nguyenngocanhminh06@gmail.com', N'01223360352', 1, CAST(0x0000A1E60006C3BA AS DateTime), CAST(0x0000A1E60006C3BA AS DateTime))
INSERT [dbo].[KhachHang] ([MaKH], [UserId], [TenKH], [DiaChi], [Email], [DienThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (13, 21, N'Nguyễn Thanh Bùi', N'243 Lê Quang Định', N'nguyenngocanhminh06@gmail.com', N'01223360352', 1, CAST(0x0000A1E600E1C954 AS DateTime), CAST(0x0000A1E600E1C954 AS DateTime))
SET IDENTITY_INSERT [dbo].[KhachHang] OFF
SET IDENTITY_INSERT [dbo].[KhuyenMai] ON 

INSERT [dbo].[KhuyenMai] ([MaKhuyenMai], [MaSP], [QuaTang], [GiamGia], [NgayBD], [NgayKT], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (2, 6, 1, 5, CAST(0x3C370B00 AS Date), CAST(0x5B370B00 AS Date), 1, CAST(0x0000A1E200000000 AS DateTime), CAST(0x0000A1E200000000 AS DateTime))
INSERT [dbo].[KhuyenMai] ([MaKhuyenMai], [MaSP], [QuaTang], [GiamGia], [NgayBD], [NgayKT], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (3, 7, 1, 3, CAST(0x3C370B00 AS Date), CAST(0x5B370B00 AS Date), 1, CAST(0x0000A1E200000000 AS DateTime), CAST(0x0000A1E200000000 AS DateTime))
INSERT [dbo].[KhuyenMai] ([MaKhuyenMai], [MaSP], [QuaTang], [GiamGia], [NgayBD], [NgayKT], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (4, 8, 1, 3, CAST(0x3C370B00 AS Date), CAST(0x5B370B00 AS Date), 1, CAST(0x0000A1E200000000 AS DateTime), CAST(0x0000A1E200000000 AS DateTime))
INSERT [dbo].[KhuyenMai] ([MaKhuyenMai], [MaSP], [QuaTang], [GiamGia], [NgayBD], [NgayKT], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (5, 9, 1, 5, CAST(0x3C370B00 AS Date), CAST(0x5B370B00 AS Date), 1, CAST(0x0000A1E200000000 AS DateTime), CAST(0x0000A1E200000000 AS DateTime))
INSERT [dbo].[KhuyenMai] ([MaKhuyenMai], [MaSP], [QuaTang], [GiamGia], [NgayBD], [NgayKT], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (6, 10, 1, 5, CAST(0x3C370B00 AS Date), CAST(0x5B370B00 AS Date), 1, CAST(0x0000A1E200000000 AS DateTime), CAST(0x0000A1E200000000 AS DateTime))
INSERT [dbo].[KhuyenMai] ([MaKhuyenMai], [MaSP], [QuaTang], [GiamGia], [NgayBD], [NgayKT], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (7, 11, 1, 10, CAST(0x3C370B00 AS Date), CAST(0x5B370B00 AS Date), 1, CAST(0x0000A1E200000000 AS DateTime), CAST(0x0000A1E200000000 AS DateTime))
INSERT [dbo].[KhuyenMai] ([MaKhuyenMai], [MaSP], [QuaTang], [GiamGia], [NgayBD], [NgayKT], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (8, 12, 1, 3, CAST(0x3C370B00 AS Date), CAST(0x5B370B00 AS Date), 1, CAST(0x0000A1E200000000 AS DateTime), CAST(0x0000A1E200000000 AS DateTime))
INSERT [dbo].[KhuyenMai] ([MaKhuyenMai], [MaSP], [QuaTang], [GiamGia], [NgayBD], [NgayKT], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (10, 13, 2, 5, CAST(0x3C370B00 AS Date), CAST(0x5B370B00 AS Date), 1, CAST(0x0000A1E200000000 AS DateTime), CAST(0x0000A1E200000000 AS DateTime))
INSERT [dbo].[KhuyenMai] ([MaKhuyenMai], [MaSP], [QuaTang], [GiamGia], [NgayBD], [NgayKT], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (11, 14, 2, 7, CAST(0x3C370B00 AS Date), CAST(0x5B370B00 AS Date), 1, CAST(0x0000A1E200000000 AS DateTime), CAST(0x0000A1E200000000 AS DateTime))
INSERT [dbo].[KhuyenMai] ([MaKhuyenMai], [MaSP], [QuaTang], [GiamGia], [NgayBD], [NgayKT], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (12, 15, 1, 5, CAST(0x3C370B00 AS Date), CAST(0x5B370B00 AS Date), 1, CAST(0x0000A1E200000000 AS DateTime), CAST(0x0000A1E200000000 AS DateTime))
INSERT [dbo].[KhuyenMai] ([MaKhuyenMai], [MaSP], [QuaTang], [GiamGia], [NgayBD], [NgayKT], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (13, 16, 2, 7, CAST(0x3C370B00 AS Date), CAST(0x5B370B00 AS Date), 1, CAST(0x0000A1E200000000 AS DateTime), CAST(0x0000A1E200000000 AS DateTime))
INSERT [dbo].[KhuyenMai] ([MaKhuyenMai], [MaSP], [QuaTang], [GiamGia], [NgayBD], [NgayKT], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (14, 17, 2, 7, CAST(0x3C370B00 AS Date), CAST(0x5B370B00 AS Date), NULL, CAST(0x0000A1E200000000 AS DateTime), CAST(0x0000A1E200000000 AS DateTime))
SET IDENTITY_INSERT [dbo].[KhuyenMai] OFF
SET IDENTITY_INSERT [dbo].[LoaiSanPham] ON 

INSERT [dbo].[LoaiSanPham] ([MaLoaiSP], [TenLoaiSP], [TrangThai], [NgayTao], [NgayCapNhat], [LinkName]) VALUES (1, N'Điện thoại', 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime), N'Mobile')
INSERT [dbo].[LoaiSanPham] ([MaLoaiSP], [TenLoaiSP], [TrangThai], [NgayTao], [NgayCapNhat], [LinkName]) VALUES (2, N'Máy tính bảng', 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime), N'iPad')
SET IDENTITY_INSERT [dbo].[LoaiSanPham] OFF
SET IDENTITY_INSERT [dbo].[MauSac] ON 

INSERT [dbo].[MauSac] ([MaMauSac], [TenMauSac], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (1, N'Trắng', 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime))
INSERT [dbo].[MauSac] ([MaMauSac], [TenMauSac], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (2, N'Đen', 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime))
INSERT [dbo].[MauSac] ([MaMauSac], [TenMauSac], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (3, N'Bạc', 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime))
INSERT [dbo].[MauSac] ([MaMauSac], [TenMauSac], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (4, N'Đỏ', 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime))
SET IDENTITY_INSERT [dbo].[MauSac] OFF
SET IDENTITY_INSERT [dbo].[NhanVien] ON 

INSERT [dbo].[NhanVien] ([MaNV], [UserId], [TenNV], [ChucVu], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (1, 3, N'Nguyễn Thị Bích Ngọc', N'Admin', 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1DC00D1B0D3 AS DateTime))
INSERT [dbo].[NhanVien] ([MaNV], [UserId], [TenNV], [ChucVu], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (2, 2, N'Nguyễn Ngọc Ánh Minh', N'Admin', 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime))
INSERT [dbo].[NhanVien] ([MaNV], [UserId], [TenNV], [ChucVu], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (3, 5, N'Lê Mỹ Vân', N'Quản lý', 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime))
INSERT [dbo].[NhanVien] ([MaNV], [UserId], [TenNV], [ChucVu], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (4, 6, N'Nguyễn Thái Châu', N'Nhân viên', 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1DC0102D4CD AS DateTime))
INSERT [dbo].[NhanVien] ([MaNV], [UserId], [TenNV], [ChucVu], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (5, 15, N'Ngô Thiều Kiều Chinh', N'Admin', 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A04B00000000 AS DateTime))
INSERT [dbo].[NhanVien] ([MaNV], [UserId], [TenNV], [ChucVu], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (6, 20, N'Nguyễn Thanh Mi', N'Admin', 1, CAST(0x0000A1E600C6AF87 AS DateTime), CAST(0x0000A1E600C6AF87 AS DateTime))
SET IDENTITY_INSERT [dbo].[NhanVien] OFF
SET IDENTITY_INSERT [dbo].[NhaSanXuat] ON 

INSERT [dbo].[NhaSanXuat] ([MaNSX], [TenNSX], [LoaiSP], [DiaChi], [DienThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (1, N'Apple', 1, NULL, NULL, 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime))
INSERT [dbo].[NhaSanXuat] ([MaNSX], [TenNSX], [LoaiSP], [DiaChi], [DienThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (2, N'SamSung', 1, NULL, NULL, 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime))
INSERT [dbo].[NhaSanXuat] ([MaNSX], [TenNSX], [LoaiSP], [DiaChi], [DienThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (3, N'Nokia', 1, NULL, NULL, 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime))
INSERT [dbo].[NhaSanXuat] ([MaNSX], [TenNSX], [LoaiSP], [DiaChi], [DienThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (4, N'Blackberry', 1, NULL, NULL, 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime))
INSERT [dbo].[NhaSanXuat] ([MaNSX], [TenNSX], [LoaiSP], [DiaChi], [DienThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (5, N'Sony', 1, NULL, NULL, 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime))
INSERT [dbo].[NhaSanXuat] ([MaNSX], [TenNSX], [LoaiSP], [DiaChi], [DienThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (6, N'Philips', 1, NULL, NULL, 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime))
INSERT [dbo].[NhaSanXuat] ([MaNSX], [TenNSX], [LoaiSP], [DiaChi], [DienThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (7, N'LG', 1, NULL, NULL, 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime))
INSERT [dbo].[NhaSanXuat] ([MaNSX], [TenNSX], [LoaiSP], [DiaChi], [DienThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (8, N'Motorola', 1, NULL, NULL, 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime))
INSERT [dbo].[NhaSanXuat] ([MaNSX], [TenNSX], [LoaiSP], [DiaChi], [DienThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (9, N'Mobell', 1, NULL, NULL, 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime))
INSERT [dbo].[NhaSanXuat] ([MaNSX], [TenNSX], [LoaiSP], [DiaChi], [DienThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (10, N'Lenovo', 1, NULL, NULL, 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime))
INSERT [dbo].[NhaSanXuat] ([MaNSX], [TenNSX], [LoaiSP], [DiaChi], [DienThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (11, N'HTC', 1, NULL, NULL, 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime))
INSERT [dbo].[NhaSanXuat] ([MaNSX], [TenNSX], [LoaiSP], [DiaChi], [DienThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (12, N'Apple', 2, NULL, NULL, 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A04B00000000 AS DateTime))
INSERT [dbo].[NhaSanXuat] ([MaNSX], [TenNSX], [LoaiSP], [DiaChi], [DienThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (13, N'Samsung', 2, NULL, NULL, 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A04B00000000 AS DateTime))
INSERT [dbo].[NhaSanXuat] ([MaNSX], [TenNSX], [LoaiSP], [DiaChi], [DienThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (14, N'Sony', 2, NULL, NULL, 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A04B00000000 AS DateTime))
INSERT [dbo].[NhaSanXuat] ([MaNSX], [TenNSX], [LoaiSP], [DiaChi], [DienThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (15, N'Asus', 2, NULL, NULL, 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A04B00000000 AS DateTime))
INSERT [dbo].[NhaSanXuat] ([MaNSX], [TenNSX], [LoaiSP], [DiaChi], [DienThoai], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (16, N'Fpt', 2, NULL, NULL, 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A04B00000000 AS DateTime))
SET IDENTITY_INSERT [dbo].[NhaSanXuat] OFF
SET IDENTITY_INSERT [dbo].[QuaTang] ON 

INSERT [dbo].[QuaTang] ([MaQuaTang], [TenQuaTang], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (1, N'Miếng dán màn hình', 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime))
INSERT [dbo].[QuaTang] ([MaQuaTang], [TenQuaTang], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (2, N'Áo', 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime))
SET IDENTITY_INSERT [dbo].[QuaTang] OFF
SET IDENTITY_INSERT [dbo].[SanPham] ON 

INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (6, N'NOKIA Lumia 920 32Gb', 1, 3, 50, 10190, 1, 12, 1, N'1 máy, 1 đầu sạc, tai nghe, 1 cable USB, sách hướng dẫn, cây lấy sim', 1, CAST(0x0000A1E2010A3AF2 AS DateTime), CAST(0x0000A1E2010A3AF2 AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (7, N'NOKIA Lumia 710 8Gb', 1, 3, 50, 3990, 1, 12, 0, N'1 máy, 1 pin, 1 sạc nhanh, tai nghe, cable, PBH, sách hướng dẫn', 1, CAST(0x0000A1E2010EBA02 AS DateTime), CAST(0x0000A1E2010EBA02 AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (8, N'NOKIA Asha 311', 1, 3, 50, 2460, 1, 12, 1, N'1 máy, 1 pin, 1 sạc nhanh, tai nghe, sách hướng dẫn', 1, CAST(0x0000A1E201141D55 AS DateTime), CAST(0x0000A1E201141D55 AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (9, N'NOKIA Asha 300', 1, 3, 50, 2200, 0, 12, 1, N'1 máy, 1 pin, 1 sạc nhanh, tai nghe, sách hướng dẫn', 1, NULL, CAST(0x0000A1E201159858 AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (10, N'NOKIA Asha 309', 1, 3, 50, 1890, 1, 12, 0, N'1 máy, 1 pin, 1 sạc nhanh, tai nghe, sách hướng dẫn', 1, CAST(0x0000A1E20115E442 AS DateTime), CAST(0x0000A1E20115E442 AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (11, N'BLACKBERRY P''9981 Porsche Design', 1, 4, 50, 42500, 1, 12, 0, N'1 máy, 1 pin, 1 sạc nhanh, tai nghe, sách hướng dẫn', 1, CAST(0x0000A1E201168A88 AS DateTime), CAST(0x0000A1E201168A88 AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (12, N'BLACKBERRY Q10 16Gb', 1, 4, 50, 16500, 1, 12, 0, N'1 máy, 1 pin, 1 sạc nhanh, tai nghe, sách hướng dẫn', 1, CAST(0x0000A1E201172F7B AS DateTime), CAST(0x0000A1E201172F7B AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (13, N'BLACKBERRY Q10', 1, 4, 50, 14070, 1, 12, 0, N'1 máy, 1 pin, 1 sạc nhanh, tai nghe, sách hướng dẫn', 1, CAST(0x0000A1E20117FF1E AS DateTime), CAST(0x0000A1E20117FF1E AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (14, N'BLACKBERRY Z10 16Gb', 1, 4, 50, 11860, 1, 12, 0, N'1 máy, 1 pin, 1 sạc nhanh, tai nghe, sách hướng dẫn', 1, CAST(0x0000A1E20118C02D AS DateTime), CAST(0x0000A1E20118C02D AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (15, N'BLACKBERRY Bold Touch 9900', 1, 4, 50, 9090, 1, 12, 0, N'1 máy, 1 pin, 1 sạc nhanh, tai nghe, sách hướng dẫn', 1, CAST(0x0000A1E2011A734B AS DateTime), CAST(0x0000A1E2011A734B AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (16, N'BLACKBERRY Bold 9790', 1, 4, 50, 6390, 1, 12, 0, N'1 máy, 1 pin, 1 sạc nhanh, tai nghe, sách hướng dẫn', 1, CAST(0x0000A1E2011AFA52 AS DateTime), CAST(0x0000A1E2011AFA52 AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (17, N'SAMSUNG i9500 Galaxy S4 16Gb', 1, 2, 50, 15890, 0, 12, 0, N'1 máy, 1 pin, 1 sạc nhanh, tai nghe, sách hướng dẫn', 1, NULL, CAST(0x0000A1E2011B9D86 AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (18, N'SAMSUNG N7100 Galaxy Note II 16Gb', 1, 2, 50, 12790, 1, 12, 0, N'1 máy, 1 pin, 1 sạc nhanh, tai nghe, sách hướng dẫn', 1, CAST(0x0000A1E2011BCF34 AS DateTime), CAST(0x0000A1E2011BCF34 AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (19, N'SAMSUNG i9300 Galaxy S III 16Gb', 1, 2, 50, 9890, 1, 12, 1, N'1 máy, 1 pin, 1 sạc nhanh, tai nghe, sách hướng dẫn', 1, CAST(0x0000A1E2011C8614 AS DateTime), CAST(0x0000A1E2011C8614 AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (20, N'LG E960 Nexus 4 16Gb', 1, 7, 50, 10590, 1, 12, 0, N'1 máy, 1 pin, 1 sạc nhanh, tai nghe, sách hướng dẫn', 1, CAST(0x0000A1E2011D2815 AS DateTime), CAST(0x0000A1E2011D2815 AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (21, N'LG P768 Optimus L9', 1, 7, 50, 6190, 1, 12, 0, N'1 máy, 1 pin, 1 sạc nhanh, tai nghe, sách hướng dẫn', 1, CAST(0x0000A1E2011DA9F5 AS DateTime), CAST(0x0000A1E2011DA9F5 AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (22, N'LG E612 Optimus L5', 1, 7, 50, 3220, 1, 12, 0, N'1 máy, 1 pin, 1 sạc nhanh, tai nghe, sách hướng dẫn', 1, CAST(0x0000A1E2011E7358 AS DateTime), CAST(0x0000A1E2011E7358 AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (23, N'APPLE iPad mini Wi-Fi + Cellular 32Gb', 2, 12, 20, 12440, 0, 12, 0, N'iPad mini, Dock Connector to USB Cable, USB Power Adapter, Documentation', 1, NULL, CAST(0x0000A1E2013C8CE7 AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (24, N'APPLE iPad mini Wi-Fi + Cellular 16Gb', 2, 12, 20, 10480, 0, 12, 0, N'iPad mini, Dock Connector to USB Cable, USB Power Adapter, Documentation', 1, NULL, CAST(0x0000A1E2013C6D51 AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (25, N'APPLE iPad mini Wi-Fi 16Gb', 2, 12, 20, 8090, 0, 12, 0, N'iPad mini, Dock Connector to USB Cable, USB Power Adapter, Documentation', 1, NULL, CAST(0x0000A1E2013CAE1C AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (26, N'SAMSUNG N5100 Galaxy Note 8.0', 2, 13, 20, 11490, 1, 12, 0, N'iPad mini, Dock Connector to USB Cable, USB Power Adapter, Documentation', 1, CAST(0x0000A1E2013CF307 AS DateTime), CAST(0x0000A1E2013CF307 AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (27, N'SAMSUNG P3100 Galaxy Tab 2 7.0 3G', 2, 13, 20, 7250, 1, 12, 1, N'iPad mini, Dock Connector to USB Cable, USB Power Adapter, Documentation', 1, CAST(0x0000A1E2013D78E7 AS DateTime), CAST(0x0000A1E2013D78E7 AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (28, N'ASUS Google Nexus 7 Wifi + 3G 32gb', 2, 15, 20, 7490, 0, 12, 0, N'iPad mini, Dock Connector to USB Cable, USB Power Adapter, Documentation', 1, NULL, CAST(0x0000A1E2013E21CD AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (29, N'ASUS MeMo Pad ME172V Wifi 8Gb', 2, 15, 20, 3290, 0, 12, 0, N'iPad mini, Dock Connector to USB Cable, USB Power Adapter, Documentation', 1, NULL, CAST(0x0000A1E2013EA356 AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (30, N'SONY Xperia Z 16Gb (C6602)', 1, 5, 20, 14890, 1, 12, 0, N'iPad mini, Dock Connector to USB Cable, USB Power Adapter, Documentation', 1, CAST(0x0000A1E2013F48A0 AS DateTime), CAST(0x0000A1E2013F48A0 AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (31, N'PHILIPS W732 (2 Sim)', 1, 6, 20, 5420, 1, 12, 1, N'iPad mini, Dock Connector to USB Cable, USB Power Adapter, Documentation', 1, CAST(0x0000A1E2013FF05B AS DateTime), CAST(0x0000A1E2013FF05B AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (32, N'MOTOROLA EX119 (2 sim)', 1, 8, 50, 1400, 1, 12, 0, N'iPad mini, Dock Connector to USB Cable, USB Power Adapter, Documentation', 1, CAST(0x0000A1E2014062B8 AS DateTime), CAST(0x0000A1E2014062B8 AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (33, N'NOKIA 301 (2sim)', 1, 3, 50, 1820, 1, 12, 1, N'iPad mini, Dock Connector to USB Cable, USB Power Adapter, Documentation', 1, CAST(0x0000A1E20140A731 AS DateTime), CAST(0x0000A1E20140A731 AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (34, N'NOKIA Asha 308 (2 Sim)', 1, 3, 50, 1790, 1, 12, 0, N'iPad mini, Dock Connector to USB Cable, USB Power Adapter, Documentation', 1, CAST(0x0000A1E2014105FA AS DateTime), CAST(0x0000A1E2014105FA AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (35, N'NOKIA X2-02 ( 2 Sim )', 1, 3, 50, 1520, 1, 12, 0, N'iPad mini, Dock Connector to USB Cable, USB Power Adapter, Documentation', 1, CAST(0x0000A1E201414041 AS DateTime), CAST(0x0000A1E201414041 AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (36, N'NOKIA C2-03 (2 Sim)', 1, 3, 50, 1270, 1, 12, 1, N'iPad mini, Dock Connector to USB Cable, USB Power Adapter, Documentation', 1, CAST(0x0000A1E2014175FC AS DateTime), CAST(0x0000A1E2014175FC AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (37, N'NOKIA C2-00 (2 sim)', 1, 3, 50, 1190, 1, 12, 2, N'iPad mini, Dock Connector to USB Cable, USB Power Adapter, Documentation', 1, CAST(0x0000A1E20141C055 AS DateTime), CAST(0x0000A1E20141C055 AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (38, N'NOKIA Asha 205 (2 sim)', 1, 3, 50, 1280, 1, 12, 0, N'iPad mini, Dock Connector to USB Cable, USB Power Adapter, Documentation', 1, CAST(0x0000A1E201421942 AS DateTime), CAST(0x0000A1E201421942 AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (39, N'NOKIA C2-03 (2 Sim)', 1, 3, 50, 1280, 1, 12, 0, N'iPad mini, Dock Connector to USB Cable, USB Power Adapter, Documentation', 1, CAST(0x0000A1E201425F8B AS DateTime), CAST(0x0000A1E201425F8B AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (40, N'NOKIA Asha 306', 1, 3, 50, 1600, 1, 12, 0, N'iPad mini, Dock Connector to USB Cable, USB Power Adapter, Documentation', 1, CAST(0x0000A1E201429CE3 AS DateTime), CAST(0x0000A1E201429CE3 AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (41, N'NOKIA Asha 200 (2 sim)', 1, 3, 50, 1590, 1, 12, 0, N'iPad mini, Dock Connector to USB Cable, USB Power Adapter, Documentation', 1, CAST(0x0000A1E20142E204 AS DateTime), CAST(0x0000A1E20142E204 AS DateTime))
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [MaLoaiSP], [MaNSX], [SoLuong], [DonGia], [TinhTrang], [BaoHanh], [SoLuotXem], [SanPhamKemTheo], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (42, N'NOKIA 5233', 1, 3, 50, 1950, 1, 12, 2, N'iPad mini, Dock Connector to USB Cable, USB Power Adapter, Documentation', 1, CAST(0x0000A1E201430B1C AS DateTime), CAST(0x0000A1E201430B1C AS DateTime))
SET IDENTITY_INSERT [dbo].[SanPham] OFF
SET IDENTITY_INSERT [dbo].[Slider] ON 

INSERT [dbo].[Slider] ([MaSlider], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (1, N'images/home/mobiles/slider-1.png', 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime))
INSERT [dbo].[Slider] ([MaSlider], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (2, N'images/home/mobiles/slider-2.png', 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime))
INSERT [dbo].[Slider] ([MaSlider], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (3, N'images/home/mobiles/slider-3.png', 1, CAST(0x0000A04B00000000 AS DateTime), CAST(0x0000A1B800000000 AS DateTime))
INSERT [dbo].[Slider] ([MaSlider], [Link], [TrangThai], [NgayTao], [NgayCapNhat]) VALUES (6, N'images/home/mobiles/slider-1.png', 2, CAST(0x0000A1DD015D7960 AS DateTime), CAST(0x0000A1DD015D7960 AS DateTime))
SET IDENTITY_INSERT [dbo].[Slider] OFF
SET IDENTITY_INSERT [dbo].[TrangThai] ON 

INSERT [dbo].[TrangThai] ([MaTrangThai], [TrangThai]) VALUES (1, N'active')
INSERT [dbo].[TrangThai] ([MaTrangThai], [TrangThai]) VALUES (2, N'expired')
SET IDENTITY_INSERT [dbo].[TrangThai] OFF
SET IDENTITY_INSERT [dbo].[UserProfile] ON 

INSERT [dbo].[UserProfile] ([UserId], [UserName]) VALUES (2, N'anhminh')
INSERT [dbo].[UserProfile] ([UserId], [UserName]) VALUES (3, N'bichngoc')
INSERT [dbo].[UserProfile] ([UserId], [UserName]) VALUES (5, N'myvan')
INSERT [dbo].[UserProfile] ([UserId], [UserName]) VALUES (6, N'thaichau')
INSERT [dbo].[UserProfile] ([UserId], [UserName]) VALUES (7, N'thanhphuc')
INSERT [dbo].[UserProfile] ([UserId], [UserName]) VALUES (8, N'minhloan')
INSERT [dbo].[UserProfile] ([UserId], [UserName]) VALUES (9, N'thienphu')
INSERT [dbo].[UserProfile] ([UserId], [UserName]) VALUES (10, N'minhthu')
INSERT [dbo].[UserProfile] ([UserId], [UserName]) VALUES (11, N'ngocyen')
INSERT [dbo].[UserProfile] ([UserId], [UserName]) VALUES (12, N'ngocgiao')
INSERT [dbo].[UserProfile] ([UserId], [UserName]) VALUES (13, N'mydung')
INSERT [dbo].[UserProfile] ([UserId], [UserName]) VALUES (14, N'phuocthinh')
INSERT [dbo].[UserProfile] ([UserId], [UserName]) VALUES (15, N'kieuchinh')
INSERT [dbo].[UserProfile] ([UserId], [UserName]) VALUES (16, N'minhhang')
INSERT [dbo].[UserProfile] ([UserId], [UserName]) VALUES (17, N'tinivippro')
INSERT [dbo].[UserProfile] ([UserId], [UserName]) VALUES (18, N'tinipink')
INSERT [dbo].[UserProfile] ([UserId], [UserName]) VALUES (19, N'deketini')
INSERT [dbo].[UserProfile] ([UserId], [UserName]) VALUES (20, N'thanhmin')
INSERT [dbo].[UserProfile] ([UserId], [UserName]) VALUES (21, N'thanhthanh')
SET IDENTITY_INSERT [dbo].[UserProfile] OFF
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (2, CAST(0x0000A1CD0158CDB2 AS DateTime), NULL, 1, NULL, 0, N'AIg6ZhdQ3sjSq04SFJnKjfiikDvCK3BdEP21lbBA4olUjRv55CFfb7JkIKUMEDIuyQ==', CAST(0x0000A1CD0158CDB2 AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (3, CAST(0x0000A1D201072F24 AS DateTime), NULL, 1, NULL, 0, N'AO46BoZKVEcflUcTJ3sddWzabp3tu0g+NL+RbI2bFoatiNCvisTUaBODtelSx9+xlA==', CAST(0x0000A1DD00109D5A AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (5, CAST(0x0000A1D20142F7BF AS DateTime), NULL, 1, NULL, 0, N'AA2T9QOwR3ULs15GQ4tgSimlpbpyfQ9AlTd2zT9+hQyQgB+sE9vh5aSrVPNYKl71HA==', CAST(0x0000A1D20142F7BF AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (6, CAST(0x0000A1D201432003 AS DateTime), NULL, 1, NULL, 0, N'AEVmIhG2JQatC23HnxWqMNXwLGYod3S6rmU9Ow+jEeV2MAQ092dsYNpXGIPCFiwq4Q==', CAST(0x0000A1D201432003 AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (7, CAST(0x0000A1D201436B99 AS DateTime), NULL, 1, NULL, 0, N'AI3pq9yWsgm6+h/WiwRXWUQ3YH/NI1vSD0qzCFM8UyzQYVVIoVaHAaXN5B8nO11uWw==', CAST(0x0000A1D201436B99 AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (8, CAST(0x0000A1D201438DAA AS DateTime), NULL, 1, NULL, 0, N'AJipi4s6TaBlJHVxtA7ywkV8tF7RONzFdyDqBW1o2XTM8r6MMdhFU4taQZp6FK9Obw==', CAST(0x0000A1D201438DAA AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (9, CAST(0x0000A1D20143A8E1 AS DateTime), NULL, 1, NULL, 0, N'AMWVxCaSNs225F3er2jUXvYjMr4cJ2J/IiuMFAxtpTHXl8OCorbCAOhV4Q49A5cxoQ==', CAST(0x0000A1D20143A8E1 AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (10, CAST(0x0000A1D20143D260 AS DateTime), NULL, 1, NULL, 0, N'AE/YSyRmObx5actQ8cZ3R745vwwLSgLi6SeqIY8uJljBafdFabgI1Pblx7Et+pTEuA==', CAST(0x0000A1D20143D260 AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (11, CAST(0x0000A1D20143EE54 AS DateTime), NULL, 1, NULL, 0, N'AL3EBcdafJSLF7Txo3/yiV2dr+YTaHNzB5965aohgJPHg3WiF+cnjGGsbUdUkjneCw==', CAST(0x0000A1D20143EE54 AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (12, CAST(0x0000A1D201440B19 AS DateTime), NULL, 1, NULL, 0, N'AMs8b5Q/s5A2OQAEgCfr0LMj8XJm/noPzgbDn9WfQhRPMJFJYA/Gle6cGMVV5QYJvw==', CAST(0x0000A1D201440B19 AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (13, CAST(0x0000A1D201442BD3 AS DateTime), NULL, 1, NULL, 0, N'ACydBKP6R9y0UeLSvFiPge0gXs3LuRsPWFdIJsTJv/4OEeOGoLtCG86wnt4nDAMLtg==', CAST(0x0000A1D201442BD3 AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (14, CAST(0x0000A1D201445BD9 AS DateTime), NULL, 1, NULL, 0, N'AMvgVI4utTOrzhQnhJWpXF4QLYO7EVZVYjyHi67vO2GakjCc5vhEm7iSjXXzB6W8Rw==', CAST(0x0000A1D201445BD9 AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (15, CAST(0x0000A1D201448400 AS DateTime), NULL, 1, NULL, 0, N'ADgmHfpYELhzJhoTOyFxmRNzmtCXi57Zq+PX0pn8RHmuez+ZnAY9Mx4WG08V/6+6lA==', CAST(0x0000A1D201448400 AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (16, CAST(0x0000A1E200EC3970 AS DateTime), NULL, 1, NULL, 0, N'AP80j9yu1UWkhpTAtI9LOT0HgFV/pWZJlqFCEWa/glZZZIq42bz8GquJixN/RNaV0Q==', CAST(0x0000A1E200EC3970 AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (17, CAST(0x0000A1E5011DC844 AS DateTime), NULL, 1, NULL, 0, N'ALIHR2KYR5b4qqXa46LvQeQiPRj5NBo1BGGXHz2npcLEK8DoLOLE59KQRReimygeGw==', CAST(0x0000A1E5011DC844 AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (18, CAST(0x0000A1E5011E2C25 AS DateTime), NULL, 1, NULL, 0, N'ABtoY9BDOSyWVzU39mQpb+X9zZkks4aPIzYc7T79jU2cVsezXo+ro2LiHKng/91rag==', CAST(0x0000A1E5011E2C25 AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (19, CAST(0x0000A1E5011EEA76 AS DateTime), NULL, 1, NULL, 0, N'ACxGtW5uyD7DevtkF16th6A/vRjXXD/CNImEp6duEwuNR043Uo/0fF0R3FARkX2tHg==', CAST(0x0000A1E5011EEA76 AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (20, CAST(0x0000A1E600535442 AS DateTime), NULL, 1, NULL, 0, N'ABw/syej1jUE2ZRAjmQ88PP/2MDcbDgRsIuKEvdpsw+eMP6Hbg7EN5CyrzARXcS8jA==', CAST(0x0000A1E600535442 AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (21, CAST(0x0000A1E6006E6E0D AS DateTime), NULL, 1, NULL, 0, N'ADpbblQVxfKMYohpFDEGsuLEqLVlfHDBn6pqdAt51NLvdUr8vvS2LTj15tEWeXQ/YQ==', CAST(0x0000A1E6006F5210 AS DateTime), N'', NULL, NULL)
SET IDENTITY_INSERT [dbo].[webpages_Roles] ON 

INSERT [dbo].[webpages_Roles] ([RoleId], [RoleName]) VALUES (1, N'Administrator')
INSERT [dbo].[webpages_Roles] ([RoleId], [RoleName]) VALUES (2, N'Member')
SET IDENTITY_INSERT [dbo].[webpages_Roles] OFF
INSERT [dbo].[webpages_UsersInRoles] ([UserId], [RoleId]) VALUES (2, 1)
INSERT [dbo].[webpages_UsersInRoles] ([UserId], [RoleId]) VALUES (3, 1)
INSERT [dbo].[webpages_UsersInRoles] ([UserId], [RoleId]) VALUES (5, 1)
INSERT [dbo].[webpages_UsersInRoles] ([UserId], [RoleId]) VALUES (6, 1)
INSERT [dbo].[webpages_UsersInRoles] ([UserId], [RoleId]) VALUES (7, 2)
INSERT [dbo].[webpages_UsersInRoles] ([UserId], [RoleId]) VALUES (8, 2)
INSERT [dbo].[webpages_UsersInRoles] ([UserId], [RoleId]) VALUES (9, 2)
INSERT [dbo].[webpages_UsersInRoles] ([UserId], [RoleId]) VALUES (10, 2)
INSERT [dbo].[webpages_UsersInRoles] ([UserId], [RoleId]) VALUES (11, 2)
INSERT [dbo].[webpages_UsersInRoles] ([UserId], [RoleId]) VALUES (12, 2)
INSERT [dbo].[webpages_UsersInRoles] ([UserId], [RoleId]) VALUES (13, 2)
INSERT [dbo].[webpages_UsersInRoles] ([UserId], [RoleId]) VALUES (14, 2)
INSERT [dbo].[webpages_UsersInRoles] ([UserId], [RoleId]) VALUES (15, 1)
INSERT [dbo].[webpages_UsersInRoles] ([UserId], [RoleId]) VALUES (19, 2)
INSERT [dbo].[webpages_UsersInRoles] ([UserId], [RoleId]) VALUES (21, 2)
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__webpages__8A2B61601D40D726]    Script Date: 6/23/2013 10:49:59 PM ******/
ALTER TABLE [dbo].[webpages_Roles] ADD UNIQUE NONCLUSTERED 
(
	[RoleName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__webpages__8A2B6160E21FE81A]    Script Date: 6/23/2013 10:49:59 PM ******/
ALTER TABLE [dbo].[webpages_Roles] ADD UNIQUE NONCLUSTERED 
(
	[RoleName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[webpages_Membership] ADD  DEFAULT ((0)) FOR [IsConfirmed]
GO
ALTER TABLE [dbo].[webpages_Membership] ADD  DEFAULT ((0)) FOR [PasswordFailuresSinceLastSuccess]
GO
ALTER TABLE [dbo].[BinhLuan]  WITH CHECK ADD  CONSTRAINT [FK_BinhLuan_SanPham] FOREIGN KEY([MaSP])
REFERENCES [dbo].[SanPham] ([MaSP])
GO
ALTER TABLE [dbo].[BinhLuan] CHECK CONSTRAINT [FK_BinhLuan_SanPham]
GO
ALTER TABLE [dbo].[BinhLuan]  WITH CHECK ADD  CONSTRAINT [FK_BinhLuan_TrangThai] FOREIGN KEY([TrangThai])
REFERENCES [dbo].[TrangThai] ([MaTrangThai])
GO
ALTER TABLE [dbo].[BinhLuan] CHECK CONSTRAINT [FK_BinhLuan_TrangThai]
GO
ALTER TABLE [dbo].[ChiTietHoaDon]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietHoaDon_HoaDon] FOREIGN KEY([MaHD])
REFERENCES [dbo].[HoaDon] ([MaHD])
GO
ALTER TABLE [dbo].[ChiTietHoaDon] CHECK CONSTRAINT [FK_ChiTietHoaDon_HoaDon]
GO
ALTER TABLE [dbo].[ChiTietHoaDon]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietHoaDon_SanPham] FOREIGN KEY([MaSP])
REFERENCES [dbo].[SanPham] ([MaSP])
GO
ALTER TABLE [dbo].[ChiTietHoaDon] CHECK CONSTRAINT [FK_ChiTietHoaDon_SanPham]
GO
ALTER TABLE [dbo].[ChiTietHoaDon]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietHoaDon_TrangThai] FOREIGN KEY([TrangThai])
REFERENCES [dbo].[TrangThai] ([MaTrangThai])
GO
ALTER TABLE [dbo].[ChiTietHoaDon] CHECK CONSTRAINT [FK_ChiTietHoaDon_TrangThai]
GO
ALTER TABLE [dbo].[ChiTietSanPham]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietSanPham_SanPham] FOREIGN KEY([MaSP])
REFERENCES [dbo].[SanPham] ([MaSP])
GO
ALTER TABLE [dbo].[ChiTietSanPham] CHECK CONSTRAINT [FK_ChiTietSanPham_SanPham]
GO
ALTER TABLE [dbo].[ChiTietSanPham]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietSanPham_TrangThai] FOREIGN KEY([TrangThai])
REFERENCES [dbo].[TrangThai] ([MaTrangThai])
GO
ALTER TABLE [dbo].[ChiTietSanPham] CHECK CONSTRAINT [FK_ChiTietSanPham_TrangThai]
GO
ALTER TABLE [dbo].[HinhAnh]  WITH CHECK ADD  CONSTRAINT [FK_HinhAnh_SanPham] FOREIGN KEY([MaSP])
REFERENCES [dbo].[SanPham] ([MaSP])
GO
ALTER TABLE [dbo].[HinhAnh] CHECK CONSTRAINT [FK_HinhAnh_SanPham]
GO
ALTER TABLE [dbo].[HinhAnh]  WITH CHECK ADD  CONSTRAINT [FK_HinhAnh_TrangThai] FOREIGN KEY([TrangThai])
REFERENCES [dbo].[TrangThai] ([MaTrangThai])
GO
ALTER TABLE [dbo].[HinhAnh] CHECK CONSTRAINT [FK_HinhAnh_TrangThai]
GO
ALTER TABLE [dbo].[HoaDon]  WITH CHECK ADD  CONSTRAINT [FK_HoaDon_KhachHang] FOREIGN KEY([MaKH])
REFERENCES [dbo].[KhachHang] ([MaKH])
GO
ALTER TABLE [dbo].[HoaDon] CHECK CONSTRAINT [FK_HoaDon_KhachHang]
GO
ALTER TABLE [dbo].[HoaDon]  WITH CHECK ADD  CONSTRAINT [FK_HoaDon_TrangThai] FOREIGN KEY([TrangThai])
REFERENCES [dbo].[TrangThai] ([MaTrangThai])
GO
ALTER TABLE [dbo].[HoaDon] CHECK CONSTRAINT [FK_HoaDon_TrangThai]
GO
ALTER TABLE [dbo].[KhachHang]  WITH CHECK ADD  CONSTRAINT [FK_KhachHang_TrangThai] FOREIGN KEY([TrangThai])
REFERENCES [dbo].[TrangThai] ([MaTrangThai])
GO
ALTER TABLE [dbo].[KhachHang] CHECK CONSTRAINT [FK_KhachHang_TrangThai]
GO
ALTER TABLE [dbo].[KhachHang]  WITH CHECK ADD  CONSTRAINT [FK_KhachHang_UserProfile] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserProfile] ([UserId])
GO
ALTER TABLE [dbo].[KhachHang] CHECK CONSTRAINT [FK_KhachHang_UserProfile]
GO
ALTER TABLE [dbo].[KhuyenMai]  WITH CHECK ADD  CONSTRAINT [FK_KhuyenMai_QuaTang] FOREIGN KEY([QuaTang])
REFERENCES [dbo].[QuaTang] ([MaQuaTang])
GO
ALTER TABLE [dbo].[KhuyenMai] CHECK CONSTRAINT [FK_KhuyenMai_QuaTang]
GO
ALTER TABLE [dbo].[KhuyenMai]  WITH CHECK ADD  CONSTRAINT [FK_KhuyenMai_SanPham] FOREIGN KEY([MaSP])
REFERENCES [dbo].[SanPham] ([MaSP])
GO
ALTER TABLE [dbo].[KhuyenMai] CHECK CONSTRAINT [FK_KhuyenMai_SanPham]
GO
ALTER TABLE [dbo].[KhuyenMai]  WITH CHECK ADD  CONSTRAINT [FK_KhuyenMai_TrangThai] FOREIGN KEY([TrangThai])
REFERENCES [dbo].[TrangThai] ([MaTrangThai])
GO
ALTER TABLE [dbo].[KhuyenMai] CHECK CONSTRAINT [FK_KhuyenMai_TrangThai]
GO
ALTER TABLE [dbo].[LoaiSanPham]  WITH CHECK ADD  CONSTRAINT [FK_LoaiSanPham_TrangThai] FOREIGN KEY([TrangThai])
REFERENCES [dbo].[TrangThai] ([MaTrangThai])
GO
ALTER TABLE [dbo].[LoaiSanPham] CHECK CONSTRAINT [FK_LoaiSanPham_TrangThai]
GO
ALTER TABLE [dbo].[MauSac]  WITH CHECK ADD  CONSTRAINT [FK_MauSac_TrangThai] FOREIGN KEY([TrangThai])
REFERENCES [dbo].[TrangThai] ([MaTrangThai])
GO
ALTER TABLE [dbo].[MauSac] CHECK CONSTRAINT [FK_MauSac_TrangThai]
GO
ALTER TABLE [dbo].[MauSacSanPham]  WITH CHECK ADD  CONSTRAINT [FK_MauSacSanPham_MauSac] FOREIGN KEY([MaMS])
REFERENCES [dbo].[MauSac] ([MaMauSac])
GO
ALTER TABLE [dbo].[MauSacSanPham] CHECK CONSTRAINT [FK_MauSacSanPham_MauSac]
GO
ALTER TABLE [dbo].[MauSacSanPham]  WITH CHECK ADD  CONSTRAINT [FK_MauSacSanPham_SanPham] FOREIGN KEY([MaSP])
REFERENCES [dbo].[SanPham] ([MaSP])
GO
ALTER TABLE [dbo].[MauSacSanPham] CHECK CONSTRAINT [FK_MauSacSanPham_SanPham]
GO
ALTER TABLE [dbo].[MauSacSanPham]  WITH CHECK ADD  CONSTRAINT [FK_MauSacSanPham_TrangThai] FOREIGN KEY([TrangThai])
REFERENCES [dbo].[TrangThai] ([MaTrangThai])
GO
ALTER TABLE [dbo].[MauSacSanPham] CHECK CONSTRAINT [FK_MauSacSanPham_TrangThai]
GO
ALTER TABLE [dbo].[NhanVien]  WITH CHECK ADD  CONSTRAINT [FK_NhanVien_TrangThai] FOREIGN KEY([TrangThai])
REFERENCES [dbo].[TrangThai] ([MaTrangThai])
GO
ALTER TABLE [dbo].[NhanVien] CHECK CONSTRAINT [FK_NhanVien_TrangThai]
GO
ALTER TABLE [dbo].[NhanVien]  WITH CHECK ADD  CONSTRAINT [FK_NhanVien_UserProfile] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserProfile] ([UserId])
GO
ALTER TABLE [dbo].[NhanVien] CHECK CONSTRAINT [FK_NhanVien_UserProfile]
GO
ALTER TABLE [dbo].[NhaSanXuat]  WITH CHECK ADD  CONSTRAINT [FK_NhaSanXuat_LoaiSanPham] FOREIGN KEY([LoaiSP])
REFERENCES [dbo].[LoaiSanPham] ([MaLoaiSP])
GO
ALTER TABLE [dbo].[NhaSanXuat] CHECK CONSTRAINT [FK_NhaSanXuat_LoaiSanPham]
GO
ALTER TABLE [dbo].[NhaSanXuat]  WITH CHECK ADD  CONSTRAINT [FK_NhaSanXuat_TrangThai] FOREIGN KEY([TrangThai])
REFERENCES [dbo].[TrangThai] ([MaTrangThai])
GO
ALTER TABLE [dbo].[NhaSanXuat] CHECK CONSTRAINT [FK_NhaSanXuat_TrangThai]
GO
ALTER TABLE [dbo].[QuaTang]  WITH CHECK ADD  CONSTRAINT [FK_QuaTang_TrangThai] FOREIGN KEY([TrangThai])
REFERENCES [dbo].[TrangThai] ([MaTrangThai])
GO
ALTER TABLE [dbo].[QuaTang] CHECK CONSTRAINT [FK_QuaTang_TrangThai]
GO
ALTER TABLE [dbo].[SanPham]  WITH CHECK ADD  CONSTRAINT [FK_SanPham_LoaiSanPham] FOREIGN KEY([MaLoaiSP])
REFERENCES [dbo].[LoaiSanPham] ([MaLoaiSP])
GO
ALTER TABLE [dbo].[SanPham] CHECK CONSTRAINT [FK_SanPham_LoaiSanPham]
GO
ALTER TABLE [dbo].[SanPham]  WITH CHECK ADD  CONSTRAINT [FK_SanPham_NhaSanXuat] FOREIGN KEY([MaNSX])
REFERENCES [dbo].[NhaSanXuat] ([MaNSX])
GO
ALTER TABLE [dbo].[SanPham] CHECK CONSTRAINT [FK_SanPham_NhaSanXuat]
GO
ALTER TABLE [dbo].[SanPham]  WITH CHECK ADD  CONSTRAINT [FK_SanPham_TrangThai] FOREIGN KEY([TrangThai])
REFERENCES [dbo].[TrangThai] ([MaTrangThai])
GO
ALTER TABLE [dbo].[SanPham] CHECK CONSTRAINT [FK_SanPham_TrangThai]
GO
ALTER TABLE [dbo].[Slider]  WITH CHECK ADD  CONSTRAINT [FK_Slider_TrangThai] FOREIGN KEY([TrangThai])
REFERENCES [dbo].[TrangThai] ([MaTrangThai])
GO
ALTER TABLE [dbo].[Slider] CHECK CONSTRAINT [FK_Slider_TrangThai]
GO
ALTER TABLE [dbo].[webpages_UsersInRoles]  WITH CHECK ADD  CONSTRAINT [fk_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[webpages_Roles] ([RoleId])
GO
ALTER TABLE [dbo].[webpages_UsersInRoles] CHECK CONSTRAINT [fk_RoleId]
GO
ALTER TABLE [dbo].[webpages_UsersInRoles]  WITH CHECK ADD  CONSTRAINT [fk_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserProfile] ([UserId])
GO
ALTER TABLE [dbo].[webpages_UsersInRoles] CHECK CONSTRAINT [fk_UserId]
GO
USE [master]
GO
ALTER DATABASE [MobileWeb] SET  READ_WRITE 
GO
